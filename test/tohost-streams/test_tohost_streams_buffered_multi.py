#!/usr/bin/env python3

import concurrent.futures
import subprocess
import unittest
import os
from ast import literal_eval
from felix_test_case import FelixTestCase


class TestToHostBufferedStreamsMulti(FelixTestCase):

    def setUp(self):
        if FelixTestCase.hardware:
            print("hardware mode\n")
            os.system('feconf -S -s 200 -I 40 -d 0 GBT_test.elc ; fpepo -d 0 6630 2; femu -e')
        self.start("file2host-buffered-streams", "tohost-gbt-buffered")

    def tearDown(self):
        self.stop('file2host-buffered-streams', "tohost-gbt-buffered")

    def run_swrod_fid_stream(self, fid):
        try:
            timeout = 100
            msgs = "5000000"
            test = "./felix-test-swrod"
            if FelixTestCase.hardware:
                msgs = "200000"
            ip = FelixTestCase.ip
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join((test, "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-f", fid, "-m", msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def test_swrod_fid_stream_multi(self):
        did = literal_eval(FelixTestCase.did)
        cid = literal_eval(FelixTestCase.cid)
        fid_sid = hex(FelixTestCase.get_fid(did, cid, 64, 0x9a)).rstrip('L')
        fid_alt_sid = hex(FelixTestCase.get_fid(did, cid, 64, 0xd8)).rstrip('L')
        if FelixTestCase.hardware:
            fid_sid = hex(FelixTestCase.get_fid(did, cid, 74, 0)).rstrip('L')
            fid_alt_sid = hex(FelixTestCase.get_fid(did, cid, 74, 1)).rstrip('L')
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_swrod_fid_stream, fid_sid)
        b = executor.submit(self.run_swrod_fid_stream, fid_alt_sid)
        task_set[a] = "fid_sid"
        task_set[b] = "fid_alt_sid"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))


if __name__ == '__main__':
    unittest.main()
