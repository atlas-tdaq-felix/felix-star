#!/usr/bin/env python3

import subprocess
import unittest
import os
from ast import literal_eval

from felix_test_case import FelixTestCase

# Environment FELIX_IFACE=eth1 otherwise default list...
# Environment FELIX_HARDWARE otherwise software simulation...


class TestToHostUnbufferedStreams(FelixTestCase):

    def setUp(self):
        if FelixTestCase.hardware:
            print("hardware mode\n")
            os.system('feconf -s 5000 -S -I 40 -d 0 FM_Felixcore_3p3_CPU_test.elc ; fpepo -d 0 6630 2; femu -e')
        self.start('file2host-unbuffered-streams', 'tohost-fm-unbuffered')

    def tearDown(self):
        self.stop('file2host-unbuffered-streams', 'tohost-fm-unbuffered')

    def test_swrod_streams(self):
        try:
            timeout = 100
            did = literal_eval(FelixTestCase.did)
            cid = literal_eval(FelixTestCase.cid)
            fid_sid = hex(FelixTestCase.get_fid(did, cid, 64, 0x9a)).rstrip('L')
            msgs = "500000"
            test = "./felix-test-swrod"
            if FelixTestCase.hardware:
                msgs = "200000"
                fid_sid = FelixTestCase.hw_fid_sid_fm
            ip = FelixTestCase.ip
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join((test, "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-f", fid_sid, "-m", msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
