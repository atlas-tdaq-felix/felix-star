#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestBus(FelixTestCase):

    def setUp(self):
        self.start('stats')

    def tearDown(self):
        self.stop('stats')

    def test_bus(self):
        try:
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-bus", "--verbose", "--bus-dir", bus_dir, "--bus-groupname", group_name, "0x1000000000010000")), stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
