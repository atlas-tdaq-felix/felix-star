#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestToHostGBTInvalidHeader(FelixTestCase):

    def setUp(self):
        self.start('file2host-gbt-buffered')

    def tearDown(self):
        self.stop('file2host-gbt-buffered')

    def test_swrod_gbt(self):
        try:
            timeout = 100
            elink = "0x37"
            msgs = "20000"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-test-swrod", "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", elink, "-t", "44", "-m", msgs, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
