#!/usr/bin/env python3

import subprocess
import unittest
import os
from ast import literal_eval

from felix_test_case import FelixTestCase

# Environment FELIX_IFACE=eth1 otherwise default list...
# Environment FELIX_HARDWARE otherwise software simulation...


class TestToHostGBT(FelixTestCase):

    def setUp(self):
        if FelixTestCase.hardware:
            print("hardware mode\n")
            os.system('feconf -s  100 -I 40 -d 0 GBT_test.elc ; fpepo -d 0 6630 2; femu -e')
        self.start("file2host-buffered", "tohost-buffered")

    def tearDown(self):
        self.stop('file2host-buffered', "tohost-buffered")

    def test_swrod_gbt_elink(self):
        try:
            timeout = 100
            elink = FelixTestCase.elink
            msgs = "6000000"
            test = "./felix-test-swrod"
            if FelixTestCase.hardware:
                msgs = "300000"
                elink = "0x4A"
            print("elink:", elink)
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join((test, "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", elink, "-m", msgs, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def test_swrod_gbt_fid(self):
        try:
            timeout = 100
            fid = FelixTestCase.fid
            msgs = "5000000"
            if FelixTestCase.hardware:
                msgs = "300000"
                did = literal_eval(FelixTestCase.did)
                cid = literal_eval(FelixTestCase.cid)
                fid = hex(FelixTestCase.get_fid(did, cid, 74)).rstrip('L')
            ip = FelixTestCase.ip
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            print("fid:", fid, " mesg:", msgs)
            subprocess.check_output(' '.join(("./felix-test-swrod", "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "--fid ", fid, "-m", msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
