#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestToHostGBT(FelixTestCase):

    def setUp(self):
        if FelixTestCase.hardware:
            print("hardware mode\n")
            os.system('feconf -s  100 -I 40 -d 0 GBT_test.elc ; fpepo -d 0 6630 2; femu -e')
        self.start("file2host-buffered", "tohost-buffered")

    def tearDown(self):
        self.stop('file2host-buffered', "tohost-buffered")

    def run_test_swrod(self):
        try:
            timeout = 100
            elink = FelixTestCase.elink
            msgs = "5000000"
            test = "./felix-test-swrod"
            if FelixTestCase.hardware:
                msgs = "20000"
                elink = "0x4A"
            print("elink:", elink)
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join((test, "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", elink, "-m", msgs, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def test_resubscribe_gbt(self):
        self.run_test_swrod()
        self.run_test_swrod()
        self.run_test_swrod()


if __name__ == '__main__':
    unittest.main()
