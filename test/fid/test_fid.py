#!/usr/bin/env python3

import subprocess
import unittest


class TestFid(unittest.TestCase):

    def test_fid_to_decimal(self):
        try:
            output = subprocess.check_output("./felix-fid 1311768467463790320", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '01-035-17767-1-0039612-1-094-240')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_hex(self):
        try:
            output = subprocess.check_output("./felix-fid --hex 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x1-23-4567-1-09abc-1-5e-f0')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_did_cid_lid_tid_sid_to_fid(self):
        try:
            output = subprocess.check_output("./felix-fid --hex 0x23 0x4567 0x226a 0x3cde 0xf0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x123456789abc80f0')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_0_did_cid_lid_tid_sid_to_fid(self):
        try:
            output = subprocess.check_output("./felix-fid --update=0 --hex 0x23 0x4567 0x226a 0x3cde 0xf0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x9abc')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_vid(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --vid 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x1')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_did(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --did 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x23')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_co(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --co 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x1234567')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_elink(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --elink 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x9abc')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_lid(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --lid 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x226a')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_tid(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --tid 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0x3cde')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_fid_to_sid(self):
        try:
            output = subprocess.check_output("./felix-fid --hex --sid 0x123456789abcdef0", stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertEqual(output, '0xf0')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
