#!/usr/bin/env python3

from __future__ import unicode_literals

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestToHostBufferedLargeData(FelixTestCase):

    def setUp(self):
        self.start('file2host-buffered-large-data')

    def tearDown(self):
        self.stop('file2host-buffered-large-data')

    def test_large_chunks(self):
        try:
            timeout = 30
            elink = "0x40"
            truncated_chunks = "4000"  # number of truncated chunks for input file and 5000 msgs
            msgs = "4000"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-test-swrod", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", elink, "-m", msgs, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print("rc = {}".format(e.returncode))
            print("cmd = {}".format(e.cmd))
            print("output = {}".format(e.output))
            self.assertTrue("\"chk\": {}, \"t\": {}".format(str(msgs), str(truncated_chunks)) in e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
