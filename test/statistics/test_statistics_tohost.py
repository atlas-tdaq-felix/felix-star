#!/usr/bin/env python3

import concurrent.futures
import json
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestStatisticsToHost(FelixTestCase):

    def setUp(self):
        self.start('mkfifo-tohost')
        self.start('file2host-gbt-buffered')

    def tearDown(self):
        self.stop('file2host-gbt-buffered')
        self.stop('mkfifo-tohost')
        self.start('rmfifo-tohost')
        self.stop('rmfifo-tohost')

    def run_gbt_swrod(self):
        try:
            timeout = 100
            elink = FelixTestCase.elink
            msgs = "5000000"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-test-swrod", "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", elink, "-m", msgs, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def run_statistics(self):
        json_test = {}
        jsonfifo = '/tmp/felix-star_stats_tohost_' + FelixTestCase.uuid + '_fifo'
        with open(jsonfifo, 'r') as f:
            i = 0
            for line in f:
                json_test = json.loads(line)
                print(json_test)
                i += 1
                self.assertTrue('fid' in json_test)
                self.assertTrue(isinstance(json_test['fid'], int))
                self.assertTrue('ts' in json_test)
                self.assertTrue(isinstance(json_test['ts'], str))
                if 'global' in json_test:
                    gbl = json_test['global']
                    self.assertTrue('throughput[Gbps]' in gbl)
                    self.assertTrue(isinstance(gbl['throughput[Gbps]'], float))
                    self.assertTrue('blockrate[kHz]' in gbl)
                    self.assertTrue(isinstance(gbl['blockrate[kHz]'], float))
                    self.assertTrue('chunkrate[kHz]' in gbl)
                    self.assertTrue(isinstance(gbl['chunkrate[kHz]'], float))
                    self.assertTrue('buffer_events' in gbl)
                    self.assertTrue(isinstance(gbl['buffer_events'], int))
                    self.assertTrue('interrupts' in gbl)
                    self.assertTrue(isinstance(gbl['interrupts'], int))
                    self.assertTrue('polls' in gbl)
                    self.assertTrue(isinstance(gbl['polls'], int))
                else:
                    self.assertTrue(False)
                if i > 20:
                    break
        return True

    def test_statistics_tohost(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_gbt_swrod)
        task_set[a] = "gbt_swrod"
        b = executor.submit(self.run_statistics)
        task_set[b] = "statistics"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + ' ' + str(task.result()))


if __name__ == '__main__':
    unittest.main()
