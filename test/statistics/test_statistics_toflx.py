#!/usr/bin/env python3

import concurrent.futures
import json
import numbers
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestStatisticsToFlx(FelixTestCase):

    def setUp(self):
        self.start('mkfifo-toflx')
        self.start('toflx2file-buffered')

    def tearDown(self):
        self.stop('toflx2file-buffered')
        self.stop('mkfifo-toflx')
        self.start('rmfifo-toflx')
        self.stop('rmfifo-toflx')

    def send_buffered(self):
        try:
            ip = FelixTestCase.ip
            port = FelixTestCase.port
            elink = FelixTestCase.elink
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            subprocess.check_output(' '.join(('./felix-test-send-toflx-buffered', ip, port, elink, '--did', did, '--cid', cid, '"#Hello"')), stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            json_ref = {
                "elink": elink,
                "data": "#Hello",
            }
            print(json_ref)
            json_test = {}
            with open('test/statistics/data-buffered-' + FelixTestCase.uuid + '.json') as f:
                json_test = json.load(f)
            self.assertJSON(json_ref, json_test)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def run_statistics(self):
        json_test = {}
        jsonfifo = '/tmp/felix-star_stats_toflx_' + FelixTestCase.uuid + '_fifo'
        with open(jsonfifo, 'r') as f:
            i = 0
            for line in f:
                json_test = json.loads(line)
                i += 1
                self.assertTrue('fid' in json_test)
                self.assertTrue(isinstance(json_test['fid'], int))
                self.assertTrue('ts' in json_test)
                self.assertTrue(isinstance(json_test['ts'], str))
                self.assertTrue('throughput[Gbps]' in json_test)
                self.assertTrue(isinstance(json_test['throughput[Gbps]'], numbers.Number))
                self.assertTrue('msg_rate[kHz]' in json_test)
                self.assertTrue(isinstance(json_test['msg_rate[kHz]'], numbers.Number))
                self.assertTrue('dma_rate[kHz]' in json_test)
                self.assertTrue(isinstance(json_test['dma_rate[kHz]'], numbers.Number))
                self.assertTrue('msgs_received' in json_test)
                self.assertTrue(isinstance(json_test['msgs_received'], int))
                if i > 1:
                    break
        return True

    def test_statistics_toflx(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.send_buffered)
        task_set[a] = "send_buffered"
        b = executor.submit(self.run_statistics)
        task_set[b] = "statistics"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + ' ' + str(task.result()))


if __name__ == '__main__':
    unittest.main()
