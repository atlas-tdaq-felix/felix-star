import pytest


def pytest_addoption(parser):
    parser.addoption("--skip-updown", action='store_true')
    parser.addoption('--scas', default=1, type=int)
    parser.addoption('--did', default=-1, type=int)
    parser.addoption('--cid', default=-1, type=int)
    parser.addoption('--bus-dir', default="")
    parser.addoption('--bus-groupname', default="")


@pytest.fixture
def skip_updown(request):
    return request.config.getoption("--skip-updown")


@pytest.fixture
def scas(request):
    return request.config.getoption("--scas")


@pytest.fixture
def did(request):
    return request.config.getoption("--did")


@pytest.fixture
def cid(request):
    return request.config.getoption("--cid")


@pytest.fixture
def bus_dir(request):
    return request.config.getoption("--bus-dir")


@pytest.fixture
def bus_groupname(request):
    return request.config.getoption("--bus-groupname")
