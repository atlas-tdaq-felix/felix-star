#!/usr/bin/env python3

import atexit
import functools
import math
import pytest
import sys
import time
import timeout_decorator

from dataclasses import dataclass, field

from felix_test_case import FelixTestCase
from felix_fid import FID

from libfelix_client_thread_py import FelixClientConfig, FelixClientThread  # noqa: E402
from felix_client_properties import FelixClientKey  # noqa: E402

from hdlc import encode_hdlc_ctrl_header, encode_hdlc_msg_header, encode_hdlc_trailer, verify_hdlc_trailer, HDLC_CTRL_RESET

printf = functools.partial(print, flush=True)


# The following constant relates to Henk B. method of achieving (relatively small - up to say 50 us) delays on the HDLC-encoded elink.
# A good reading on Henk's method is in: https://its.cern.ch/jira/browse/FLX-581
# The constant below defines how many zeros to put per microsecond of delay.
# HDLC Elink throughput is 80Mbps, so 80 bits goes through each 1us.  80bits is 10 bytes that's why we insert 10 bytes to delay by 1 us.
#
# see also: https://gitlab.cern.ch/atlas-dcs-common-software/ScaSoftware/-/blob/master/NetioNextBackend/HdlcBackend.cpp#L109
#
class TestSendCmd(FelixTestCase):

    @dataclass
    class Counters:
        t0: float = 0.0
        times: list = field(default_factory=list)
        send_counter: int = 0
        recv_counter: int = 0

    connected_map = {}      # by send_tag and subscribe tag -> bool
    subscription_map = {}   # by subscribe_tag -> send_tag
    performance_map = {}    # by send_tag -> Counters
    reply_map = {}          # by send_tag -> list of bytearrays
    seqnr = 0
    client = None
    timeout_ms = 1000

    @pytest.fixture(autouse=True)
    def inject_config(self, request):
        self.config = request.config

    def setUp(self):
        if self.config.getoption("--skip-updown"):
            return
        self.start('mkfifo')
        self.start('fifo2host-buffered-' + FelixTestCase.netio_protocol)
        self.start('sca-simulator')
        self.start('toflx2fifo-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        if self.config.getoption("--skip-updown"):
            return
        self.stop('toflx2fifo-buffered-' + FelixTestCase.netio_protocol)
        self.stop('sca-simulator')
        self.stop('fifo2host-buffered-' + FelixTestCase.netio_protocol)
        self.stop('mkfifo')
        self.start('rmfifo')
        self.stop('rmfifo')

    def on_init(self):
        printf("Initialising connections...")

    def on_connect(self, fid):
        printf("\x1b[32m Connection up for fid", hex(fid), "\x1b[0m")
        self.connected_map[fid] = True

    def on_disconnect(self, fid):
        printf("\x1b[31m Disconnection for fid", hex(fid), "\x1b[0m")
        # crashes as its called from the exit handler, no more this pointer
        # self.connected_map[fid] = false;

    def control_cmd(self, addr, control, send_tag, reply):
        cmd = encode_hdlc_ctrl_header(addr, control)
        cmd.extend(encode_hdlc_trailer(cmd))
        self.reply_map[send_tag].append(reply)
        return bytes(cmd)

    def cmd(self, addr, seqnr, data, send_tag, reply):
        cmd = encode_hdlc_msg_header(addr, seqnr)
        cmd.extend(data)
        cmd.extend(encode_hdlc_trailer(cmd))
        expected_reply = encode_hdlc_msg_header(addr, seqnr)
        expected_reply.extend(reply)
        self.reply_map[send_tag].append(bytes(expected_reply))
        return bytes(cmd)

    def send_cmd(self, send_tag):
        msgs = []

        # reset
        msgs.append(self.control_cmd(0, HDLC_CTRL_RESET, send_tag, b'\xFF\x63'))

        # chip ID
        msgs.append(self.cmd(0, self.seqnr, b'\x01\x14\x04\xd1\x00\x00\x01\x00', send_tag, b'\x01\x14\x00\x04\x0B\x00\x00\x07'))

        # ADC Go
        msgs.append(self.cmd(0, self.seqnr, b'\x01\x14\x04\x02\x00\x00\x01\x00', send_tag, b'\x01\x14\x00\x04\x00\x00\x07\x88'))

        printf("Sending commands for", hex(send_tag))
        for msg in msgs:
            printf("\x1b[33m Sending cmd", msg, "\x1b[0m")

        self.performance_map[send_tag].t0 = time.clock_gettime(time.CLOCK_MONOTONIC_RAW)
        try:
            self.client.send_data(send_tag, msgs)
            printf("Sending done")
        except Exception as e:
            printf("\x1b[31m Unable to send, because", e, "\x1b[0m")

        self.performance_map[send_tag].send_counter += 1

    def on_data(self, fid, data, status):
        send_tag = self.subscription_map[fid]

        ms = 1.0e-6 * (time.clock_gettime(time.CLOCK_MONOTONIC_RAW) - self.performance_map[send_tag].t0)
        self.performance_map[send_tag].times.append(ms)
        self.performance_map[send_tag].recv_counter += 1

        printf("Message from", hex(fid), "msg \x1b[32m", data, "status", status, "received after", ms, "ms\x1b[0m")

        # Check CRC
        if not verify_hdlc_trailer(data):
            printf("ERROR CRC incorrect")
            exit(1)

        printf(data)
        data = data[:-2]
        printf(data)

        # verify answer FIXME
        for reply in self.reply_map[send_tag]:
            printf("Checking for expected reply", reply)
            if data == reply:
                # remove entry
                self.reply_map[send_tag].remove(reply)
                printf("Reply found!")
                return

        printf("Error Unexpected Reply", data)

    def results(self, perf, fid):
        avg = 0.0
        rms = 0.0
        n = 0
        for t in perf.times:
            if t > 0 and t < self.timeout_ms:
                avg += t
                rms += t * t
                n += 1
        avg /= n
        rms = math.sqrt(rms) / (n-1)
        printf("\x1b[32m  FID:", hex(fid), "Averaged time on", n, "measurements:", avg, "+/-", rms, "ms\x1b[0m")
        self.client.unsubscribe(fid)

    @timeout_decorator.timeout(15)
    def test_sca_commands(self):

        config = FelixClientConfig()
        config.on_init_callback(self.on_init)
        config.on_connect_callback(self.on_connect)
        # config.on_disconnect_callback(self.on_disconnect)
        config.on_data_callback(self.on_data)

        config.property[FelixClientKey.LOCAL_IP_OR_INTERFACE.value] = FelixTestCase.iface
        bus_dir = self.config.getoption("--bus-dir")
        if bus_dir == "":
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
        config.property[FelixClientKey.BUS_DIR.value] = bus_dir
        bus_groupname = self.config.getoption("--bus-groupname")
        if bus_groupname == "":
            bus_groupname = "FELIX_STAR_" + FelixTestCase.uuid
        config.property[FelixClientKey.BUS_GROUP_NAME.value] = bus_groupname
        config.property[FelixClientKey.LOG_LEVEL.value] = 'trace'
        config.property[FelixClientKey.VERBOSE_BUS.value] = 'True'
        config.property[FelixClientKey.TIMEOUT.value] = '0'
        config.property[FelixClientKey.NETIO_PAGES.value] = '256'
        config.property[FelixClientKey.NETIO_PAGESIZE.value] = '65536'

        atexit.register(self.cleanup, config)

        did = self.config.getoption("--did")
        if did < 0:
            did = int(FelixTestCase.did, 0)
        cid = self.config.getoption("--cid")
        if cid < 0:
            cid = int(FelixTestCase.cid, 0)

        scas = self.config.getoption("--scas")
        elink_start = int(FelixTestCase.sca_elink, 0)
        elinks = []
        for i in range(scas):
            elinks.append(elink_start + i)

        self.client = FelixClientThread(config)

        send_tags = []
        for elink in elinks:
            stream_id = 0

            printf("Creating fid with did", hex(did), " cid", hex(cid), "elink", hex(elink), "streamId", hex(stream_id))
            subscribe_tag = FID.get_fid(did, cid, elink, stream_id, 0, 0)
            send_tag = FID.get_fid(did, cid, elink, stream_id, 1, 0)
            send_tags.append(send_tag)

            printf("Subscribe tag", hex(subscribe_tag))
            printf("Send tag", hex(send_tag))

            self.subscription_map[subscribe_tag] = send_tag
            self.performance_map[send_tag] = self.Counters()
            self.connected_map[send_tag] = False
            self.connected_map[subscribe_tag] = False
            self.reply_map[send_tag] = []

            self.client.subscribe(subscribe_tag)

        # check if all subscriptions are connected
        printf("Waiting for subscriptions...")
        all_connected = False
        while not all_connected:
            all_connected = True
            for subscribe_tag in self.subscription_map:
                all_connected &= self.connected_map[subscribe_tag]
            time.sleep(1)

        for send_tag in self.subscription_map.values():
            printf("Sending commands...")
            try:
                self.send_cmd(send_tag)
                printf("Sending done to", hex(send_tag))
            except Exception as e:
                printf("\x1b[31m Unable to send, because: ", e, " \x1b[0m")

        printf("Waiting for the following replies...")
        for send_tag in self.subscription_map.values():
            printf(" for tag", hex(send_tag))
            for reply in self.reply_map[send_tag]:
                printf("    Expected Reply:", reply, "\x1b[0m")

        printf("Checking for replies...")
        all_replied = False
        while not all_replied:
            all_replied = True
            for send_tag in self.subscription_map.values():
                all_replied &= (len(self.reply_map[send_tag]) == 0)
            time.sleep(1)

        for send_tag in self.subscription_map.values():
            self.results(self.performance_map[send_tag], send_tag)
        self.subscription_map.clear()

    def cleanup(self, config):
        printf("Cleanup")
        config.on_init_callback(None)
        config.on_connect_callback(None)
        config.on_disconnect_callback(None)
        config.on_data_callback(None)

        for subscribe_tag in self.subscription_map:
            self.client.unsubscribe(subscribe_tag)


if __name__ == '__main__':
    arg_list = sys.argv
    arg_list.append("-s")
    sys.exit(pytest.main(arg_list))
