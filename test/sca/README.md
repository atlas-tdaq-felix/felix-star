Start Up by hand for test_sca_commands_buffered_multi.py:

mkfifo tohost toflx
./felix-file2host tohost --trailer-size 2 --vmem --no-repeat --verbose --ip 10.193.176.30 --port 5000 --tag 0x2c --no-of-tags 100
./felix-sca-simulator toflx tohost 0x2c --verbose --scas 100
./felix-toflx2file toflx --verbose --raw --vmem --ip 10.193.176.30 --port 5001 --tag 0x2c --dma 1 --no-of-tags 100

../felix-client/felix-client-thread-sca-commands 10.193.176.30 0 0 0x2c --scas 100
