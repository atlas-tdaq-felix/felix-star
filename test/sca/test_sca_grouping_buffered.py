#!/usr/bin/env python3

import shutil
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestScaGroupingUnbuffered(FelixTestCase):

    def setUp(self):
        self.start('mkfifo')
        self.start('fifo2host-buffered-' + FelixTestCase.netio_protocol)
        self.start('sca-simulator')
        self.start('toflx2fifo-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('toflx2fifo-buffered-' + FelixTestCase.netio_protocol)
        self.stop('sca-simulator')
        self.stop('fifo2host-buffered-' + FelixTestCase.netio_protocol)
        self.stop('mkfifo')
        self.start('rmfifo')
        self.stop('rmfifo')

    def test_sca_grouping_unbuffered(self):
        try:
            timeout = 60
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            iface = FelixTestCase.iface
            sca_elink = FelixTestCase.sca_elink
            iterations = str(10)
            # fid_sca_toflx = FelixTestCase.fid_sca_toflx
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            cmd = shutil.which('felix-client-thread-sca-grouping', path='../felix-client:./felix-client')
            full_cmd = ' '.join((cmd, "--bus-dir", bus_dir, "--bus-group-name", group_name, iface, cid, did, iterations, sca_elink))
            print(full_cmd)
            output = subprocess.check_output(full_cmd, timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
