#!/usr/bin/env python3

import concurrent.futures
import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestToHostUnbuffered(FelixTestCase):

    def setUp(self):
        if FelixTestCase.hardware:
            print("hardware mode\n")
            os.system('feconf -s 6000 -S -I 40 -d 0 FM_Felixcore_3p3_CPU_test.elc ; fpepo -d 0 6630 2; femu -e')

        self.start('file2host-unbuffered', 'tohost-unbuffered')

    def tearDown(self):
        self.stop('file2host-unbuffered', 'tohost-unbuffered')

    def test_swrod(self):
        try:
            timeout = 100
            link = FelixTestCase.link
            msgs = "300000" if FelixTestCase.hardware else "500000"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            test = "./felix-test-swrod"
            subprocess.check_output(' '.join((test, "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", link, "-m", msgs, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def test_multi_swrod_fm(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.test_swrod)
        task_set[a] = "a"
        b = executor.submit(self.test_swrod)
        task_set[b] = "b"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))


if __name__ == '__main__':
    unittest.main()
