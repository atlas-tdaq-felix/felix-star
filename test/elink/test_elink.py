#!/usr/bin/env python3

import os
import time
import unittest

from felix_test_case import FelixTestCase


class TestElink(FelixTestCase):

    def setUp(self):
        self.start('fifo')
        self.start('fifo2elink')
        self.start('elink2file')

    def tearDown(self):
        self.stop('elink2file')
        self.stop('fifo2elink')
        self.stop('fifo')

    def test_fifo2elink_elink2file(self):
        # wait for all connections to be setup
        time.sleep(3)
        uuid = FelixTestCase.uuid
        fifo = "/tmp/felix-star_" + uuid + "_fifo.ndjson"
        out = '/tmp/felix-star_' + uuid + '_elink.ndjson'
        ref = '/tmp/felix-star_' + uuid + '_ref.ndjson'

        with open(ref, 'w') as f:
            f.write('{"fid": ' + str(int(FelixTestCase.vfid, 0)) + ', "message": "Hello Felix"}\n')

        with open(fifo, 'w') as f:
            f.write('{"fid": ' + str(int(FelixTestCase.vfid, 0)) + ', "message": "Hello Felix"}\n')
            f.write('{"fid": 2235595520556335104, "message": "Goodbye"}')

        time.sleep(30)
        self.assertFile(out, ref)

        os.remove(fifo)
        os.remove(out)
        os.remove(ref)


if __name__ == '__main__':
    unittest.main()
