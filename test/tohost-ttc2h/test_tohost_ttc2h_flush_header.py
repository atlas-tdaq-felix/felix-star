#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestToHostTtc2hFlushHeader(FelixTestCase):

    def setUp(self):
        self.start('file2host-ttc2h-block-header')

    def tearDown(self):
        self.stop('file2host-ttc2h-block-header')

    def test_swrod_ttc2h_block(self):
        try:
            timeout = 45
            ttc2h_elink = FelixTestCase.ttc2h_elink
            msgs = "300"
            buffers = "300"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-test-swrod", "--on-buffer", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-g", ttc2h_elink, "-m", msgs, "-n", buffers, '--did', did, '--cid', cid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
