#!/usr/bin/env python3

import concurrent.futures
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestToHostTTC2HFULL(FelixTestCase):

    def setUp(self):
        self.start('file2host-full-ttc2h')

    def tearDown(self):
        self.stop('file2host-full-ttc2h')

    def run_test_swrod_full(self):
        try:
            timeout = 30
            elink = FelixTestCase.link
            msgs = "800000"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            command = ' '.join(("./felix-test-swrod", "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-t", elink, "-m", msgs, '--did', did, '--cid', cid))
            print(command)
            subprocess.check_output(command, timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def run_test_swrod_ttc2h(self):
        try:
            timeout = 30
            msgs = "800000"
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            ttc2h_elink = FelixTestCase.ttc2h_elink
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            command = ' '.join(("./felix-test-swrod", "-v", "--verbose-bus", "--bus-dir", bus_dir, "--bus-groupname", group_name, ip, "-g", ttc2h_elink, "-m", msgs, '--did', did, '--cid', cid))
            print(command)
            subprocess.check_output(command, timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
            return e.returncode
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)

    def test_swrod_ttc2h_unbuf_full(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_test_swrod_full)
        task_set[a] = "full"
        b = executor.submit(self.run_test_swrod_ttc2h)
        task_set[b] = "ttc2h"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))


if __name__ == '__main__':
    unittest.main()
