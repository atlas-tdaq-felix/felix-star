#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestToFlxBuffered(FelixTestCase):

    def setUp(self):
        self.start('toflx2file-buffered')

    def tearDown(self):
        self.stop('toflx2file-buffered')

    def test_bus(self):
        try:
            fid_toflx = FelixTestCase.fid_toflx
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            output = subprocess.check_output(' '.join(("./felix-bus", "--verbose", "--bus-dir", bus_dir, "--bus-groupname", group_name, fid_toflx)), stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
