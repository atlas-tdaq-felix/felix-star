#!/usr/bin/env python3

import hashlib
import subprocess
import unittest

from felix_test_case import FelixTestCase
from felix_fid import FID


class TestToFlxBuffered(FelixTestCase):

    def setUp(self):
        self.start('toflx2file-buffered')

    def tearDown(self):
        self.stop('toflx2file-buffered')

    def test_send_b(self):
        try:
            iface = FelixTestCase.iface
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid

            did = int(FelixTestCase.did, 16)
            cid = int(FelixTestCase.cid, 16)
            sid = 0
            elink = 0x8
            is_virtual = 0
            is_to_flx = 1
            fid_toflx = str(hex(FID.get_fid(did, cid, elink, sid, is_to_flx, is_virtual)))

            # The message will be written to file encoded.
            # The hash depends on the message content, elink number and encoding format (PCIe generation).
            # The hash is independent from the network protocol.
            msg = "hellofelix"
            expected_hash = hex(0x4c70ffcaa9787884be49b02a9ec53be3)  # md5sum.

            cmd = ' '.join(('./felix-test-send-toflx', "--bus-dir", bus_dir, "--bus-groupname", group_name, iface, fid_toflx, msg))
            subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')

            filename = 'test/toflx/data-buffered-' + FelixTestCase.uuid
            output_hash = '0x'+hashlib.md5(open(filename, 'rb').read()).hexdigest()
            self.assertEqual(expected_hash, output_hash)

        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
