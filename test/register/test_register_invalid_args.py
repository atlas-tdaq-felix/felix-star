#!/usr/bin/env python3

import subprocess
import unittest

from felix_fid import FID
from felix_test_case import FelixTestCase


class TestFelixRegister(FelixTestCase):

    def setUp(self):
        self.start('felix-register')

    def tearDown(self):
        self.stop('felix-register')

    def test_felix_register(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            cmd_fid = FelixTestCase.cmd_fid
            ctrl_fid = FID.get_ctrl_fid(int(cmd_fid, 0))
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            output = subprocess.check_output(' '.join(("./felix-register-client",
                                                       "--bus-dir", bus_dir,
                                                       "--bus-groupname", group_name, ip, cmd_fid,
                                                       "--expect-reply", ":".join((str(ctrl_fid), "6", "0")),
                                                       "get", "REG_MAP_VERSION", "invalid_args")),
                                             timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
            self.assertTrue(False)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 6)   # ERROR_INVALID_ARGS
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
