#!/usr/bin/env python3

import subprocess
import unittest

from felix_fid import FID
from felix_test_case import FelixTestCase


class TestFelixRegisterRoute(FelixTestCase):

    def setUp(self):
        self.start('felix-register')

    def tearDown(self):
        self.stop('felix-register')

    def test_felix_register_route(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            cmd_fid = FelixTestCase.cmd_fid
            ctrl_fid = FID.get_ctrl_fid(int(cmd_fid, 0))
            alt_cmd_fid = FelixTestCase.alt_cmd_fid
            alt_ctrl_fid = FID.get_ctrl_fid(int(alt_cmd_fid, 0))
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            json_output = subprocess.check_output(' '.join(("./felix-register-client",
                                                            "--bus-dir", bus_dir,
                                                            "--bus-groupname", group_name, ip, cmd_fid, "--fid", alt_cmd_fid,
                                                            "--expect-reply", ":".join((str(ctrl_fid), "0", "0")),
                                                            "--expect-reply", ":".join((str(alt_ctrl_fid), "0", "0")),
                                                            "get", "TTC_DEC_CTRL_MASTER_BUSY")),
                                                  timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(json_output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertTrue(False)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
