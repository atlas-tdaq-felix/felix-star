#!/usr/bin/env python3

import concurrent.futures
import subprocess
import time
import unittest

from felix_fid import FID
from felix_test_case import FelixTestCase


class TestFelixRegister(FelixTestCase):

    def setUp(self):
        self.start('felix-register')

    def tearDown(self):
        self.stop('felix-register')

    def run_register_restart(self):
        time.sleep(10)
        self.stop('felix-register')
        time.sleep(10)
        self.start('felix-register')

    def run_test_felix_register(self):
        try:
            timeout = 60
            ip = FelixTestCase.ip
            cmd_fid = FelixTestCase.cmd_fid
            ctrl_fid = FID.get_ctrl_fid(int(cmd_fid, 0))
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + FelixTestCase.uuid
            cmd = ' '.join(("./felix-register-client",
                            "--bus-dir", bus_dir,
                            "--bus-groupname", group_name,
                            "--repeat", "40",
                            "--dont-expect-reply", ":".join((str(ctrl_fid), "3", "0")),
                            ip, cmd_fid,
                            "ecr_reset", "0x22"))
            print(cmd)
            output = subprocess.check_output(cmd, timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertTrue(False)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)

    def test_felix_register(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_test_felix_register)
        task_set[a] = "test"
        b = executor.submit(self.run_register_restart)
        task_set[b] = "restart"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))


if __name__ == '__main__':
    unittest.main()
