#!/usr/bin/env python3

import json
import os
import subprocess
import unittest

from felix_fid import FID
from felix_test_case import FelixTestCase


class TestFelixRegister(FelixTestCase):

    def setUp(self):
        self.start('felix-register')

    def tearDown(self):
        self.stop('felix-register')

    def test_felix_register(self):
        try:
            timeout = 60
            ip = FelixTestCase.ip
            cmd_fid = FelixTestCase.cmd_fid
            uuid = FelixTestCase.uuid
            mon_fid = FID.get_mon_fid(int(cmd_fid, 0))
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_STAR_" + uuid
            mon_output = "/tmp/felix-star_" + uuid + "_mon_output.ndjson"
            json_output = subprocess.check_output(' '.join(("./felix-register-client",
                                                            "--bus-dir", bus_dir,
                                                            "--bus-groupname", group_name,
                                                            "--expect-mon", str(5),
                                                            "--output-mon", mon_output,
                                                            ip, str(mon_fid)
                                                            )),
                                                  timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(json_output)
            os.remove(mon_output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)

            # read ndjson
            json_mon_output = []
            with open(mon_output) as f:
                for line in f:
                    json_mon_output.append(json.loads(line))
            os.remove(mon_output)

            json_ref_output = {
                "real_hostname": "pc-tbed-felix-07.cern.ch",
                "ip": "libfabric:" + ip,
                "timestamp": "Wed Jul 28 09:23:49 2021",
                "fpga":  {
                    "dna": "0x012007c004500580",
                    "core_temp": 57,
                    "fan_speed": 8917,
                    "clock": "LCLK",
                    "clk_lock": "YES"
                },
                "firmware":  {
                    "mode": "GBT",
                    "board_timestamp": "20/06/11 20:09",
                    "git_hash": "0x00000000d20ac176",
                    "reg_map_version": "4.0",
                    "gbt_alignment": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                }
            }
            self.assertJSON(json_ref_output, json_mon_output[2], exclude_paths=["root['real_hostname']", "root['hostname']", "root['timestamp']", "root['firmware']['reg_map_version']"])

            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            os.remove(mon_output)
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
