#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestIp(FelixTestCase):

    def test_ip(self):
        try:
            output = subprocess.check_output("./felix-get-ip " + TestIp.iface, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            self.assertRegex(output, r"\d+\.\d+\.\d+\.\d+")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
