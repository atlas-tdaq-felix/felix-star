#include <cstdio>
#include <cstdlib>
#include <argp.h>
#include <string>

#include "felix/felix_fid.h"
#include "felixtag.h"

char buf[80];
const char *argp_program_version = buf;
const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLXUSERS>";

/* Program documentation. */
static char doc[] = "FELIX FID conversion (accepts hex 0x, binary 0b and octal 0)";

/* A description of the arguments we accept. */
static char args_doc[] = "<felix id>\n<detector id> <connector id> <link id> <transport id> [<stream id>]";

static struct argp_option options[] = {
  {"cid",       'c', 0,         0,  "print Connector ID" },
  {"did",       'd', 0,         0,  "print Detector ID" },
  {"elink",     'e', 0,         0,  "print E-link" },
  {"hex",       'h', 0,         0,  "print in hex format" },
  {"lid",       'l', 0,         0,  "print Link ID" },
  {"co",        'o', 0,         0,  "print Connector Offset" },
  {"sid",       's', 0,         0,  "print Stream ID" },
  {"tid",       't', 0,         0,  "print Transport ID" },
  {"vid",       'i', 0,         0,  "print Version ID" },
  {"update",    'u', "VERSION", 0,  "Update Version ID. Default: 1" },
  {"verbose",   'v', 0,         0,  "Produce verbose output" },
  { 0 }
};

struct fid_config
{
  int verbose;
  int to_fid;
  unsigned long fid;

  unsigned long co;
  unsigned long gid;

  unsigned vid;
  unsigned did;
  unsigned cid;
  unsigned lid;
  unsigned tid;
  unsigned sid;

  unsigned vlink;
  unsigned elink;
  unsigned direction;
  unsigned protocol;

  char output;
  int hex;
};


/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
     know is a pointer to our config structure. */
  struct fid_config *cfg = reinterpret_cast<struct fid_config *>(state->input);
  char* end;

  switch (key)
    {
    case 'c':
    case 'd':
    case 'e':
    case 'i':
    case 'l':
    case 'o':
    case 's':
    case 't':
      cfg->output = key;
      break;

    case 'h':
      cfg->hex = 1;
      break;

    case 'v':
      cfg->verbose = 1;
      break;

    case 'u':
      cfg->vid = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "version id '%s' is not a number", arg);
      }
      if(cfg->vid >= 16) {
        argp_error(state, "version id '%s' should be between 0 and 15", arg);
      }
      break;

    case ARGP_KEY_ARG:
      switch(state->arg_num) {
        case 0: // fid OR did
          cfg->fid = strtol(arg, &end, 0);
          if(end==arg) {
            argp_error(state, "'%s' is not a number", arg);
          }
          break;
        case 1: // cid
          cfg->did = cfg->fid;
          cfg->fid = 0;
          cfg->to_fid = 1;
          cfg->cid = strtol(arg, &end, 0);
          if(end==arg) {
            argp_error(state, "'%s' is not a number", arg);
          }
          if(cfg->cid >= 65536) {
            argp_error(state, "connector id '%s' should be between 0 and 65535", arg);
          }
          if(cfg->did >= 256) {
            argp_error(state, "detector id '%d' should be between 0 and 255", cfg->did);
          }
          break;
        case 2: // lid
          cfg->lid = strtol(arg, &end, 0);
          if(cfg->lid >= 16384) {
            argp_error(state, "link id '%s' should be between 0 and 16383", arg);
          }
          if(end==arg) {
            argp_error(state, "'%s' is not a number", arg);
          }
          break;
        case 3: // tid
          cfg->tid = strtol(arg, &end, 0);
          if(cfg->tid >= 16384) {
            argp_error(state, "transport id '%s' should be between 0 and 16383", arg);
          }
          if(end==arg) {
            argp_error(state, "'%s' is not a number", arg);
          }
          break;
        case 4: // sid
          cfg->sid = strtol(arg, &end, 0);
          if(end==arg) {
            argp_error(state, "'%s' is not a number", arg);
          }
          if(cfg->sid >= 256) {
            argp_error(state, "stream id '%s' should be between 0 and 255", arg);
          }
          break;
        default:
          /* Too many arguments. */
          argp_usage (state);
          break;
      }
      break;

    case ARGP_KEY_END:
      if ((state->arg_num != 1) && (state->arg_num != 4) && (state->arg_num != 5))
        /* Not enough arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

void cli_parse(int argc, char **argv, struct fid_config* config)
{
  snprintf(buf, sizeof buf, "%s %s", argv[0], FELIX_TAG);

  // set default values
  config->to_fid = 0;
  config->vid = 1;
  config->verbose = 0;
  config->output = 'x';
  config->hex = 0;

  // parse arguments
  argp_parse (&argp, argc, argv, 0, 0, config);
}

struct fid_config config;

void print_long() {
  // int elink = elink_from_lid_and_tid(config.lid, config.tid);
  printf("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n");
  printf("|FID                                                            |\n");
  printf("|                                             0x%016lx|\n", config.fid);
  printf("|                                           %20lu|\n", config.fid);
  printf("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n");
  printf("|CO                         |GID                                |\n");
  printf("|                  0x%07lx|                        0x%09lx|\n", config.co, config.gid);
  printf("|                  %9lu|                        %11lu|\n", config.co, config.gid);
  printf("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n");
  printf("|VID|DID    |CID            |LID          |TID          |SID    |\n");
  printf("|0x%01x|   0x%02x|         0x%04x|       0x%04x|       0x%04x|   0x%02x|\n",
    config.vid, config.did, config.cid, config.lid, config.tid, config.sid);
  printf("| %2u|    %3u|          %5u|        %5u|        %5u|    %3u|\n",
    config.vid, config.did, config.cid, config.lid, config.tid, config.sid);
  printf("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n");
  printf("|VID|DID    |CID            |V Elink            |D Proto|SID    |\n");
  printf("|0x%01x|   0x%02x|         0x%04x|%01x           0x%05x|%01x  0x%02x|   0x%02x|\n",
    config.vid, config.did, config.cid, config.vlink, config.elink, config.direction, config.protocol, config.sid);
  printf("| %2u|    %3u|          %5u|%1u           %7u|%1u   %3u|    %3u|\n",
    config.vid, config.did, config.cid, config.vlink, config.elink, config.direction, config.protocol, config.sid);
  printf("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n\n");
  printf("FID:   (64 bits) - Felix ID \n");
  printf("CO:    (28 bits) - Connector Offset \n");
  printf("GID:   (36 bits) - Generic ID \n");
  printf("VID:   (4 bits)  - Version ID \n");
  printf("DID:   (8 bits)  - Detector ID \n");
  printf("CID:   (16 bits) - Connector ID \n");
  printf("LID:   (14 bits) - Link ID \n");
  printf("TID:   (14 bits) - Transport ID \n");
  printf("SID:   (8 bits)  - Stream ID \n");
  printf("V:     (1 bit)   - Virtual\n");
  printf("Elink: (19 bits) - As used in the FELIX card \n");
  printf("D:     (1 bit)   - Direction [0=tohost, 1=toflx] \n");
  printf("Proto: (7 bits)  - Protocol \n");
}

// Main
int main(int argc, char** argv)
{
  cli_parse(argc, argv, &config);

  if (config.to_fid) {
    // Convert to FID
    felix_co_1_t connector_offset;
    connector_offset.b.vid = config.vid;
    connector_offset.b.did = config.did;
    connector_offset.b.cid = config.cid;
    uint8_t to_flx = is_to_flx_from_tid(config.tid);
    uint8_t virtual_fid = is_virtual_from_lid(config.lid);
    felix_id_1_t felix_id;
    felix_id.fid = get_fid(connector_offset.co, get_elink_from_ids(config.lid, config.tid, config.vid), config.sid, config.vid, to_flx, virtual_fid);
    config.fid = felix_id.fid;
    config.co = felix_id.o.co;
    config.gid = felix_id.o.gid;
    config.vid = felix_id.b.vid;
    config.did = felix_id.b.did;
    config.cid = felix_id.b.cid;
    config.lid = felix_id.b.lid;
    config.tid = felix_id.b.tid;
    config.sid = felix_id.b.sid;
    config.vlink = felix_id.e.vlink;
    config.elink = felix_id.e.elink;
    config.direction = felix_id.e.direction;
    config.protocol = felix_id.e.protocol;
    if (config.verbose) {
      print_long();
    } else {
      printf(config.hex ? "0x%lx" : "%ld", config.fid);
    }
  } else {
    // Convert from FID
    felix_id_1_t felix_id;
    felix_id.fid = config.fid;
    config.co = felix_id.o.co;
    config.gid = felix_id.o.gid;
    config.vid = felix_id.b.vid;
    config.did = felix_id.b.did;
    config.cid = felix_id.b.cid;
    config.lid = felix_id.b.lid;
    config.tid = felix_id.b.tid;
    config.sid = felix_id.b.sid;
    config.vlink = felix_id.e.vlink;
    config.elink = felix_id.e.elink;
    config.direction = felix_id.e.direction;
    config.protocol = felix_id.e.protocol;
    if (config.verbose) {
      print_long();
    } else {
      switch (config.output) {
        case 'c':
          printf(config.hex ? "0x%x": "%d", config.cid);
          break;
        case 'd':
          printf(config.hex ? "0x%x": "%d", config.did);
          break;
        case 'e':
          printf(config.hex ? "0x%x": "%d", config.elink);
          break;
        case 'i':
          printf(config.hex ? "0x%x": "%d", config.vid);
          break;
        case 'l':
          printf(config.hex ? "0x%x": "%d", config.lid);
          break;
        case 'o':
          printf(config.hex ? "0x%lx": "%ld", config.co);
          break;
        case 's':
          printf(config.hex ? "0x%x": "%d", config.sid);
          break;
        case 't':
          printf(config.hex ? "0x%x": "%d", config.tid);
          break;
        case 'x':
        default:
          printf(config.hex ? "0x%01x-%02x-%04x-%01x-%05x-%01x-%02x-%02x" : "%02d-%03d-%05d-%01d-%07d-%01d-%03d-%03d", config.vid, config.did, config.cid, config.vlink, config.elink, config.direction, config.protocol, config.sid);
          break;
      }
    }
  }

  return 0;
}
