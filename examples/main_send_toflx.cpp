#include <cstdio>
#include <string>
#include <thread>
#include <functional>
#include <iostream>
#include <unistd.h>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-test-send-toflx - Sends msg to a felix-id using felix-bus to find the publisher.

    Usage:
      felix-test-send-toflx [options] <local_ip_or_interface> <fid> <msg>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, notice, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-groupname=<group-name>      Set group for bus to use [default: FELIX]
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --sleep=<seconds>                 Sleep after sending [default: 0]
      --size=<S>                        Send a message of S bytes obtained repeating msg [default: 0]
      --repeat=<N>                      Send the message N times [default: 1]
)";

void on_init() {
    printf("on_init called\n");
}

void on_connect(uint64_t fid) {
    printf("on_connect called 0x%lx\n", fid);
}

void on_disconnect(uint64_t fid) {
    printf("on_disconnect called 0x%lx\n", fid);
}

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    FelixClientThread::Config config;
    config.on_init_callback = on_init;
    config.on_connect_callback = on_connect;
    config.on_disconnect_callback = on_disconnect;

    uint64_t fid;
    std::string msg;
    char* c_msg;
    size_t msg_size = 0;
    int repeat = 1;

    try {
        config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
        config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
        config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
        config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-groupname"].asString();
        config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";

        fid = args["<fid>"].asLong();
        msg = args["<msg>"].asString();
        msg_size = args["--size"].asLong();
        repeat = args["--repeat"].asLong();
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl;
        std::cout << std::endl;
        std::cout << USAGE << std::endl;
        return -1;
    }

    FelixClientThread* client = new FelixClientThread(config);
    bool free_msg = false;

    if (msg_size) {
        c_msg = reinterpret_cast<char*>(malloc(msg_size));
        free_msg = true;
        for(unsigned int i=0; i<msg_size; ++i) {
            unsigned int j = i % msg.size();
            c_msg[i] = msg[j];
        }
    }
    else {
        c_msg = const_cast<char*>(msg.c_str());
        msg_size = msg.size();
    }

    for (int r=0; r < repeat; ++r) {
        try {
            // NOTE: send as real data, without trailing 0, as toflx2file will decode and add 0 to print
            client->send_data(fid, reinterpret_cast<const uint8_t*>(c_msg), msg_size, true);
        } catch (FelixClientResourceNotAvailableException& error) {
            printf("Failed to send message number %d. Retrying \n", r);
            --r;
        }
    }

    int sleep_time = args["--sleep"].asLong();
    if (sleep_time > 0) {
        printf("Sleeping %d s\n", sleep_time);
        sleep(sleep_time);
    }
    if (free_msg) {
      free(c_msg);
    }
    printf("Normal exit\n");
    delete client;
    return 0;
}
