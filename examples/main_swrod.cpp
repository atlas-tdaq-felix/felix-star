#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <ctime>
#include <unistd.h>
#include <csignal>

#include "docopt/docopt.h"

#include "block.hpp"
#include "felix/felix_fid.h"
#include "netio/netio.h"

#include "felix/felix_client.hpp"
#include "felix/felix_client_util.hpp"
#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread.hpp"
#include "felixtag.h"

#include "src/log.hpp"
#include "l0id_decoder.hpp"


static const char USAGE[] =
R"(felix-test-swrod - FELIX test SWROD application (using felixbus)

    Usage: felix-test-swrod [options --fid=<fid>... --tag=<tag>... --vtag=<vtag>...] <local-ip>

    Options:
        --bus-dir=<directory>     Set directory for bus to use. [default: ./bus]
        --bus-groupname=<group>   Set group for bus to use. [default: FELIX]
        --cid=<cid>               CID (Connector Id) to set in FID (Felix ID) [default: 0],
                                  incompatible with --fid and --co.
        --co=<co>                 CO (Connector Offset) to offset FID (Felix ID) [default: 0],
                                  incompatible with --fid, --did and --cid.
        --did=<did>               DID (Detector Id) to set in FID (Felix ID) [default: 0],
                                  incompatible with --fid and --co.
        --dump                    Dump data to stdout
        --dump-file=<path>        Dump data to binary file with filename. Format (data_size, elink, data)
        --size-check=<size>       Check that all messages (except TTC2H) have the expected size in bytes [default: 0]
        --l1id-check=<format>     Check L1ID sequentiality. Formats: 1=TTC2H only, 2=LATOME, 3=FMEMU, 4=FELIG, 5=NSWVMM, 6=NSWTP [default: 0]
        --fm                      Use Full Mode elinks/fids to subscribe. Default: GBT elinks/fids
        --on-buffer               Use on_buffer callback instead of on_message (advanced feature, incompatible with --thread)
        -f --fid=<fid>...         Subscribe to the given FID. This option may be used multiple times (2048 max) to
                                  subscribe to multiple FIDs. Default: some pre-defined FID list.
                                  Incompatible with --vid, --co, --cid and --did.
        -g --vtag=<vtag>...       Subscribe to the given virtual tag (virtual elink). This option may be used
                                  multiple times (2048 max) to subscribe to multiple virtual tags.
                                  Incompatible with --fid.
        -m --msg=<number>         Exit after N messages are received. [default: 0]
        -n --buf=<number>         Exit after M messages and N buffers are received. [default: 0]
        --streams                 Use streams. Incompatible with --fid.
        --hang=<N>                Wait for N us in on_data callback [default: 0]
        -t --tag=<tag>...         Subscribe to the given tag (elink). This option may be used multiple times
                                  (2048 max) to subscribe to multiple tags. Default: some pre-defined tag list.
                                  Incompatible with --fid.
        --thread                  Use felix-client-thread (i.e. felix-client via shared library) instead of felix-client (compiled against)
        --timeout=<ms>            Timeout (in ms) on initial connection (only in --thread mode), [default: 0]
        -v --verbose              Produce verbose output
        --verbose-bus             Produce verbose bus output
        --vid=<vid>               VID (Version Id) to set in FID (Felix ID), [default: 1],
                                  incompatible with --fid and --co.
        -h --help                 Give this help list
        --usage                   Give a short usage message
        -V --version              Print program version

    Report bugs to <https://its.cern.ch/jira/projects/FLXUSERS>
)";

// ------------------------------------------------------
// Global variables, functions and function prototypes
//-------------------------------------------------------

char timestamp[64];
time_t ltime;
struct tm* tm_info;

void update_timestamp(){
  ltime=time(NULL);
  tm_info=localtime(&ltime);
  strftime(timestamp, 64, "%Y-%m-%d %H-%M-%S", tm_info);
}

void take_l1id_snapshot(xl1id_t &);

// ------------------------------------------------------
// Data structures
//-------------------------------------------------------

struct elink_stats {
    uint64_t fid;
    uint64_t received;
    uint64_t truncated;
    uint64_t error;
    uint64_t crc;
    uint64_t received_buffers;
    uint64_t received_in_last_buffer;
    xl1id_t snapshot_xl1id;
    uint32_t mask;
    int ttc2h_ec_difference;
    std::unique_ptr<L0Decoder> l0check;
    elink_stats(uint64_t f, int l0id_fmt) : fid(f), received(0), truncated(0), error(0), crc(0), 
        received_buffers(0), received_in_last_buffer(0),
        mask(0), ttc2h_ec_difference(0),
        l0check(std::make_unique<L0Decoder>(L0Decoder(f, l0id_fmt)))
    {
      snapshot_xl1id.xl1id = 0x00ffffff;
    }
};

struct {
    struct timespec t0;
    struct timespec t_printout;
    uint64_t total_messages_received_t0;
    uint64_t total_messages_received;
    uint64_t messages_received;
    uint64_t bytes_received;
    uint64_t total_buffers_received;
    std::map<uint64_t, elink_stats> fid_stats;
} stats;


struct swrod_config {
  std::string local_hostname;
  std::string bus_dir;
  std::string bus_group;

  bool thread;
  bool timeout;
  bool buffer_cb;
  bool verbose;
  bool verbose_bus;
  uint32_t co, did, cid;
  uint8_t vid;
  bool dump;
  bool full_mode;
  bool streams;

  std::vector<netio_tag_t> elinks;
  std::vector<uint8_t> is_virtual;
  std::vector<netio_tag_t> fids;

  uint64_t expected_messages;
  uint64_t expected_buffers;
  std::string dump_path;
  FILE* dump_file;

  int l1id_check;
  uint64_t size_check;
  uint32_t hang;

  // NOTE: one or the other
  FelixClient* client = nullptr;
  FelixClientThread* client_thread = nullptr;
} config;

int return_value = 0;

// ------------------------------------------------------
// Configuration
//-------------------------------------------------------

void cli_parse(std::map<std::string, docopt::value> args, struct swrod_config* config)
{
  config->thread = args["--thread"].asBool();
  config->timeout = args["--timeout"].asLong();
  config->buffer_cb = args["--on-buffer"].asBool();
  config->local_hostname = args["<local-ip>"].asString();
  config->bus_dir = args["--bus-dir"].asString();
  config->bus_group = args["--bus-groupname"].asString();
  config->verbose = args["--verbose"].asBool();
  config->verbose_bus = args["--verbose-bus"].asBool();
  config->co = args["--co"].asLong();
  config->did = args["--did"].asLong();
  config->cid = args["--cid"].asLong();
  config->vid = args["--vid"].asLong();
  config->dump = args["--dump"].asBool();
  config->full_mode = args["--fm"].asBool();
  config->streams = args["--streams"].asBool();
  config->expected_messages = args["--msg"].asLong();
  config->expected_buffers = args["--buf"].asLong();
  config->dump_path = args["--dump-file"] ? args["--dump-file"].asString() : "";
  config->dump_file = NULL;

  config->l1id_check = args["--l1id-check"].asLong();
  config->size_check = args["--size-check"].asLong();
  config->hang = args["--hang"].asLong();

  // calculated parts
  if (config->co == 0) {
    config->co = get_co_from_ids(config->did, config->cid, config->vid);
  } else {
    config->did = get_did_from_co(config->co, config->vid);
    config->cid = get_cid_from_co(config->co, config->vid);
  }

  // copy fids
  if (args["--fid"]) {
    for (auto fid : args["--fid"].asStringList()) {
      uint64_t f = std::stol(fid, nullptr, 0);
      config->fids.push_back(f);
    }
  }

  // copy elinks
  if (args["--tag"]) {
    for (auto elink : args["--tag"].asStringList()) {
      uint64_t e = std::stol(elink, nullptr, 0);
      config->elinks.push_back(e);
      config->is_virtual.push_back(0);
    }
  }

  // copy vlinks
  if (args["--vtag"]) {
    for (auto vlink : args["--vtag"].asStringList()) {
      uint64_t v  = std::stol(vlink, nullptr, 0);
      config->elinks.push_back(v);
      config->is_virtual.push_back(1);
    }
  }

  if (config->fids.size() == 0) {
    // set default elinks
    if (config->elinks.size() == 0) {
      netio_tag_t gbt_elinks[] = {
        0x008, 0x009, 0x00a, 0x00b, 0x00c, 0x00d, 0x00e, 0x00f,
        0x048, 0x049, 0x04a, 0x04b, 0x04c, 0x04d, 0x04e, 0x04f,
        0x088, 0x089, 0x08a, 0x08b, 0x08c, 0x08d, 0x08e, 0x08f,
        0x0c8, 0x0c9, 0x0ca, 0x0cb, 0x0cc, 0x0cd, 0x0ce, 0x0cf,
        0x108, 0x109, 0x10a, 0x10b, 0x10c, 0x10d, 0x10e, 0x10f,
        0x148, 0x149, 0x14a, 0x14b, 0x14c, 0x14d, 0x14e, 0x14f,
        0x188, 0x189, 0x18a, 0x18b, 0x18c, 0x18d, 0x18e, 0x18f,
        0x1c8, 0x1c9, 0x1ca, 0x1cb, 0x1cc, 0x1cd, 0x1ce, 0x1cf,
        0x208, 0x209, 0x20a, 0x20b, 0x20c, 0x20d, 0x20e, 0x20f,
        0x248, 0x249, 0x24a, 0x24b, 0x24c, 0x24d, 0x24e, 0x24f,
        0x288, 0x289, 0x28a, 0x28b, 0x28c, 0x28d, 0x28e, 0x28f,
        0x2c8, 0x2c9, 0x2ca, 0x2cb, 0x2cc, 0x2cd, 0x2ce, 0x2cf,
        0x33b};
      netio_tag_t fm_elinks[] = { 0x000, 0x040, 0x080, 0x0c0, 0x100, 0x140, 0x33b};

      unsigned default_num_elinks = config->full_mode ? sizeof(fm_elinks)/sizeof(fm_elinks[0]) : sizeof(gbt_elinks)/sizeof(gbt_elinks[0]);
      netio_tag_t* default_elinks = config->full_mode ? &fm_elinks[0] : &gbt_elinks[0];

      for(unsigned i=0; i<default_num_elinks; i++) {
        config->elinks.push_back(default_elinks[i]);
        config->is_virtual.push_back(0);
      }
    }

    // if disabled use first stream (0)
    uint8_t default_streams[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
    unsigned no_of_streams = config->streams ? sizeof(default_streams)/sizeof(default_streams[0]) : 1;

    // copy to fids, including streams
    for (unsigned i=0; i < config->elinks.size(); ++i) {
      for(unsigned j=0; j < no_of_streams; j++) {
        uint8_t to_flx = 0;
        uint8_t virt = config->is_virtual.at(i);
        uint64_t new_fid = get_fid(config->co, config->elinks.at(i), default_streams[j], config->vid, to_flx, virt);
        config->fids.push_back(new_fid);
      }
    }
  }

}

// ------------------------------------------------------
// Callbacks
// ------------------------------------------------------

void on_stats();

void on_init() {
    LOG_INFO("on_init called");
    clock_gettime(CLOCK_MONOTONIC_RAW, &stats.t0);
    if (config.client) {
      config.client->user_timer_init();
      config.client->callback_on_user_timer(on_stats);
    }
    for(auto f : config.fids){
        int fmt = 0;
        if (config.l1id_check){
            is_ttc2h_elink(f) ? fmt = 1 : fmt = config.l1id_check;
        }
        stats.fid_stats.emplace(f, elink_stats(f, fmt));
    }
}

void on_connect(netio_tag_t fid) {
    LOG_INFO("on_connect called for fid 0x%lx", fid);
    if (config.client) {
      config.client->user_timer_start(1000);
    }
}

void on_disconnect(netio_tag_t fid) {
    LOG_INFO("on_disconnect called for fid 0x%lx", fid);
}

void on_stats(){
    if(stats.total_messages_received == stats.total_messages_received_t0){
        return;
    }
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
    double seconds = t1.tv_sec - stats.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - stats.t0.tv_nsec);
    update_timestamp();
    printf("{\"TS\": \"%s\", \"Gbps\": %.2f, \"kHz\": %.2f, \"stats\": [",
          timestamp,
          stats.bytes_received*8/1000./1000./1000./seconds,
          stats.messages_received/1000./seconds);
    size_t idx = 0;
    for(auto & e : stats.fid_stats){
      uint64_t fid = e.first;
      printf("{ \"fid\": \"0x%lx\", \"link\": \"0x%x\", \"chk\": %lu, \"t\": %lu, \"e\": %lu, \"crc\": %lu", fid,  get_elink(fid), e.second.received, e.second.truncated, e.second.error, e.second.crc);
      if(config.buffer_cb == true){
        double pu = (double)e.second.received/(double)e.second.received_buffers;
        double avg_pu = (double)e.second.received/(double)stats.total_buffers_received;
        printf(", \"pu\": %f, \"nbuf\": %lu, \"avg_pu\": %f", pu, e.second.received_buffers, avg_pu);
        printf(", \"l1id\": \"0x%08x\", \"EC diff\": %d, \"mask\": \"0x%x\"",e.second.snapshot_xl1id.xl1id, e.second.ttc2h_ec_difference, e.second.mask);
      }
      if(idx != stats.fid_stats.size()-1 && stats.fid_stats.size() > 1 ){
        printf("},");
      } else{
        printf("]}\n");
      }
      ++idx;
    }
    fflush(stdout);
    stats.bytes_received = 0;
    stats.messages_received = 0;
    stats.t0 = t1;
    stats.total_messages_received_t0 = stats.total_messages_received;
}

void on_data(netio_tag_t fid, const uint8_t* data, size_t size, uint8_t status) {

    stats.total_messages_received++;
    stats.messages_received++;
    stats.bytes_received += size + 1;
    stats.fid_stats.at(fid).received++;

    // error flags
    if((status & CHUNK_FW_TRUNC) || (status & CHUNK_SW_TRUNC)){
      stats.fid_stats.at(fid).truncated++;
    }
    if(status & CHUNK_FW_CRC){
      stats.fid_stats.at(fid).crc++;
    }
    if((status & CHUNK_FW_MALF) || (status & CHUNK_SW_MALF)){
      stats.fid_stats.at(fid).error++;
    }

    if (config.size_check && !is_ttc2h_elink(fid)){
      if (size != config.size_check){
        LOG_ERR("Message from fid 0x%lx has size %lu, expected %lu", fid, size, config.size_check);
      }
    }

    if (config.expected_messages > 0 && (stats.total_messages_received >= config.expected_messages)) {
      LOG_INFO("Expected messages received");
      on_stats();
      if (config.client) {
        config.client->stop();
      } else {
        delete config.client_thread;
      }

      if(config.expected_buffers == 0 || config.expected_buffers == stats.total_buffers_received){
        throw std::runtime_error("42");
      } else{
        LOG_ERR("Number of buffer received %lu, expected %lu", stats.total_buffers_received, config.expected_buffers);
        throw std::runtime_error("1");
      }
    }

    if(config.l1id_check > 0){
      stats.fid_stats.at(fid).l0check->check_sequence_error(data, size);
    }

    uint32_t elink = get_elink(fid);
    if(config.dump > 0){
        LOG_INFO("Message from 0x%lx (0x%x), length %zu", fid, elink, size + 1);
        printf("%02X ", status);
        for(unsigned int b=0; b<size; ++b){
            printf("%02X ", data[b]);
        }
        printf("\n");
        return;
    }

    if(config.dump_file != NULL) {
      uint32_t lentowrite = (uint32_t)size;
      uint32_t elinktowrite = elink;
      fwrite(&lentowrite, sizeof(uint32_t), 1, config.dump_file);
      fwrite(&elinktowrite, sizeof(uint32_t), 1, config.dump_file);
      fwrite(data, sizeof(uint8_t), size, config.dump_file);
      return;
    }

    if(config.hang){
        usleep(config.hang);
    }
    //no user timer in felix-client-thread = no on_stats call
    //less precise packet count
    if(config.client_thread){
      struct timespec t1;
      clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
      double seconds = t1.tv_sec - stats.t0.tv_sec + 1e-9*(t1.tv_nsec - stats.t0.tv_nsec);
      if (seconds < 1.0) {
        return;
      }

      printf("data rate: %.2f Gb/s   message rate: %.2f kHz     ",
        stats.bytes_received*8/1000./1000./1000./seconds,
        stats.messages_received/1000./seconds);

      stats.bytes_received = 0;
      stats.messages_received = 0;
      stats.t0 = t1;

      printf("FID, elink, chunks, trunc, err, crc: { ");
      size_t idx = 0;
      for(const auto & e : stats.fid_stats){
        printf("0x%lx:0x%x:%lu:%lu:%lu:%lu ", e.first, get_elink(e.first), e.second.received, e.second.truncated, e.second.error, e.second.crc);
        if (idx>=8) {
          printf("... ");
          break;
        }
        ++idx;
      }
      printf("}\n");
    }
}

void on_buf_received(struct netio_buffer* buf, size_t len)
{
  stats.total_buffers_received++;
  for(auto f : config.fids){
    stats.fid_stats.at(f).received_in_last_buffer = 0;
  }

  size_t pos = 0;
  uint32_t messages_in_this_buffer=0;
  uint8_t* buffer_data = reinterpret_cast<uint8_t*>(buf->data);

  while(pos < len) {
    uint32_t* msg_size_ptr = reinterpret_cast<uint32_t*>(buffer_data + pos);
    pos += sizeof(uint32_t);
    netio_tag_t fid = *reinterpret_cast<netio_tag_t*>(buffer_data + pos);
    pos += sizeof(uint64_t);
    uint8_t status = *(buffer_data + pos);
    pos += sizeof(uint8_t);
    size_t msg_size = *msg_size_ptr - sizeof(netio_tag_t) - sizeof(uint8_t);
    if (stats.fid_stats.find(fid) != stats.fid_stats.end()){
      stats.fid_stats.at(fid).received_buffers++;
      stats.fid_stats.at(fid).received_in_last_buffer++;
      uint8_t* data = reinterpret_cast<uint8_t*>(buffer_data + pos);
      //TTC2H vs DATA L1ID comparison
      if (messages_in_this_buffer == 0 && is_ttc2h_elink(fid) == 1){
        xl1id_t ttc2hl1id;
        ttc2hl1id.xl1id = reinterpret_cast<uint32_t*>(data)[1];
        take_l1id_snapshot(ttc2hl1id);
      }
      on_data(fid, data, msg_size, status);

      ++messages_in_this_buffer;
    } else {
      LOG_ERR("Buffer contains message from fid 0x%lx status %u size %lu", fid, status, msg_size);
    }
    pos += msg_size;
  }
  if (config.verbose == true){
    LOG_INFO("Buffer %lu contains:", stats.total_buffers_received);
    for(auto f : config.fids){
      if (stats.fid_stats.at(f).received_in_last_buffer > 0){
        printf("fid 0x%lx msg %lu\n", f, stats.fid_stats.at(f).received_in_last_buffer);
      }
    }
  }
}

void take_l1id_snapshot(xl1id_t& ttc2hl1id)
{
  for(auto f : config.fids){
    elink_stats* fid_stats = &stats.fid_stats.at(f);
    fid_stats->snapshot_xl1id = fid_stats->l0check->get_last();
    fid_stats->mask = fid_stats->l0check->get_ec_mask();
    fid_stats->ttc2h_ec_difference = (int)(fid_stats->mask & ttc2hl1id.fields.ec) - (int)(fid_stats->mask & fid_stats->snapshot_xl1id.fields.ec);
  }
}


// ------------------------------------------------------
// Create felix-client
// ------------------------------------------------------

int felix_client()
{
    try {
        setbuf(stdout, NULL);

        unsigned log_level = get_log_level(config.verbose ? "debug" : "info");

        config.client = new FelixClient(
            config.local_hostname,
            config.bus_dir,
            config.bus_group,
            log_level,
            config.verbose_bus
        );

        config.client->callback_on_init(on_init);
        config.client->callback_on_connect(on_connect);
        config.client->callback_on_disconnect(on_disconnect);
        if(config.buffer_cb == true){
          config.client->enable_buffer_callback();
          config.client->callback_on_buffer(on_buf_received);
        } else {
          config.client->callback_on_data(on_data);
          if(return_value){
            return return_value;
          }
        }

        for( auto f : config.fids ){
          config.client->subscribe(f, config.timeout);
        }

        config.client->run();

    } catch (std::invalid_argument const& err) {
        LOG_ERR("Argument or option of wrong type");
        return_value = -1;

    }
    return return_value;
}


// ------------------------------------------------------
// Create felix-client-thread
// ------------------------------------------------------

int felix_client_thread()
{
    try {
        setbuf(stdout, NULL);

        FelixClientThread::Config client_config;
        client_config.on_init_callback = on_init;
        client_config.on_data_callback = on_data;
        client_config.on_connect_callback = on_connect;
        client_config.on_disconnect_callback = on_disconnect;

        client_config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = config.local_hostname;
        client_config.property[FELIX_CLIENT_LOG_LEVEL] = config.verbose ? "debug" : "info";
        client_config.property[FELIX_CLIENT_BUS_DIR] = config.bus_dir;
        client_config.property[FELIX_CLIENT_BUS_GROUP_NAME] = config.bus_group;
        client_config.property[FELIX_CLIENT_VERBOSE_BUS] = config.verbose_bus ? "True" : "False";
        client_config.property[FELIX_CLIENT_TIMEOUT] = config.timeout;

        config.client_thread = new FelixClientThread(client_config);

        for(auto f : config.fids) {
            config.client_thread->subscribe(f);
        }

        while(return_value==0) {
            sleep(1);
        }

        delete config.client_thread;


    } catch (FelixClientResourceNotAvailableException& exc) {
        LOG_ERR("Timed out on subscription");
        return_value = 1;
    } catch (std::invalid_argument const& err) {
        LOG_ERR("Argument or option of wrong type");
        return_value = -1;
    }
    return return_value;
}

// ------------------------------------------------------
// Signal handler
// ------------------------------------------------------
void signalHandler(int signal){
	if(signal==SIGINT){
		LOG_INFO("Signal Ctrl-C received");
		if(config.dump_file != NULL) {
      fclose(config.dump_file);
    }
    throw std::runtime_error("SIGINT");
	}
}

// ------------------------------------------------------
// Main function
// ------------------------------------------------------

int main(int argc, char** argv)
{
  struct sigaction action, oldAction;
  action.sa_handler = signalHandler;
  sigemptyset(&action.sa_mask); // empty the list of signals
  action.sa_flags = SA_RESTART;
  sigaction(SIGINT, &action, &oldAction);

  std::map<std::string, docopt::value> args
      = docopt::docopt(USAGE,
                        { argv + 1, argv + argc },
                        true,               // show help if requested
                        (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

  try {
    cli_parse(args, &config);
  } catch (std::invalid_argument const& err) {
    LOG_ERR("Argument or option of wrong type");
    std::cout << USAGE << std::endl;
    return -1;
  }

  stats.total_messages_received_t0 = 0;
  stats.total_messages_received = 0;
  stats.messages_received = 0;
  stats.bytes_received = 0;
  stats.total_buffers_received = 0;

  if (config.dump_path != "") {
    config.dump_file = fopen(config.dump_path.c_str(), "wb");
    if (config.dump_file == NULL) {
      LOG_ERR("error opening file %s", config.dump_path.c_str());
      return 1;
    }
  }
  try{
    if (config.thread) {
      LOG_INFO("Running felix-client-thread");
      felix_client_thread();
    } else {
      LOG_INFO("Running felix-client");
      felix_client();
    }
  }
  catch(std::runtime_error const& e){
    if(std::strcmp(e.what(), "42") == 0){
      return 42;
    }else if(std::strcmp(e.what(), "1") == 0){
      return 1;
    }else if(std::strcmp(e.what(), "SIGINT") == 0){
      return 1;
    }
  }
  return return_value;
}
