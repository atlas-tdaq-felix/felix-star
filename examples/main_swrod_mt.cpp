#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <string>
#include <cmath>
#include <iostream>
#include <tuple>
#include <unistd.h>
#include <vector>
#include <map>
#include <atomic>
#include <thread>
#include <chrono>
#include <mutex>
#include <ctime>
#include <algorithm>
#include <condition_variable>

#include "docopt/docopt.h"

#include "felix/felix_fid.h"
#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread.hpp"

#include "block.hpp"
#include "log.hpp"
#include "l0id_decoder.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-test-swrod-mt - FELIX test SWROD application (using felixbus).

    Usage: felix-test-swrod-mt [options] <local_ip_or_interface> <thread_fids>...

    Options:
        --bus-dir=<directory>     Set directory for bus to use. [default: ./bus]
        --bus-groupname=<group>   Set group for bus to use. [default: FELIX]
        --dump-file=<path>        Dump data to binary file with filename. Format (data_size, elink, data)
        --size-check=<size>       Check that all messages (except TTC2H) have the expected size in bytes [default: 0]
        --l1id-check=<format>     Check L1ID sequentiality. Formats: 1=TTC2H only, 2=LATOME, 3=FMEMU, 4=FELIG, 5=NSWVMM, 6=NSWTP [default: 0]
        --log-level=<loglevel>    Specify level of logging (trace, debug, info, notice, warning, error, fatal) [default: warning]
        -m --msg=<number>         Exit after N messages are received. [default: 0]
        --hang=<N>                Wait for N us in on_data callback [default: 0]
        --timeout=<ms>            Subscription timeout (in ms) [default: 0]
        -v --verbose              Produce verbose output
        --verbose-bus             Produce verbose bus output
        -h --help                 Give this help list
        --usage                   Give a short usage message
        -V --version              Print program version

    Report bugs to <https://its.cern.ch/jira/projects/FLXUSERS>
    Example:

        felix-test-swrod-mt --bus-dir /tmp/bus priv0 0:0x1000000000000000 1:0x1000000000400000 2:0x1000000000800000

    The numbers preceding the fids identify the thread to be used for the subscriptions.
    The total number of thread is determined by the largest thread idenfier passed.
    A thread can subscribe to multiple fids, e.g. passing 0:0x1000000000000000 0:0x1000000000400000.
    Multiple thread can subscribe to the same fid e.g. 0:0x1000000000400000 0:0x1000000000400000.
)";

std::map<std::string, docopt::value> args;
std::atomic<int> running_threads;
std::atomic<bool> last_msg_was_ttc2h;
int l1id_check_fmt;
struct timespec t0;
uint64_t reference_ttc2h_fid;
uint64_t processing_time_us;

void take_l1id_snapshot(xl1id_t& ttc2hl1id);

struct elink_stats {
    uint64_t fid;
    uint64_t received;
    uint64_t truncated;
    uint64_t error;
    uint64_t crc;
    uint32_t mask;
    int ttc2h_ec_difference;
    std::unique_ptr<L0Decoder> l0check;
    xl1id_t snapshot_xl1id;
    
    elink_stats(uint64_t f, int l0id_fmt) 
        : fid(f), received(0), truncated(0), error(0), crc(0),
          mask(0xffffffff), ttc2h_ec_difference(0), l0check(new L0Decoder(f, l1id_check_fmt))
    {
        snapshot_xl1id.xl1id = 0x00ffffff;
    }

    elink_stats() 
        : fid(0), received(0), truncated(0), error(0), crc(0),
          mask(0xffffffff), ttc2h_ec_difference(0){ }
};

struct thread_stats {
    uint64_t total_messages_received_prev{0};
    uint64_t total_messages_received{0};
    uint64_t messages_received{0};
    uint64_t bytes_received{0};
    std::map<uint64_t, elink_stats> fid_stats{};
};


class ElinkClient {

public:
    ElinkClient(const std::vector<uint64_t>& fids, int thread_no) {

        try {
            LOG_INFO("Starting client %d", thread_no);
            m_thread_no = thread_no;
            m_fids = fids;
            m_connected_fids = 0;
            m_max_msg = args["--msg"].asLong();
            FelixClientThread::Config config;
            config.on_data_callback = std::bind(&ElinkClient::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&ElinkClient::on_connect, this, std::placeholders::_1);
            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-groupname"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            //usleep(1e3*thread_no);
            client = std::make_unique<FelixClientThread>(config);
        } catch (std::invalid_argument const& error) {
            LOG_ERR("Argument or option of wrong type");
            std::cout << USAGE << std::endl;
        }
        //initialise stats
        for(auto f : m_fids ){
            LOG_INFO("Client %d initialising statistics for fid 0x%lx", m_thread_no, f);
            m_stats.fid_stats.emplace(
                std::piecewise_construct,
                std::forward_as_tuple(f),
                std::forward_as_tuple(f, l1id_check_fmt)
            );
        }
        m_stats.bytes_received = 0;
        m_stats.total_messages_received = 0;
        m_stats.total_messages_received_prev = 0xFFFFFFFFFFFFFFFF;
        m_stats.messages_received = 0;
        m_stats.bytes_received = 0;
        m_reset_stats = false;
    }

    void subscribe(){
        std::unique_lock lk(mtx);
        try {
            LOG_INFO("Client %d subscribing", m_thread_no);
            client->subscribe(m_fids);
        } catch (FelixClientResourceNotAvailableException& exc) {
            LOG_ERR("Client %d subscription timeout, disconnecting.", m_thread_no);
            return;
        }
        m_running.wait(lk);
        running_threads--;
        return;
    }

    void reset_stats(){
        m_reset_stats = true;
    }

    int m_thread_no;
    thread_stats m_stats;

private:

    size_t m_connected_fids;
    uint64_t m_max_msg;
    std::atomic<bool> m_reset_stats;
    std::vector<uint64_t> m_fids;
    std::unique_ptr<FelixClientThread> client;
    std::mutex mtx;
    std::condition_variable m_running;
    uint64_t m_expected_messages;

    void on_connect(uint64_t fid) {
        ++m_connected_fids;
        LOG_INFO("Client %d: subscribed to fid 0x%lx", m_thread_no, fid);
        if(m_connected_fids == m_fids.size()){
            LOG_INFO("Client %d: all fids subscribed", m_thread_no);
        }
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {

        if(m_reset_stats == true) {
            m_stats.bytes_received = 0;
            m_stats.messages_received = 0;
            m_reset_stats = false;
        }
        m_stats.bytes_received+=size;
        m_stats.messages_received++;
        m_stats.total_messages_received++;
        if(m_stats.fid_stats.count( fid ) == 0){
            LOG_ERR("fid 0x%lx not present in client %d!!", fid, m_thread_no);
        }
        m_stats.fid_stats[fid].received++;

        // error flags
        if(status == CHUNK_FW_TRUNC || status == CHUNK_SW_TRUNC){
            m_stats.fid_stats[fid].truncated++;
        }
        if(status == CHUNK_FW_CRC){
            m_stats.fid_stats[fid].crc++;
        }
        if(status == CHUNK_FW_MALF || status == CHUNK_SW_MALF){
            m_stats.fid_stats[fid].error++;
        }

        if(m_max_msg > 0 && m_stats.fid_stats[fid].received >= m_max_msg){
            LOG_INFO("Client %d, all messages receved for 0x%lx. Unsubscribing,", m_thread_no, fid);
            client->unsubscribe(fid);
            m_connected_fids--;
            if(m_connected_fids==0){
                m_running.notify_one();
            }
        }

        if(l1id_check_fmt > 0){
            m_stats.fid_stats.at(fid).l0check->check_sequence_error(data, size);
        }

        if(processing_time_us > 0){
            usleep(processing_time_us);
        }
    }
};


std::vector<std::unique_ptr<ElinkClient>> clients;


void take_l1id_snapshot(xl1id_t& ttc2hl1id)
{
    for (auto & c : clients){
        for(auto & e : c->m_stats.fid_stats){
            e.second.snapshot_xl1id = e.second.l0check->get_last();
            e.second.mask = e.second.l0check->get_ec_mask();
            e.second.ttc2h_ec_difference = (int)(e.second.mask & ttc2hl1id.fields.ec) - (int)(e.second.mask & e.second.snapshot_xl1id.fields.ec);
        }
    }
}


void print_stats(){
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
    double seconds = t1.tv_sec - t0.tv_sec + 1e-9*(t1.tv_nsec - t0.tv_nsec);
    LOG_INFO("Statistics");
    for (auto & c : clients){
        //if there were no new messages the counters were not reset by on_data
        if(c->m_stats.total_messages_received_prev == c->m_stats.total_messages_received){
            c->m_stats.bytes_received = 0;
            c->m_stats.messages_received = 0;
        }
        printf("{\"thread\": %d, \"Gbps\": %.2f, \"kHz\": %.2f, \"stats\": [",
            c->m_thread_no,
            c->m_stats.bytes_received*8/1000./1000./1000./seconds,
            c->m_stats.messages_received/1000./seconds);
        size_t idx = 0;
        for(const auto & e : c->m_stats.fid_stats){
            printf("{ \"fid\": \"0x%lx\", \"chk\": %lu, \"t\": %lu, \"e\": %lu, \"crc\": %lu", e.first, e.second.received, e.second.truncated, e.second.error, e.second.crc);
            if (l1id_check_fmt > 0){
                printf(", \"l1id\": \"0x%08x\", \"EC diff\": %d, \"mask\": \"0x%x\"",e.second.snapshot_xl1id.xl1id, e.second.ttc2h_ec_difference, e.second.mask);
            }
            if(idx != c->m_stats.fid_stats.size()-1 && c->m_stats.fid_stats.size() > 1 ){
                printf("},");
            } else { printf("}\n"); }
            idx++;
        }
        c->m_stats.total_messages_received_prev = c->m_stats.total_messages_received;
        c->reset_stats();
    }
    t0 = t1;
    printf("\n");
}


int main(int argc, char** argv) {
    args = docopt::docopt(USAGE,
                    { argv + 1, argv + argc },
                    true,               // show help if requested
                    (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string


    std::vector<std::string> fid_list = args["<thread_fids>"].asStringList();
    std::map <int, std::vector<uint64_t> > fids_per_thread;
    bool found_ttc2h_reference = false;
    processing_time_us = args["--hang"].asLong();

    std::string delimiter = ":";
    for(auto& entry : fid_list){
        size_t pos = entry.find(delimiter);
        if(pos == std::string::npos){
            LOG_ERR("Cannot parse fid entry %s", entry);
            continue;
        }
        int tid = stoi(entry.substr(0, pos));
        std::string fid = entry.substr(pos+1, entry.length());
        uint64_t ufid = strtoul(fid.c_str(), NULL, 0);
        if (is_ttc2h_elink(ufid) == 1 && found_ttc2h_reference == false){
            reference_ttc2h_fid = ufid;
            found_ttc2h_reference = true;
        }
        fids_per_thread[tid].push_back(ufid);
    }
    LOG_INFO("Reference TTC2H fid is: 0x%lx", reference_ttc2h_fid);
    LOG_INFO("Subscription scheme for %lu threads:", fids_per_thread.size());
    for(const auto & el : fids_per_thread){
        printf("    Thread %d: ", el.first);
        for(auto f : el.second){
            printf("0x%lx ", f);
        }
        printf("\n");
    }

    std::vector<std::thread> threads;
    int number_of_threads = fids_per_thread.size();
    l1id_check_fmt = args["--l1id-check"].asLong();
    threads.reserve(number_of_threads);
    clients.reserve(number_of_threads);
    for(auto & el : fids_per_thread){
        clients.emplace_back(std::make_unique<ElinkClient>(el.second, el.first));
        threads.emplace_back(&ElinkClient::subscribe, clients.back().get());
    }
    running_threads = number_of_threads;

    while(running_threads > 0){
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        print_stats();
    }

    for(auto& th : threads){
        th.join();
    }
    clients.clear();
    threads.clear();
    LOG_INFO("All threads returned.");
    return 0;
}
