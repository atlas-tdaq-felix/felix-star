#include <algorithm>
#include <cstdio>
#include <cstring>
#include <string>
#include <unistd.h>
#include <vector>
#include <map>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "felixtag.h"
#include "felix/felix_fid.h"

#include "log.hpp"
#include "util.hpp"


static const char USAGE[] =
R"(felix-register-client - Send read/write commands to the register via an elink.

    Usage:
      felix-register-client [options --fid=<fid>... --expect-reply=<ctrl_fid:status_code:value>... --dont-expect-reply=<ctrl_fid:status_code:value>... --repeat=<n> --delay=<s>] <local_ip_or_interface> <fid> <cmd> [<cmd_args>...]
      felix-register-client [options --fid=<fid>... --expect-mon=<n>] <local_ip_or_interface> <fid>

    Options:
      -h --help                                         Show this screen.
      --version                                         Show version.
      --fid=<fid>...                                    Additional fids to send the command to
      --log-level=<loglevel>                            Specify level of logging (trace, debug, info, notice, warning, error, fatal) [default: warning]
      --verbose-bus                                     Show bus information
      --bus-dir=<directory>                             Set directory for bus to use [default: ./bus]
      --bus-groupname=<groupname>                       Set groupname for bus to use [default: FELIX]
      --timeout=<timeoutms>                             Set timeout to subscribe (0 is no timeout) [default: 0]
      --netio-pages=<pages>                             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>                       Pagesize for buffer in bytes [default: 65536]
      --expect-reply=<ctrl_fid:status_code:value>...    Replies to be expected and checked
      --expect-mon=<n>                                  Monitoring replies expected
      --output-mon=<filename>                           Write output of monitoring in ndjson file
      --repeat=<n>                                      Repeat the command n times expected [default: 1]
      --delay=<s>                                       Delay inbetween requests (for repeat) [default: 0]
      --dont-expect-reply=<ctrl_fid:status_code:value>  Fail on this reply

    Arguments:
      <local_ip_or_interface>           Local hostname or IP
      <did>                             DID (Detector Id) to use in FID (Felix ID)
      <cid>                             CID (Connector Id) to use in FID (Felix ID)
      <cmd>                             Command to be performed (noop, get, set, reset, ecr_reset, ...)
      <cmd_args>                        Optional command arguments

    Commands
      noop                              no operation, just test connections (cmd_args: none)
      get                               reads register (cmd_args: register)
      set                               writes register (cmd_args: register, value)
      reset                             not implemented yet (cmd_args: none)
      ecr_reset                         ecr reset (cmd_args: counter_value)
)";


class UserClass {

public:
    UserClass(const UserClass &) = delete;
    UserClass &operator=(const UserClass &) = delete;
    explicit UserClass(std::map<std::string, docopt::value> &args) {

        // for(auto const& arg : args) {
        //      std::cout << arg.first <<  arg.second);
        // }

        try {
            FelixClientThread::Config config;
            config.on_init_callback = std::bind(&UserClass::on_init, this);
            config.on_data_callback = std::bind(&UserClass::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&UserClass::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&UserClass::on_disconnect, this, std::placeholders::_1);

            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-groupname"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            std::vector<FelixClientThread::Reply> expected_replies;
            for (auto expect_reply : args["--expect-reply"].asStringList()) {
                std::vector<std::string> part = Util::split(expect_reply, ':');
                if (part.size() != 3) {
                    LOG_ERR("Expected 3 parts in --expect-reply, but got %d", part.size());
                    return_code = -1;
                    return;
                }
                FelixClientThread::Reply reply;
                reply.ctrl_fid = std::stol(part[0], nullptr, 0);
                reply.status = FelixClientThread::Status(std::stol(part[1], nullptr, 0));
                reply.value = std::stol(part[2], nullptr, 0);
                expected_replies.push_back(reply);
            }

            std::vector<FelixClientThread::Reply> not_expected_replies;
            for (auto not_expect_reply : args["--dont-expect-reply"].asStringList()) {
                std::vector<std::string> part = Util::split(not_expect_reply, ':');
                if (part.size() != 3) {
                    LOG_ERR("Expected 3 parts in --dont-expect-reply, but got %d", part.size());
                    return_code = -1;
                    return;
                }
                FelixClientThread::Reply reply;
                reply.ctrl_fid = std::stol(part[0], nullptr, 0);
                reply.status = FelixClientThread::Status(std::stol(part[1], nullptr, 0));
                reply.value = std::stol(part[2], nullptr, 0);
                not_expected_replies.push_back(reply);
            }

            client = new FelixClientThread(config);

            subscribed = false;
            uint64_t fid = args["<fid>"].asLong();

            std::vector<uint64_t> fids;
            fids.push_back(fid);

            auto fid_args = args["--fid"].asStringList();
            std::transform(fid_args.begin(), fid_args.end(), std::back_inserter(fids), stol);


            FelixClientThread::Status status = FelixClientThread::OK;

            if (args["<cmd>"]) {
                // Command version
                monitoring = false;

                std::string cmd_string = args["<cmd>"].asString();
                FelixClientThread::Cmd cmd = FelixClientThread::to_cmd(cmd_string);

                std::vector<std::string> cmd_args = args["<cmd_args>"].asStringList();

                int repeat = args["--repeat"].asLong();
                LOG_INFO("Repeat: %d times", repeat);
                int delay = args["--delay"].asLong();
                LOG_INFO("Delay inbetween repeats: %d ms", delay);

                for(int i=0; i<repeat; i++) {

                    LOG_INFO("Attempt %d", i);
                    std::vector<FelixClientThread::Reply> replies;
                    status = client->send_cmd(fids, cmd, cmd_args, replies);

                    if (status != FelixClientThread::Status::OK) {
                        LOG_ERR("Status: %s", FelixClientThread::to_string(status).c_str());
                    }
                    int r = 1;
                    for(auto const& reply : replies) {
                        std::string st = FelixClientThread::to_string(reply.status);
                        LOG_INFO("[Reply %d of %u] fid: 0x%x, status: %s, value 0x%lx, msg %s.", r, replies.size(), reply.ctrl_fid, st.c_str(),  reply.value, reply.message.c_str());
                        ++r;
                    }

                    // Check just for unit tests
                    if ((expected_replies.size() > 0 ) && (expected_replies.size() != replies.size())) {
                        LOG_ERR("Expected %d replies and got %d", expected_replies.size(), replies.size());
                        return_code = -1;
                        return;
                    }

                    // Check expected replies
                    for(unsigned int j=0; j<expected_replies.size(); j++) {
                        FelixClientThread::Reply& expected_reply = expected_replies[j];
                        std::vector<FelixClientThread::Reply>::iterator it;
                        it = find_if(replies.begin(), replies.end(), [expected_reply](const FelixClientThread::Reply& reply){ return (reply.ctrl_fid == expected_reply.ctrl_fid); });
                        if (it == replies.end()){
                            LOG_ERR("Could not find matching ctrl_fid for expected_reply 0x%x", expected_reply.ctrl_fid);
                            // delete client;
                            sleep(1);
                            return_code = -1;
                            return;
                        }

                        const FelixClientThread::Reply& reply = *it;

                        if (reply.status != expected_reply.status) {
                            LOG_ERR("Expected status %d for reply %d but got %d - %s", expected_reply.status, j, reply.status, FelixClientThread::to_string(reply.status).c_str());
                            // delete client;
                            sleep(1);
                            return_code = -1;
                            return;
                        }

                        if (reply.value != expected_reply.value) {
                            LOG_ERR("Expected value 0x%x for reply %d but got 0x%x", expected_reply.value, j, reply.value);
                            // delete client;
                            sleep(1);
                            return_code = -1;
                            return;
                        }
                    }

                    // Check not expected replies
                    for(unsigned int j=0; j<not_expected_replies.size(); j++) {
                        FelixClientThread::Reply& not_expected_reply = not_expected_replies[j];
                        std::vector<FelixClientThread::Reply>::iterator it;
                        it = find_if(replies.begin(), replies.end(), [not_expected_reply](const FelixClientThread::Reply& reply){ return (reply.ctrl_fid == not_expected_reply.ctrl_fid); });
                        if (it != replies.end()){
                            const FelixClientThread::Reply& reply = *it;
                            if (reply.status == not_expected_reply.status) {
                                LOG_ERR("Did NOT expect status %d - %s for reply %d", reply.status, FelixClientThread::to_string(reply.status).c_str(), j);
                                // delete client;
                                sleep(1);
                                return_code = -1;
                                return;
                            }
                        }
                    }
                    if (repeat > 1 && delay > 0) {
                        sleep(delay);
                    }
                }
            } else {
                // Monitoring version
                monitoring = true;
                mon_replies = 0;

                if(args["--output-mon"]) {
  	                mon_output_file = fopen(args["--output-mon"].asString().c_str(), "w");
	                if (mon_output_file == NULL) {
                            LOG_ERR("error opening file %s", args["--output-mon"].asString().c_str());
                            return_code = 1;
                            return;
	                }
                }

                for (auto fid_arg : fids) {
                    client->subscribe(fid_arg);
                }

                int32_t expected_mon_replies = args["--expect-mon"] ? args["--expect-mon"].asLong() : -1;
                while(mon_replies < expected_mon_replies) {
                    sleep(1);
                }

                if (mon_output_file) {
                    fclose(mon_output_file);
                }

                if (expected_mon_replies > 0) {
                    // delete client;
                    return_code = 42;
                    return;
                }
            }
            // delete client;
            sleep(1);
            return_code = status;
        } catch (std::invalid_argument const& error) {
            LOG_INFO("Argument or option of wrong type");
            LOG_INFO("");
            LOG_INFO("%s", USAGE);
            return_code = -1;
        } catch (const FelixClientResourceNotAvailableException& error) {
            LOG_ERR("Failed to send");
            // delete client;
            return_code = 1;
        }
    }

    ~UserClass() {
        delete client;
    }

    int return_code = 0;

private:
    static long stol(const std::string& str) {
        return std::stol(str, nullptr, 0);
    }

    void on_init() {
        LOG_INFO("on_init called");
    }

    void on_connect(uint64_t fid) {
        LOG_INFO("on_connect called 0x%lx", fid);

        // FIXME seems to deadlock trying to setup send connection
        if (fid == subscribe_tag) {
            subscribed = true;
        }
    }

    void on_disconnect(uint64_t fid) {
        LOG_INFO("on_disconnect called 0x%lx", fid);
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        LOG_INFO("register_client: on_data called for 0x%lx with size %lu and status %u", fid, size, status);

        if (monitoring) {
            LOG_INFO("%s", data);
            if (mon_output_file) {
                fwrite(data, sizeof(uint8_t), size, mon_output_file);
            }
            mon_replies++;
        }
    }

    FelixClientThread* client;
    bool subscribed;
    uint64_t subscribe_tag;

    bool monitoring;
    FILE* mon_output_file = NULL;
    int32_t mon_replies;
};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    UserClass userClass(args);
    return userClass.return_code;
}
