#include "register_device_interface.hpp"
#include <cstdint>
#include <nlohmann/json.hpp>
#include "log.hpp"
#include <catch2/catch_test_macros.hpp>

RegmapManager regmap;

TEST_CASE( "RegisterDeviceManager", "[OK]" )
{
    auto d0 = std::make_shared<FileDevice>(0);
    auto d1 = std::make_shared<FileDevice>(1);
    d0->open_device(0);
    d1->open_device(0);

    auto dut = RegisterDeviceInterface(regmap, d1, d0);
    uint64_t reg = dut.get_register("PCIE_ENDPOINT");
    REQUIRE(reg == 1);
};


TEST_CASE( "DeviceRegister", "[Reroute to primary device]" )
{
    auto d0 = std::make_shared<FileDevice>(0);
    auto d1 = std::make_shared<FileDevice>(1);
    d0->open_device(0);
    d1->open_device(0);

    auto dut = RegisterDeviceInterface(regmap, d1, d0);
    uint64_t reg = dut.get_register("TTC_DEC_CTRL_MASTER_BUSY");
    REQUIRE(reg == 0);
    dut.set_register("TTC_DEC_CTRL_MASTER_BUSY", 1);
    reg = dut.get_register("TTC_DEC_CTRL_MASTER_BUSY");
    REQUIRE(reg == 1);
};


TEST_CASE( "DeviceRegister", "[Primary device not available]" )
{
    auto d1 = std::make_shared<FileDevice>(1);
    d1->open_device(0);

    auto dut = RegisterDeviceInterface(regmap, d1, d1);
    uint64_t reg = dut.get_register("TTC_DEC_CTRL_MASTER_BUSY");
    REQUIRE(reg == 0);
    dut.set_register("TTC_DEC_CTRL_MASTER_BUSY", 1);
    reg = dut.get_register("TTC_DEC_CTRL_MASTER_BUSY");
    REQUIRE(reg == 0);
};
