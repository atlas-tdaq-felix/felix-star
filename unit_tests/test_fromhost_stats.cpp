#include <sys/types.h>
#include <iostream>
#include <catch2/catch_test_macros.hpp>

#include "felix/felix_fid.h"
#include "fromhost_monitor.hpp"
#include "util.hpp"

TEST_CASE( "FromHost json structure", "[One elink/decoder, one writer, one buffer, one device]" )
{
    std::string serial_msg;
    nlohmann::json parsed;

    int device = 1;
    int dma = 2;
    int buffer = 3;

    FromHostElinkStats elink_stats(0x8);
    elink_stats.processed_msg = 7;
    elink_stats.processed_bytes = 45;

    FromHostWriterStats writer_stats(buffer, 0, 32);
    FromHostDmaStats dma_stats(dma, 50, 450, 320);
    FromHostDeviceStats device_stats(device);

    std::string fifoname = "";
    FromHostMonitor mon(fifoname);
    std::string ts = Util::ISO8601TimeUTC();
    std::string hostname = Util::get_full_hostname();

    mon.append_device_stats(ts, hostname, device_stats);
    mon.append_dma_stats(ts, hostname, device, dma_stats);
    serial_msg = mon.get_serialized_message();
    std::cout << serial_msg << std::endl;
    parsed = nlohmann::json::parse(serial_msg);

    REQUIRE( parsed["ts"] == ts);
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["dma_free_MB"] == 50 );
    //Rates require a time measurement so here they remain 0. Check for key only.
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["msg_rate_Hz"] == 0 );
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["msg_rate_Mbps"] == 0 );

    mon.append_writer_stats(ts, hostname, device, dma, writer_stats);
    serial_msg = mon.get_serialized_message();
    std::cout << serial_msg << std::endl;
    parsed = nlohmann::json::parse(serial_msg);

    REQUIRE( parsed["ts"] == ts);
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["number_of_connections"] == 32);
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["type"] == 0);

    mon.append_elink_stats(ts, hostname, device, dma, buffer, elink_stats);
    serial_msg = mon.get_serialized_message();
    std::cout << serial_msg << std::endl;
    parsed = nlohmann::json::parse(serial_msg);

    REQUIRE( parsed["ts"] == ts);
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["fid"]["0x8"]["msgs"] == 7 );
    REQUIRE( parsed["host"][hostname]["app"]["fromhost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["fid"]["0x8"]["bytes"] == 45 );
}
