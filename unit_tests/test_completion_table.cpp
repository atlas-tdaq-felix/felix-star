#include <catch2/catch_test_macros.hpp>

#include "log.hpp"

#include "completion_table.hpp"

TEST_CASE( "completion table tests", "[completion_table]" )
{
    CompletionTable T;

    SECTION( "Check 0: table is empty, return UINT32_MAX" ) {
        uint32_t rd = T.get_rd();
        REQUIRE(rd == UINT32_MAX);
    }

    SECTION( "Check 1: check counts" ) {
        //add three entries
        T.push(0x00001);
        T.push(0x00002);
        T.push(0x00003);
        REQUIRE(T.get_entries() == 3);

        //increase counter of third entry
        T.push(0x00003);
        T.push(0x00003);
        REQUIRE(T.get_count(0x00001) == 1);
        REQUIRE(T.get_count(0x00002) == 1);
        REQUIRE(T.get_count(0x00003) == 3);

        //decrease count of third entry
        T.update(0x00003);
        REQUIRE(T.get_count(0x00003) == 2);

        //decrease count of second entry
        T.update(0x00002);
        REQUIRE(T.get_entries() == 2);

        //empty list
        T.update(0x00001);
        T.update(0x00003);
        T.update(0x00003);
        T.inspect();
        REQUIRE(T.get_entries() == 0);
    }

    SECTION( "Check 2: read pointer" ) {
        //empty table
        REQUIRE( T.get_rd() == UINT32_MAX );

        T.push(0x00001);
        T.push(0x00002);
        T.push(0x00003);
        REQUIRE( T.get_rd() == UINT32_MAX );

        //remove 0x00002, rd should not advance
        T.update(0x00002);
        REQUIRE( T.get_rd() == UINT32_MAX );

        //remove 0x00001, 0x00002 is gone, rd should point at 0x00003
        T.update(0x00001);
        T.inspect();
        REQUIRE( T.get_rd() == 0x00003 );

        //remove 0x00003, rd should not move
        T.update(0x00003);
        REQUIRE( T.get_rd() == 0x00003 );
    }

    SECTION("Check 3: wrap-around") {
        T.push(0x0);
        T.push(0x1);
        T.update(0x0);
        T.update(0x1);
        T.push(0x2);
        //wrap around
        T.push(0x0);
        T.update(0x2);
        T.update(0x0);
        T.push(0x1);
        T.update(0x1);
        //rd should be 0x1
        T.inspect();
        REQUIRE( T.get_rd() == 0x1 );
    }

    SECTION("Check 4: wrap-around with current table emptied") {
        T.push(0x0);
        T.push(0x1);
        T.update(0x0);
        T.update(0x1);
        T.push(0x2);
        T.update(0x2);
        T.inspect();
        //wrap around
        T.push(0x0);
        T.update(0x0);
        T.push(0x1);
        T.update(0x1);
        //rd should be 0x1
        T.inspect();
        REQUIRE( T.get_rd() == 0x1 );
    }


    SECTION("Check 5: addresses with offset") {
        T.push(0x9990);
        T.push(0x9991);
        T.update(0x9990);
        T.update(0x9991);
        T.push(0x9992);
        T.update(0x9992);
        T.inspect();
        //wrap around
        T.push(0x9990);
        T.update(0x9990);
        T.push(0x9991);
        T.update(0x9991);
        //rd should be 0x1
        T.inspect();
        REQUIRE( T.get_rd() == 0x9991 );
    }

}
