#include <catch2/catch_test_macros.hpp>

#include "log.hpp"
#include "flx_api.hpp"

#if REGMAP_VERSION >= 0x0500
#define NUM_LINKS 6
TEST_CASE( "broadcast elinks tests", "[broadcast-elinks]" )
{
    SECTION( "Check 0: no broadcast elinks" ) {
        uint64_t broadcast_elinks[MAX_BROADCAST_ELINKS] = {0};
        uint64_t broadcast_enable_registers[NUM_LINKS] = {0, 0, 0, 0, 0, 0};
        int n = FlxApi::broadcast_elinks(broadcast_elinks, broadcast_enable_registers, NUM_LINKS);
        REQUIRE(n == 0);
    }

    SECTION( "Check 1: one broadcast elink" ) {
        uint64_t broadcast_elinks[MAX_BROADCAST_ELINKS] = {0};
        uint64_t broadcast_enable_registers[NUM_LINKS] = {0, 0, 0, 1, 0, 0};
        int n = FlxApi::broadcast_elinks(broadcast_elinks, broadcast_enable_registers, NUM_LINKS);
        REQUIRE(n == 3);
        REQUIRE(broadcast_elinks[--n] == 0x7ff);
        REQUIRE(broadcast_elinks[--n] == (0x1f << 6));
        REQUIRE(broadcast_elinks[--n] == ((3 << 6) | 0x3f));
    }

    SECTION( "Check 2: two different broadcast elinks" ) {
        uint64_t broadcast_elinks[MAX_BROADCAST_ELINKS] = {0};
        uint64_t broadcast_enable_registers[NUM_LINKS] = {0, 0, 0, 1, 2, 0};
        int n = FlxApi::broadcast_elinks(broadcast_elinks, broadcast_enable_registers, NUM_LINKS);
        REQUIRE(n == 5);
        // for (int i=0; i<n; i++) {
        //     LOG_INFO("broadcast_elinks[%d] = 0x%x", i, broadcast_elinks[i]);
        // }
        REQUIRE(broadcast_elinks[--n] == 0x7ff);
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 1));
        REQUIRE(broadcast_elinks[--n] == (0x1f << 6));
        REQUIRE(broadcast_elinks[--n] == ((4 << 6) | 0x3f));
        REQUIRE(broadcast_elinks[--n] == ((3 << 6) | 0x3f));
    }

    SECTION( "Check 3: two identical broadcast elinks" ) {
        uint64_t broadcast_elinks[MAX_BROADCAST_ELINKS] = {0};
        uint64_t broadcast_enable_registers[NUM_LINKS] = {0, 0, 0, 4, 0, 4};
        int n = FlxApi::broadcast_elinks(broadcast_elinks, broadcast_enable_registers, NUM_LINKS);
        REQUIRE(n == 4);
        REQUIRE(broadcast_elinks[--n] == 0x7ff);
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 2));
        REQUIRE(broadcast_elinks[--n] == ((5 << 6) | 0x3f));
        REQUIRE(broadcast_elinks[--n] == ((3 << 6) | 0x3f));
    }

    SECTION( "Check 4: three broadcast elinks" ) {
        uint64_t broadcast_elinks[MAX_BROADCAST_ELINKS] = {0};
        uint64_t broadcast_enable_registers[NUM_LINKS] = {0b00000, 0b10000, 0b00000, 0b00101, 0b00011, 0b00001};
        int n = FlxApi::broadcast_elinks(broadcast_elinks, broadcast_enable_registers, NUM_LINKS);
        REQUIRE(n == 9);
        REQUIRE(broadcast_elinks[--n] == 0x7ff);
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 4));
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 2));
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 1));
        REQUIRE(broadcast_elinks[--n] == (0x1f << 6));
        REQUIRE(broadcast_elinks[--n] == ((5 << 6) | 0x3f));
        REQUIRE(broadcast_elinks[--n] == ((4 << 6) | 0x3f));
        REQUIRE(broadcast_elinks[--n] == ((3 << 6) | 0x3f));
        REQUIRE(broadcast_elinks[--n] == ((1 << 6) | 0x3f));
    }

    SECTION( "Check 5: channel 1: elink 0, channel 0 elink 8 and 22, (these last two as in tests)" ) {
        uint64_t broadcast_elinks[MAX_BROADCAST_ELINKS] = {0};
        uint64_t broadcast_enable_registers[NUM_LINKS] = {0b00000000010000000000000100000000, 0b00000000000000000000000000000001};
        int n = FlxApi::broadcast_elinks(broadcast_elinks, broadcast_enable_registers, NUM_LINKS);
        REQUIRE(n == 6);
        // for (int i=0; i<n; i++) {
        //     LOG_INFO("broadcast_elinks[%d] = 0x%x", i, broadcast_elinks[i]);
        // }
        REQUIRE(broadcast_elinks[--n] == 0x7ff);
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 22));
        REQUIRE(broadcast_elinks[--n] == ((0x1f << 6) | 8));
        REQUIRE(broadcast_elinks[--n] == (0x1f << 6));
        REQUIRE(broadcast_elinks[--n] == ((1 << 6) | 0x3f));
        REQUIRE(broadcast_elinks[--n] == 0x3f);
    }

    SECTION( "Check 6: flx_broadcast_type" ) {
        REQUIRE(FlxApi::broadcast_type(0x7f) == BROADCAST_CHANNEL);
        REQUIRE(FlxApi::broadcast_type(0x7c8) == BROADCAST_ELINK);
        REQUIRE(FlxApi::broadcast_type(0x7ff) == BROADCAST_ALL);
        REQUIRE(FlxApi::broadcast_type(0x00) == BROADCAST_NONE);
    }

    SECTION( "Check 7: flx_broadcast_for" ) {
        REQUIRE(FlxApi::broadcast_for(0x7f) == 1);
        REQUIRE(FlxApi::broadcast_for(0x7c8) == 8);
        REQUIRE(FlxApi::broadcast_for(0x7ff) == -1);
        REQUIRE(FlxApi::broadcast_for(0x00) == 0);
    }
}
#endif
