#include <span>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <catch2/catch_test_macros.hpp>

#include "device.hpp"
#include "felix/felix_fid.h"
#include "block.hpp"
#include "decoder.hpp"
#include "tohost_monitor.hpp"


class DummyPublisher : public Publisher
{
    bool declare(const std::vector<Elink> &elinks)
        override {return true;};

    Result publish(felix_id_t fid, iovec *iov, uint32_t iovlen, size_t bytes, uint32_t block_addr, std::uint8_t status)
        override {return Publisher::Result::OK;};

    Result publish(felix_id_t fid, uint8_t* data, size_t len)
        override {return Publisher::Result::OK;};

    Result flush(felix_id_t fid)
        override {return Publisher::Result::OK;};

    void set_periodic_callback(uint32_t period_us, Callback callback)
        override {return;};

    void set_asynch_callback(Callback callback) override {return;};
    void fire_asynch_callback() override {return;};
    uint32_t get_progress_ptr() override {return 0;};
    uint32_t get_resource_counter() override {return 0;};
    uint64_t get_resource_available_calls() override {return 0;};
    uint32_t get_subscription_number() override {return 0;};
//    void truncate_msg_if_too_large(iovec *iov, uint32_t& size){return;}
};


class Reader
{
    public:
        Reader(const std::string& filename, size_t num_blocks, size_t block_size, Elink e, flx_tohost_format fmt, int l0id_fmt);

        std::span<Block> get_blocks();

        unsigned char* get_buf() {return m_buf.get();}

    private:
        std::unique_ptr<unsigned char[]> m_buf;
        Elink m_elink;
        size_t m_nblocks;
        size_t m_block_size;
        DummyPublisher m_publisher;  

    public:
        Decoder m_decoder;

};

//flx_tohost_format::TOHOST_SUBCHUNK_TRAILER
Reader::Reader(const std::string& filename, size_t num_blocks, size_t block_size, Elink e, flx_tohost_format fmt, int l0id_fmt)
    :   m_buf(std::make_unique<unsigned char[]>(num_blocks * block_size)),
        m_elink(e),
        m_nblocks(num_blocks),
        m_block_size(block_size),
        m_decoder(m_elink, m_publisher, fmt, 0, block_size, 0)
{
    int fd = open(filename.c_str(), O_RDONLY | O_NONBLOCK);
    FILE* fp = fdopen(fd, "r");
    REQUIRE( fp != NULL);
    size_t count = fread(m_buf.get(), block_size, num_blocks, fp);
    fclose(fp);
    REQUIRE( count == num_blocks );
}


std::span<Block> Reader::get_blocks()
{
    return std::span(reinterpret_cast<Block*>(m_buf.get()), m_nblocks);
}


#if REGMAP_VERSION < 0x0500

TEST_CASE( "decode blocks rm-4", "[basic functionality]" )
{
    unsigned int num_blocks = 1024;
    size_t block_size = 1024;
    std::string filename = "data/data-55-1024-16.blocks";

    local_elink_t lid = 0x55;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    unsigned int decode_returns = 0;

    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_TRAILER, 0);
    for (Block& block : r.get_blocks()) {
        auto ret = r.m_decoder.decode(block);
        if (ret == Publisher::OK){
            ++decode_returns;
        }
    }

    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( decode_returns == num_blocks );
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 477 );
}


TEST_CASE( "Check rm-4 FLX-1829 bugfix", "[FLX-1829]" )
{
    unsigned int num_blocks = 1;
    size_t block_size = 1024;
    std::string filename = "data/flx1829.blocks";

    local_elink_t lid = 0x0;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_TRAILER, 0);
    for (Block& block : r.get_blocks()) {
        r.m_decoder.decode(block);
    }
    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 1 );
}


TEST_CASE( "Decode FELIX L1ID, rm-4", "[L1ID FELIG]" )
{
    unsigned int num_blocks = 1;
    size_t block_size = 1024;
    std::string filename = "data/ttc2h-33B-1024-16.blocks";

    local_elink_t lid = 0x0;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_TRAILER, L0ID_FMT::FELIG);

    for (Block& block : r.get_blocks()) {
        r.m_decoder.decode(block);
    }
    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 4 );
    REQUIRE( stats.oosequence_l0id == 0 );
}


#else

TEST_CASE( "Decode 1kB block rm-5, subchunk trailer", "[C0CE]" )
{
    unsigned int num_blocks = 1024;
    size_t block_size = 1024;
    std::string filename = "data/data-55-1024-1024.blocks";

    local_elink_t lid = 0x037;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);
    
    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_TRAILER, 0);
    r.m_decoder.set_block_size(1024); //should be read from fw

    unsigned int decode_returns = 0;

    for (Block& block : r.get_blocks()) {
        auto ret = r.m_decoder.decode(block);
        if (ret == Publisher::OK){
            ++decode_returns;
        }
    }
    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( decode_returns == num_blocks );
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 486 );
}

TEST_CASE( "Decode 1kB block rm-5, subchunk header", "[C0CE]" )
{
    unsigned int num_blocks = 1;
    size_t block_size = 1024;
    std::string filename = "data/data-0x08-subchk-head-1024-32.blocks";

    local_elink_t lid = 0x08;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_HEADER, 0);
    r.m_decoder.set_block_size(1024); //should be read from fw

    unsigned int decode_returns = 0;

    for (Block& block : r.get_blocks()) {
        auto ret = r.m_decoder.decode(block);
        if (ret == Publisher::OK){
            ++decode_returns;
        }
    }
    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( decode_returns == num_blocks );
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 23 );
}

TEST_CASE( "Decode 1kB block rm-5, subchunk trailer, parse trc and err from fw ", "[C0CE]" )
{
    unsigned int num_blocks = 2;
    size_t block_size = 1024;
    std::string filename = "data/data-0x00-1024-trail-err-trunc.blocks";

    local_elink_t lid = 0x00;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_TRAILER, 0);
    r.m_decoder.set_block_size(1024); //should be read from fw

    unsigned int decode_returns = 0;

    for (Block& block : r.get_blocks()) {
        auto ret = r.m_decoder.decode(block);
        if (ret == Publisher::OK){
            ++decode_returns;
        }
    }
    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( decode_returns == num_blocks );
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 44 );
    REQUIRE( stats.fw_error_chunks == 14 );
    REQUIRE( stats.fw_trunc_chunks == 14 );
}

TEST_CASE( "Decode 1kB block rm-5, subchunk header, parse trc and err from fw ", "[C0CE]" )
{
    unsigned int num_blocks = 2;
    size_t block_size = 1024;
    std::string filename = "data/data-0x00-1024-head-err-trunc.blocks";

    local_elink_t lid = 0x00;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    Reader r(filename, num_blocks, block_size, e, flx_tohost_format::TOHOST_SUBCHUNK_HEADER, 0);
    r.m_decoder.set_block_size(1024); //should be read from fw

    unsigned int decode_returns = 0;

    for (Block& block : r.get_blocks()) {
        auto ret = r.m_decoder.decode(block);
        if (ret == Publisher::OK){
            ++decode_returns;
        }
    }
    ToHostElinkStats stats = r.m_decoder.get_decoder_stats();
    REQUIRE( decode_returns == num_blocks );
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 254 );
    REQUIRE( stats.fw_error_chunks == 254 );
    REQUIRE( stats.fw_trunc_chunks == 39 );
}




/*
std::span of Block does not support variable size block

TEST_CASE( "Decode 4kB block rm-5", "[C3CE]" )
{
    unsigned int num_blocks = 1024;
    size_t block_size = 4096;
    std::string filename = "data/data-55-1024-4096.blocks";

    local_elink_t lid = 0x037;
    felix_id_t fid = get_fid_from_ids(0x0, 0x0, lid, 0, 1, 0, 0);
    Elink e(fid, lid, false, elink_type_t::DAQ, encoding_t::ENC_8b10b);

    Reader r(filename, num_blocks, block_size, e, 0);
    r.m_decoder.set_block_size(4096); //should be read from fw
    for (Block& block : r.get_blocks()) {
        auto ret = r.m_decoder.decode(block);
    }
    ToHostElinkStats stats = r.m_decoder.get_statistics();
    REQUIRE( stats.processed_blocks == num_blocks );
    REQUIRE( stats.processed_chunks == 1957 );
}
*/
#endif
