#include "register_cmd_parser.hpp"
#include <nlohmann/json.hpp>
#include "log.hpp"
#include <catch2/catch_test_macros.hpp>


RegisterMsgParser p;

// No error conditions: NOOP, GET, SET 

TEST_CASE( "ParseCommand", "[FelixClientThread::Cmd::NOOP]" )
{
    //Define command(s)
    std::vector<Command> commands = {
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::NOOP,
            .cmd_args = {}
        }
    };
    //Encode commands in json
    std::string command_messages = p.encode_commands(commands);
    LOG_INFO("Test command: %s", command_messages.c_str());
    
    //Processs commands as if they were received over the network
    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size() == 1 );

    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status == FelixClientThread::Status::OK );
    REQUIRE( parsed_msg.status_message == FelixClientThread::to_string(status) );
    REQUIRE( parsed_msg.uuid == "0x1234" );
    REQUIRE( parsed_msg.cmd == FelixClientThread::Cmd::NOOP );

    //Encode replies
    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    LOG_INFO("Replies: %s", replies.c_str());

    //Decode replies and check content
    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);
    REQUIRE( decoded_replies.size()  == 1 );

    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.ctrl_fid == 0x1200 );
    REQUIRE( reply.status   == FelixClientThread::Status::OK );
    REQUIRE( reply.value    == 0 );
    REQUIRE( reply.message  == FelixClientThread::to_string(FelixClientThread::Status::OK) );
};


TEST_CASE( "ParseCommand", "[FelixClientThread::Cmd::GET]" )
{
    std::vector<Command> commands = {
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::GET,
            .cmd_args = {"REGISTER_NAME"}
        }
    };

    std::string command_messages = p.encode_commands(commands);
    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size() == 1 );

    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status == FelixClientThread::Status::OK );
    REQUIRE( parsed_msg.status_message == FelixClientThread::to_string(status) );
    REQUIRE( parsed_msg.uuid == "0x1234" );
    REQUIRE( parsed_msg.cmd == FelixClientThread::Cmd::GET );
    REQUIRE( parsed_msg.resource_name == "REGISTER_NAME" );

    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    LOG_INFO("Replies: %s", replies.c_str());

    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);
    REQUIRE( decoded_replies.size()  == 1 );
    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.ctrl_fid == 0x1200 );
    REQUIRE( reply.status   == FelixClientThread::Status::OK );
    REQUIRE( reply.value    == 0 ); //Nothing will change the default value.
    REQUIRE( reply.message  == FelixClientThread::to_string(FelixClientThread::Status::OK) );
}


TEST_CASE( "ParseCommand", "[FelixClientThread::Cmd::SET]" )
{
    std::vector<Command> commands = {
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME", "0x1"}
        }
    };

    std::string command_messages = p.encode_commands(commands);
    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size() == 1 );

    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status == FelixClientThread::Status::OK );
    REQUIRE( parsed_msg.status_message == FelixClientThread::to_string(status) );
    REQUIRE( parsed_msg.uuid == "0x1234" );
    REQUIRE( parsed_msg.cmd == FelixClientThread::Cmd::SET );
    REQUIRE( parsed_msg.resource_name == "REGISTER_NAME" );
    REQUIRE( parsed_msg.value == "0x1" );

    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    LOG_INFO("Replies: %s", replies.c_str());

    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);
    REQUIRE( decoded_replies.size()  == 1 );
    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.ctrl_fid == 0x1200 );
    REQUIRE( reply.status   == FelixClientThread::Status::OK );
    REQUIRE( reply.value    == 1 );
    REQUIRE( reply.message  == FelixClientThread::to_string(FelixClientThread::Status::OK) );
}


TEST_CASE( "ParseNoUuid", "[uuid not found]" )
{
    Command req{
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME", "0x1"}
        };

    nlohmann::json j = nlohmann::json::array();
    //j[0][FelixClientThread::to_string(FelixClientThread::UUID)]    = entry.uuid;
    j[0][FelixClientThread::to_string(FelixClientThread::CMD)]   = FelixClientThread::to_string(req.cmd);
    j[0][FelixClientThread::to_string(FelixClientThread::CMD_ARGS)]  = nlohmann::json(req.cmd_args);
    std::string command_messages = j.dump();

    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size()  == 1 );

    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status != FelixClientThread::Status::OK );
    REQUIRE( parsed_msg.value.empty() );

    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);

    REQUIRE( decoded_replies.size()  == 1 );
    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.status   != FelixClientThread::Status::OK );
    REQUIRE( reply.value == 0 );
}


TEST_CASE( "ParseNoCmd", "[cmd not found]" )
{
    Command req{
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME", "0x1"}
        };

    nlohmann::json j = nlohmann::json::array();
    j[0][FelixClientThread::to_string(FelixClientThread::UUID)]    = req.uuid;
    //j[0][FelixClientThread::to_string(FelixClientThread::CMD)]   = FelixClientThread::to_string(req.cmd);
    j[0][FelixClientThread::to_string(FelixClientThread::CMD_ARGS)]  = nlohmann::json(req.cmd_args);
    std::string command_messages = j.dump();

    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size()  == 1 );
    
    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status != FelixClientThread::Status::OK );
    REQUIRE( parsed_msg.value.empty() );

    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);

    REQUIRE( decoded_replies.size()  == 1 );
    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.status   != FelixClientThread::Status::OK );
    REQUIRE( reply.value == 0 );
}


TEST_CASE( "ParseNoCmdArgs", "[cmd args not found]" )
{
    Command req{
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME", "0x1"}
        };

    nlohmann::json j = nlohmann::json::array();
    j[0][FelixClientThread::to_string(FelixClientThread::UUID)]    = req.uuid;
    j[0][FelixClientThread::to_string(FelixClientThread::CMD)]   = FelixClientThread::to_string(req.cmd);
    //j[0][FelixClientThread::to_string(FelixClientThread::CMD_ARGS)]  = nlohmann::json(req.cmd_args);
    std::string command_messages = j.dump();

    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size()  == 1 );
    
    ReqData parsed_msg = parsed_cmds.at(0);
    REQUIRE( parsed_msg.value.empty() );

    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);

    REQUIRE( decoded_replies.size()  == 1 );
    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.value == 0 );
}


TEST_CASE( "ParseInvalidCmd", "[FelixClientThread::ERROR_INVALID_CMD]" )
{
    //Define command(s)
    std::vector<Command> commands = {
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::UNKNOWN,
            .cmd_args = {}
        }
    };
    //Encode commands in json
    std::string command_messages = p.encode_commands(commands);
    LOG_INFO("Test command: %s", command_messages.c_str());
    
    //Processs commands as if they were received over the network
    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size() == 1 );

    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status == FelixClientThread::Status::ERROR_INVALID_CMD );

    //Encode replies
    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    LOG_INFO("Replies: %s", replies.c_str());

    //Decode replies and check content
    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);
    REQUIRE( decoded_replies.size()  == 1 );

    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.ctrl_fid == 0x1200 );
    REQUIRE( reply.status   == FelixClientThread::Status::ERROR_INVALID_CMD );
}


TEST_CASE( "ParseInvalidArgs", "[FelixClientThread::ERROR_INVALID_ARGS]" )
{
    std::vector<Command> commands = {
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME"} //no value given
        }
    };

    std::string command_messages = p.encode_commands(commands);
    std::vector<ReqData> parsed_cmds = p.parse_commands(command_messages);
    REQUIRE( parsed_cmds.size() == 1 );

    ReqData parsed_msg = parsed_cmds.at(0);
    auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
    REQUIRE( status == FelixClientThread::Status::ERROR_INVALID_ARGS );

    std::string replies = p.encode_replies(0x1200, parsed_cmds);
    std::vector<FelixClientThread::Reply> decoded_replies = p.decode_replies(replies);
    const FelixClientThread::Reply & reply = decoded_replies.at(0);
    REQUIRE( reply.status   == FelixClientThread::Status::ERROR_INVALID_ARGS );
}


TEST_CASE( "ParseMultiple", "[Multiple commands]" )
{
    std::vector<Command> commands = {
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME","0x1"} //no value given
        },
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME", "0x2"} //no value given
        },
        Command {
            .uuid = "0x1234",
            .cmd = FelixClientThread::Cmd::SET,
            .cmd_args = {"REGISTER_NAME", "0x3"} //no value given
        }
    };

    auto parsed_cmds = p.parse_commands(p.encode_commands(commands));
    REQUIRE( parsed_cmds.size() == commands.size() );

    for (unsigned int i = 0; i < commands.size(); ++i) {
        ReqData parsed_msg = parsed_cmds.at(i);
        auto status = static_cast< FelixClientThread::Status>(parsed_msg.status_code);
        auto value = parsed_msg.value;
        REQUIRE( value == commands.at(i).cmd_args.at(1) );
        REQUIRE( status == FelixClientThread::Status::OK );
    }
    auto decoded_replies = p.decode_replies(p.encode_replies(0x1200, parsed_cmds));
    REQUIRE( decoded_replies.size() == commands.size() );
    for (unsigned int i = 0; i < commands.size(); ++i) {
        std::string command_value = commands.at(i).cmd_args.at(1);
        REQUIRE( decoded_replies.at(i).value == std::stoul(command_value, nullptr, 16) );
    } 
}
