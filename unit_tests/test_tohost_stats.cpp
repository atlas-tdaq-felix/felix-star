#include <sys/types.h>
#include <iostream>
#include <catch2/catch_test_macros.hpp>

#include "felix/felix_fid.h"
#include "tohost_monitor.hpp"
#include "util.hpp"

TEST_CASE( "ToHost json structure", "[One elink/decoder, one reader, one buffer, one device]" )
{
    std::string serial_msg;
    nlohmann::json parsed;

    int device = 1;
    int dma = 2;
    int buffer = 3;

    ToHostElinkStats elink_stats(0x8);
    elink_stats.processed_blocks = 4;
    elink_stats.processed_chunks = 16;

    ToHostReaderStats reader_stats(buffer, 0, 1, 64, 0);
    ToHostDmaStats dma_stats(dma, 50, 999);
    ToHostDeviceStats device_stats(device);

    std::string fifoname = "";
    ToHostMonitor mon(fifoname);
    std::string ts = Util::ISO8601TimeUTC();
    std::string hostname = Util::get_full_hostname();

    mon.append_device_stats(ts, hostname, device_stats);
    mon.append_dma_stats(ts, hostname, device, dma_stats);
    serial_msg = mon.get_serialized_message();
    std::cout << serial_msg << std::endl;
    parsed = nlohmann::json::parse(serial_msg);

    REQUIRE( parsed["ts"] == ts);
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["dma_free_MB"] == 50 );
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["irqs"] == 999 );

    mon.append_reader_stats(ts, hostname, device, dma, reader_stats);
    serial_msg = mon.get_serialized_message();
    std::cout << serial_msg << std::endl;
    parsed = nlohmann::json::parse(serial_msg);

    REQUIRE( parsed["ts"] == ts);
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["net_calls"] == 0);
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["net_resources"] == 64);
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["subscriptions"] == 1);
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["type"] == 0);

    mon.append_elink_stats(ts, hostname, device, dma, buffer, elink_stats);
    serial_msg = mon.get_serialized_message();
    std::cout << serial_msg << std::endl;
    parsed = nlohmann::json::parse(serial_msg);

    REQUIRE( parsed["ts"] == ts);
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["fid"]["0x8"]["blocks"] == 4 );
    REQUIRE( parsed["host"][hostname]["app"]["tohost"]["device"][std::to_string(device)]["dma"][std::to_string(dma)]["thread"][std::to_string(buffer)]["fid"]["0x8"]["chunks"] == 16 );
}
