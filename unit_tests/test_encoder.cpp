#include <iostream>
#include <format>
#include <span>
#include <array>
#include <catch2/catch_test_macros.hpp>

#include "encoder.hpp"


void print_msg(std::span<uint8_t> encoded_msg, int block_size)
{
    int idx = 0;
    for (const auto & b : encoded_msg) { 
        std::cout << std::format("{:02x}", b) << " ";
        ++idx;
        if (!(idx % 32)){std::cout << std::endl;}
        if (!(idx % block_size)){std::cout << std::endl;}
    }
}

std::vector<uint8_t> make_msg(size_t len)
{
    std::vector<uint8_t> msg;
    for(size_t i = 0; i < len; ++i){
        uint8_t bytes = (i & 0xFF);
        msg.push_back(bytes);
    }
    return msg;
}


#if REGMAP_VERSION >= 0x0500

struct FromHostHeader {
    uint8_t length;
    uint8_t axis;
    uint8_t link;
    uint8_t reserved;
} __attribute__((packed));


FromHostHeader* parse_hdr(std::span<uint8_t> dest)
{
    uint8_t* ptr = (uint8_t*)&dest.back() - sizeof(FromHostHeader) + 1;
    FromHostHeader* hdr = reinterpret_cast<FromHostHeader*>(ptr);
    std::cout << "Header len "      << std::format("{:#04x}",hdr->length) << " ";
    std::cout << "axis "     << std::format("{:#04x}",hdr->axis) << " ";
    std::cout << "link "     << std::format("{:#04x}",hdr->link) << " ";
    std::cout << "reserved " << std::format("{:#04x}",hdr->reserved) << std::endl;
    return hdr;
}


local_elink_t get_elink(FromHostHeader* hdr){
    return ((hdr->link << 8) & 0xFF00) | hdr->axis;
}


TEST_CASE( "PCIegGen3", "[multi-block]" )
{
    constexpr size_t dest_size{256};
    constexpr size_t msg_size{180};
    constexpr local_elink_t elink{0x8};

    std::array<uint8_t, dest_size> dest;
    std::vector<uint8_t> msg = make_msg(msg_size);
    
    auto enc = Encoder();
    enc.set_data_format(FROMHOST_FORMAT_HDR32_PACKET32_8B);
    enc.set_destination_parameters(dest.data(), dest_size);

    size_t dest_i = enc.encode_and_write_msg(elink, msg.data(), msg.size(), 0);
    dest_i = dest_i == 0 ? dest_size : dest_i; //if dest_i 0, the buffer is full

    print_msg(std::span(dest.data(), dest_i), enc.get_block_size());

    int block_i(0);
    while (dest_i > 0){

        FromHostHeader* hdr = parse_hdr(std::span(dest.data(), dest_i));
        //elink
        REQUIRE( get_elink(hdr) == elink );
        //length
        if (block_i == 0){
            REQUIRE( hdr->length == enc.compute_final_bytes(msg_size) );
        } else {
            REQUIRE( hdr->length == 0xFF );
        }

        dest_i -= enc.get_block_size();
        ++block_i;
    }
}


TEST_CASE( "PCIegGen4", "[multi-block]" )
{
    constexpr size_t dest_size{256};
    constexpr size_t msg_size{180};
    constexpr local_elink_t elink{0x8};

    std::array<uint8_t, dest_size> dest;
    std::vector<uint8_t> msg = make_msg(msg_size);
    
    auto enc = Encoder();
    enc.set_data_format(FROMHOST_FORMAT_HDR32_PACKET64_8B);
    enc.set_destination_parameters(dest.data(), dest_size);

    size_t dest_i = enc.encode_and_write_msg(elink, msg.data(), msg.size(), 0);
    dest_i = dest_i == 0 ? dest_size : dest_i; //if dest_i 0, the buffer is full

    print_msg(std::span(dest.data(), dest_i), enc.get_block_size());

    int block_i(0);
    while (dest_i > 0){

        FromHostHeader* hdr = parse_hdr(std::span(dest.data(), dest_i));
        //elink
        REQUIRE( get_elink(hdr) == elink );
        //length
        if (block_i == 0){
            REQUIRE( hdr->length == enc.compute_final_bytes(msg_size) );
        } else {
            REQUIRE( hdr->length == 0xFF );
        }

        dest_i -= enc.get_block_size();
        ++block_i;
    }
}


TEST_CASE( "PCIegGen5", "[multi-block]" )
{
    constexpr size_t dest_size{256};
    constexpr size_t msg_size{180};
    constexpr local_elink_t elink{0x8};

    std::array<uint8_t, dest_size> dest;
    std::vector<uint8_t> msg = make_msg(msg_size);
    
    auto enc = Encoder();
    enc.set_data_format(FROMHOST_FORMAT_HDR32_PACKET128_8B);
    enc.set_destination_parameters(dest.data(), dest_size);

    size_t dest_i = enc.encode_and_write_msg(elink, msg.data(), msg.size(), 0);
    dest_i = dest_i == 0 ? dest_size : dest_i; //if dest_i 0, the buffer is full

    print_msg(std::span(dest.data(), dest_i), enc.get_block_size());

    int block_i(0);
    while (dest_i > 0){

        FromHostHeader* hdr = parse_hdr(std::span(dest.data(), dest_i));
        //elink
        REQUIRE( get_elink(hdr) == elink );
        //length
        if (block_i == 0){
            REQUIRE( hdr->length == enc.compute_final_bytes(msg_size) );
        } else {
            REQUIRE( hdr->length == 0xFF );
        }

        dest_i -= enc.get_block_size();
        ++block_i;
    }
}

#else

struct FromHostHeader {
    uint8_t end    : 1;
    uint8_t len    : 4; //in 2-byte words
    uint8_t epath  : 3;
    uint8_t egroup : 3;
    uint8_t link   : 5;
} __attribute__((packed));


FromHostHeader* parse_hdr(std::span<uint8_t> dest)
{
    uint8_t* ptr = (uint8_t*)&dest.back() - sizeof(FromHostHeader) + 1;
    FromHostHeader* hdr = reinterpret_cast<FromHostHeader*>(ptr);
    std::cout << "Header end bit " << std::format("{:#04x}", (uint8_t)(hdr->end)   ) << " ";
    std::cout << "len "            << std::format("{:#04x}", (uint8_t)(hdr->len*2) ) << " ";
    std::cout << "epath "          << std::format("{:#04x}", (uint8_t)(hdr->epath) ) << " ";
    std::cout << "egroup "         << std::format("{:#04x}", (uint8_t)(hdr->egroup)) << std::endl;
    std::cout << "link "           << std::format("{:#04x}", (uint8_t)(hdr->link)  ) << std::endl;
    return hdr;
}


local_elink_t get_elink(FromHostHeader* hdr){
    constexpr int BLOCK_LNK_SHIFT = 6;
    constexpr int BLOCK_EGROUP_SHIFT = 3;
    return ((hdr->link << BLOCK_LNK_SHIFT) | (hdr->egroup << BLOCK_EGROUP_SHIFT) | hdr->epath);
}


TEST_CASE( "rm4", "[basic]" )
{
    constexpr size_t dest_size{256};
    constexpr size_t msg_size{180};
    constexpr local_elink_t elink{0x8};

    std::array<uint8_t, dest_size> dest;
    std::vector<uint8_t> msg = make_msg(msg_size);
    
    auto enc = Encoder();
    enc.set_data_format(FROMHOST_FORMAT_REGMAP4);
    enc.set_destination_parameters(dest.data(), dest_size);

    size_t dest_i = enc.encode_and_write_msg(elink, msg.data(), msg.size(), 0);
    dest_i = dest_i == 0 ? dest_size : dest_i; //if dest_i 0, the buffer is full

    print_msg(std::span(dest.data(), dest_i), enc.get_block_size());

    int block_i(0);
    while (dest_i > 0){

        FromHostHeader* hdr = parse_hdr(std::span(dest.data(), dest_i));
        //elink
        REQUIRE( get_elink(hdr) == elink );
        //length
        if (block_i == 0) {
            REQUIRE( hdr->end == 1 );
            REQUIRE( hdr->len == enc.compute_final_bytes(msg_size)/2 ); //len is units of in 2-byte work
        } else {
            REQUIRE( hdr->end == 0 );
            REQUIRE( hdr->len == 0xF );
        }

        dest_i -= enc.get_block_size();
        ++block_i;
    }
}


TEST_CASE( "rm4", "[no-odd-size]" )
{
    constexpr size_t dest_size{256};
    constexpr size_t msg_size{9};
    constexpr local_elink_t elink{0x8};

    std::array<uint8_t, dest_size> dest;
    std::vector<uint8_t> msg = make_msg(msg_size);
    
    auto enc = Encoder();
    enc.set_data_format(FROMHOST_FORMAT_REGMAP4);
    enc.set_destination_parameters(dest.data(), dest_size);

    size_t dest_i = enc.encode_and_write_msg(elink, msg.data(), msg.size(), 0);
    dest_i = dest_i == 0 ? dest_size : dest_i; //if dest_i 0, the buffer is full

    print_msg(std::span(dest.data(), dest_i), enc.get_block_size());

    int block_i(0);
    while (dest_i > 0){

        FromHostHeader* hdr = parse_hdr(std::span(dest.data(), dest_i));
        //elink
        REQUIRE( get_elink(hdr) == elink );
        REQUIRE( hdr->end == 1 );
        REQUIRE( hdr->len == 0xa / 2 ); //len is units of in 2-byte work
        
        dest_i -= enc.get_block_size();
        ++block_i;
    }
}

#endif
