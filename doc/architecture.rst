Software Architecture
=====================

Code Structure
---------------------

Felix-star is written using in modern C++ using an object-oriented approach.
All code is required to be testable even without FELIX or network hardware
and code replication shall be null or minimal.
As a consequence a component is often described by an abstract class that
defined the interface and two derived implementations, one that interfaces
with hardware, the other that emulates the dedicated hardware.
To minimize runtime polymorphism, classes are templated such that they can
have as member either implementation of another class.


Testing
---------------------
Unit Tests
.....................

Algorithms implemented in felix-star are subject to unit testing.
Unit-tests use the Catch [#]_ framework, are run by CTest and are kept in the
`unit-test` folder.

.. [#] `Catch2  <https://github.com/catchorg/Catch2>`_.


Integration Tests
.....................

Integration tests run multiple process verify the interaction of executables.
The framework is `felix-unit-test  <https://gitlab.cern.ch/atlas-tdaq-felix/felix-unit-test>`_
which makes use of the `supervisor  <http://supervisord.org/>`_ orchestrator.
This type of test verifies also network connectivity and can be extended to
use FELIX hardware. These tests use the Python steering files stored in the 
`test` folder.


Threading Model
---------------------

Felix-star applications are based on the event loop provided by the network
library. Following the reactor pattern [#]_, the event loop reacts to events
by invoking the corresponding callback function. Callback functions are
executed by the event loop thread to avoid thread synchronisation.
Scalability is achieved by running multiple event loops (which translated
in multiple publishers or receivers). synchronisation across different event
loops might be necessary, but the synchronisation point shall be encountered
at the lowest possible rate.

.. [#] `Reactor pattern on Wikipedia <https://en.wikipedia.org/wiki/Reactor_pattern>`_

.. warning::
  Because callbacks are executed by the event loop thread it is not possible
  to implement blocking callbacks. Blocking code will stop event loop from
  processing further events and may lead to hanging or dead-locks.
