.. _common:

Common Components
=====================

Classes that are used by multiple felix-star executables are presented below.


Device class
---------------------

The abstract device class is here presented.
The derived classes are described in the complete Doxygen documentation.

.. doxygenclass:: Device
  :no-link:
  :members:



DMA buffers
---------------------

.. doxygenclass:: DmaBuffer
  :no-link:
  :members:


Network wrappers
---------------------

Interface classes are defined to describe the required functionality of a
network publisher and receiver while abstracting the implementation.

.. doxygenclass:: Publisher
  :no-link:
  :members:


.. doxygenclass:: Receiver
  :no-link:
  :members:


Felix-bus wrapper
---------------------

Felix-bus is wrapped by a class that thus defines the interface with the rest
of the code. In this way, changes to the felix-bus package would be affect only
this part of the code.

.. doxygenclass:: Bus
  :no-link:
  :members:



Monitoring data
---------------------

Monitoring data produced by applications is JSON encoded and printed to
a UNIX FIFO. The content of the JSON string is application-specific but
I/O aspects are common.

.. doxygenclass:: Monitor
  :no-link:
  :members:


I/O with permanent storage
---------------------

Emulation of FELIX hardware can require reading or writing in files or
UNIX FIFOs. A class has been implement to simplify the task.

.. doxygenclass:: DiskIO
  :no-link:
  :members:
