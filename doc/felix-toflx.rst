.. _felix-toflx:

felix-toflx
===========================

Felix-toflx is the application that receives messages from the network,
encodes them into FromHost messages and transfers them to the corresponding
FromHost DMA buffer. Felix-toflx can serve multiple devices (each device
has a single FromHost DMA buffer).

Top-level Class
----------------------

The top-level class is `FromHost`, used to build two executables: `felix-toflx`
that works with a FELIX card and `felix-toflx2file` that emulates the hardware.
`FromHost` is templated to support both use cases. 
The template parameters are three: the configuration parameters (emulating
hardware requires passing in more parameters), the type of FromHost DMA buffer
(that interfaces with the FELIX driver or not) and the type of FELIX device
(real or emulated). `FromHost` contains pointers to devices, DMA buffers
and writers.

.. doxygenclass:: FromHost
  :no-link:
  :members:

Writers
----------------------

`FromHostWriter` serves a set of FromHost links and owns a network Receiver.
The Receiver posts on the Bus the e-links and the corresponding network
configuration. The Receiver also runs the event loop for which `FromHostWriter`
functions are registered as callbacks.
Network messages are parsed and written to the FRomHost DMA buffer using
the FromHost buffer interface.

.. note::
  Felix-toflx uses the send/receive network pattern, not publish/subscribe.

.. warning::
  Network buffers used in the send/receive patter by netio-next do not include
  any e-link identifier. For this reason each message is preceded by a `ToFlxHeader`.
  Netio3 buffer structures will include the e-link identifier and such header
  will disappear.

.. doxygenclass:: FromHostWriter
  :no-link:
  :members:

FromHost Buffer
----------------------

The *FromHost* DMA buffer is described by the `FromHostBuffer` class,
specialised to the case in which software writes and firmware reads
(as opposed to the *ToHost* case) in a circular buffer.
The circular [#]_ *FromHost* DMA buffer is supported; the "one-shot"
buffer allocated per transaction is no longer supported.

`FromHostBuffer` is an abstract class from which two implementations derive:
one to interface with a FELIX card and another one in which the behaviour
of the card is emulated. In the latter case, a dedicated thread is spawned to
read  the DMA buffer (optionally writing data to a file or UNIX fifo) and
advance the read pointer.

A message passed from `FromHostWriter` for writing is first encoded by
the `Encoder` owned by `FromHostBuffer`. The write pointer is then advanced
according to the size of the encoded message that is typically larger than
the original size.
The encoding and writing is run by the `FromHostWriter` thread.
A mutex regulates the writer's access to the buffer but it is locked only
if multiple writers are used.

.. [#] or "continuos" in flxcard jargon.


.. doxygenclass:: FromHostBuffer
  :no-link:
  :members:


Encoder
----------------------

Messages coming from the network must be encoded according to the
"Central Rounter FromHost data format" described in the firmware specification
document. In short, messages have to be split in chunks of fixed size
(the size depends on the FELIX card PCIe generation) preceded by an header
that specified the e-link identifier and message size.

.. doxygenclass:: Encoder
  :no-link:
  :members:


Monitoring
----------------------

Monitoring information consists of a JSON string containing statistics regarding
the encoder, writers, and DMA buffers / devices. The JSON string can be written
to a UNIX fifo such that another application can read it and forward it where
needed. The `FromHostMonitor` class provides methods to assemble the JSON message
from the inputs. As in the `ToHostMonitor` case, there is no thread
synchronisation in the retrieval of monitoring information. The main application
threads copies the counters without resetting them and computes the increment
with respect to the previous copy.


.. doxygenclass:: FromHostMonitor
  :no-link:
  :members:
