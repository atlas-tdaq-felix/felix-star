.. _felix-register:

felix-register
===========================

Felix-register allows to edit registers from a remote endpoint.
On the receiving side, felix-register advertises one *command* e-link per device
whose identifier is built using a predefined value, the detector ID and the
connector ID of the device. Messages bound to the command e-link are encoded
in JSON and contain the command type (e.g. GET, SET) and the name of the
register to act upon. Once the command has been executed, or an error occurred,
a reply is sent over the *reply* e-link. The reply is also in JSON format.

In addition, felix-register publishes monitoring information regarding the
hardware status of each card.

.. note::
  The reception of commands uses the send/receive pattern. Replies are sent
  using the publish/subscribe pattern.


Top-level Class
----------------------

The top-level class is `Register`, used to build two executables: `felix-register`
that works with a FELIX card and `felix-fileregister` that emulates the hardware.
`Register` is templated to support both use cases.
`Register` supports multiple devices and runs two threads: one thread
for the event loop shared by all the receivers and publishers for the register
editing, and one run by the publisher of hardware monitoring information.

.. important::
  Some registers are associated only to the primary device of a card as reported in the `FELIX register map <https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/regmap/>`_.
  Therefore, `Register` will make sure that both primary and secondary devices are open. If the primary is not, it will open it.

.. doxygenclass:: Register
  :no-link:
  :members:


Network Front-End
----------------------

`RegisterDeviceController` is the class that owns a receiver and a publisher
for each device associated to the event loop owned by `Register`.
`RegisterDeviceController`'s methods are registered as callbacks.

.. doxygenclass:: RegisterDeviceController
  :no-link:
  :members:


Command Parser
----------------------

Messages received by `RegisterDeviceController` are parsed by `RegisterMsgParser`.
The same class encodes replies. Therefore defines the message serialisation and
deserialisation.

.. doxygenclass:: RegisterMsgParser
  :no-link:
  :members:


RegisterMap Manager
----------------------

Provided that a command is well formatted, the legitimacy of the request is
checked before interacting with the hardware.
`RegmapManager` checks the register type (read or write) and PCIe endpoint. 
If the register can be written only from the primary device but the request came
for the secondary, the command will have to be rerouted by the upper layers.

.. doxygenclass:: RegmapManager
  :no-link:
  :members:


Device Interface
----------------------

The interaction with the card happens via `RegisterDeviceInterface`.
This class is templated such that is can act on a physical or emulated device.

.. doxygenclass:: RegisterDeviceInterface
  :no-link:
  :members:


Hardware Monitoring
----------------------

The hardware monitor is implemented by two classes, `HwMonitor` that owns the
publisher and interacts with the network, and and `HwMonBackend` that
gathers the data using the `Device` methods and prepares the message.

.. doxygenclass:: HwMonitor
  :no-link:
  :members:

.. doxygenclass:: HwMonBackend
  :no-link:
  :members:
