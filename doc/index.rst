.. felix-star documentation master file, created by
   sphinx-quickstart on Fri Apr 12 15:38:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Felix-star Developer's Manual
=============================

Last compilation from master branch: |today|

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   architecture
   common
   felix-tohost
   felix-toflx
   felix-register
   doxy/full_doxy
