# Felix-star
Felix-star is a suite of applications that bridge FELIX with the network.

### Developer's Documentation
https://atlas-project-felix.web.cern.ch/atlas-project-felix/felix-star/master/index.html

### Repository structure

`src`: felix-star source files  
`examples`: source files of additional application (e.g. clients)  
`test`: CI tests: Python scripts, supervisord configurations  
`unit_tests`: CI unit tests  
`scripts`: Python utilities  
`doc`: developer's documentation