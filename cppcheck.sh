#!/bin/sh

REGMAP_VERSION=$1

BASEDIR=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))

cppcheck --version

(cd ${BASEDIR}; cppcheck -j 4 -DREGMAP_VERSION=${REGMAP_VERSION} --error-exitcode=1 --enable=all --std=c++20 --includes-file=cppcheck.include --suppressions-list=cppcheck.suppress --suppressions-list=cppcheck.${REGMAP_VERSION}.suppress src unit_tests/ examples/)
