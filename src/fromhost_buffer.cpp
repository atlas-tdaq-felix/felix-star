#include "fromhost_buffer.hpp"
#include "log.hpp"
#include <cstdint>
#include <chrono>

void FromHostBuffer::encode_and_write(uint64_t elink, const uint8_t* source, size_t size, bool trickle)
{   
    // If trickle is true just start writing from the beginning of the buffer
    uint64_t dest_offset = (trickle) ? 0 : dma_get_write_offset();
    LOG_DBG("Encoding msg for elink 0x%x at CMEM buffer offset 0x%lx", elink, dest_offset);
    uint64_t new_dest_offset = m_encoder.encode_and_write_msg(elink, source, size, dest_offset);
    dma_set_write_offset(new_dest_offset);
    ++m_mon.msg_counter;
    m_mon.bytes_counter += size;
}


FromHostDmaStats FromHostBuffer::get_monitoring_data()
{
    FromHostDmaStats stats = m_mon.get_increment(m_mon_prev);
    stats.dmaid = m_dmaid;
    stats.dma_free_MB = dma_get_free_MB();
    return stats;
}
