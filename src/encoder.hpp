#ifndef FELIX_FROMHOST_ENCODER_H_
#define FELIX_FROMHOST_ENCODER_H_

#include "device.hpp"

/**
 * Encodes network messages into FromHost blocks.
 * */
class Encoder {

    public:

        /**
         * @brief Encoder default contructor.
         */
        Encoder() = default;

        /**
         * @brief Sets the data format of the encoder.
         * @param format FromHost data format supported by firmware.
         */
        void set_data_format(int format);

        /**
         * @brief set address and size of buffer where encoded messages are written.
         * @param dest_start start address of output buffer.
         * @param dest_size size of output buffer.
         */
        void set_destination_parameters(uint8_t* dest_start, uint32_t dest_size);

        /**
         * @param size input message size.
         * @return size of encoded message.
         */
        [[nodiscard]] size_t compute_max_msg_occupancy(size_t size) const;

        /**
         * @param size input message size.
         * @return number of full FromHost blocks.
         */
        [[nodiscard]] int compute_full_blocks(size_t size) const;

        /**
         * @param size input message size.
         * @return number of bytes in the last block of the encoded message.
         */
        [[nodiscard]] size_t compute_final_bytes(size_t size) const;

        /**
         * @return FromHost block size.
         */
        [[nodiscard]] size_t get_block_size() const {return m_block_bytes;};

        /**
         * @brief encodes the input and write it in the output buffer.
         * @param elink of the incoming message.
         * @param source address of the incoming message.
         * @param size of the incoming message.
         * @param dest_offset current write pointer offset in the output buffer.
         * @return new write pointer as byte index.
         */
        [[nodiscard]] uint64_t encode_and_write_msg(uint64_t elink, const uint8_t *source, size_t size, uint32_t dest_offset);

    private:
        uint8_t* m_dest_start{nullptr};
        uint32_t m_dest_size{0};

        //Default: FROMHOST_FORMAT_HDR32_PACKET32_8B (rm- >=5.1 PCIe Gen3)
        int m_header_size{4};
        int m_block_bytes{32};
        int m_payload_bytes{28};
        int m_length_mask{TOFLX_LENGTH_MASK_HDR32_8B};
        int m_elink_shift{TOFLX_ELINK_SHIFT_HDR32_8B};
};

#endif /* FELIX_FROMHOST_ENCODER_H_ */ 