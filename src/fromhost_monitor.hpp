#ifndef FELIX_FROMHOST_MONITOR_H_
#define FELIX_FROMHOST_MONITOR_H_

#include "monitor.hpp"
#include <cstdint>


/**
 * Monitoring info for one Device.
 */
struct FromHostDeviceStats
{
    int device_id;

    explicit FromHostDeviceStats(int d)
        : device_id(d) {};
};


/**
 * Data structure for the monitoring of a FromHost DMA buffer.
 * */
struct FromHostDmaStats
{
    int dmaid;
    uint32_t dma_free_MB;
    uint64_t msg_counter;
    float msg_rate_Hz;
    uint64_t bytes_counter;
    float msg_rate_Mbps;
    struct timespec ts;

    FromHostDmaStats get_increment(FromHostDmaStats & stats);

    FromHostDmaStats()
        : dmaid(0), dma_free_MB(0), msg_counter(0), msg_rate_Hz(0), bytes_counter(0), msg_rate_Mbps(0)
        {
            clock_gettime(CLOCK_MONOTONIC_RAW , &ts);
        };

    explicit FromHostDmaStats(uint32_t d, uint32_t MB, uint64_t msg, uint64_t bytes)
        : dmaid(d), dma_free_MB(MB), msg_counter(msg), msg_rate_Hz(0), bytes_counter(bytes), msg_rate_Mbps(0)
        {
            clock_gettime(CLOCK_MONOTONIC_RAW , &ts);
        };
};


/**
 * Data structure for the monitoring of a FromHost writer.
 * Multiple writers can share the same FromHost buffers.
 * */
struct FromHostWriterStats
{
    int writer_id;
    char type;
    unsigned int number_of_connections;

    explicit FromHostWriterStats(int wi, char t, uint32_t s) :
        writer_id(wi),
        type(t),
        number_of_connections(s) { };
};


/**
 * Data structure for the monitoring of a FromHost e-link.
 * */
struct FromHostElinkStats
{
    uint64_t fid              = 0;
    uint64_t processed_msg    = 0;
    uint64_t processed_bytes  = 0;
    uint64_t largest_msg_size = 0;
    float rate_msg_Hz         = 0;
    float rate_msg_Mbps       = 0;
    uint64_t dropped_msg      = 0;
    struct timespec ts;

    void on_processed_msg(uint32_t size_bytes);
    FromHostElinkStats get_increment(FromHostElinkStats & stats);
    FromHostElinkStats() = default;

    explicit FromHostElinkStats(uint64_t f) : fid(f), processed_msg(0),  processed_bytes(0),
        largest_msg_size(0), rate_msg_Hz(0), rate_msg_Mbps(0), dropped_msg(0)
        {
            clock_gettime(CLOCK_MONOTONIC_RAW , &ts);
        };
};


/**
 * Data structure for the monitoring FromHost transfers of a device.
 * It contains information about the DMA buffer, the associated writers and
 * the e-links served by each writer.
 * */
class FromHostMonitor : public Monitor
{
    public:
        explicit FromHostMonitor(std::string& fifoname) : Monitor(fifoname) {
        }
        //These functions are called in a nested loop with hierarchy
        //devices > dmas > writers > elinks.
        void append_device_stats(const std::string& ts, const std::string& hostname, const FromHostDeviceStats & s);
        void append_dma_stats(const std::string& ts, const std::string& hostname, int deviceid, const FromHostDmaStats & s);
        void append_writer_stats(const std::string& ts, const std::string& hostname, int deviceid, int dma_id, const FromHostWriterStats & s);
        void append_elink_stats(const std::string& ts, const std::string& hostname, int device, int dma_id, int reader_id, const FromHostElinkStats & s);
};

#endif /* FELIX_FROMHOST_MONITOR_H_ */
