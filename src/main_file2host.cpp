#include "tohost.hpp"
#include "log.hpp"

static volatile std::sig_atomic_t signal_status;
void signal_handler(int s){signal_status = s;}

int main(int argc, char** argv)
{
    std::unique_ptr<ConfigFileToHost> c = std::make_unique<ConfigFileToHost>();
    c->parse(argc, argv);

    auto th = ToHost<ConfigFileToHost, FileDevice, FileToHostBuffer>(std::move(c));

    for (auto & dev_no : th.cfg->resource.device){
        th.devices.emplace_back(new FileDevice(*th.cfg, dev_no));
        for(int id : th.cfg->resource.dma_ids) {
            int unique_id = th.cfg->get_unique_dmaid(id, dev_no);
            th.dma_buffers.emplace(
                unique_id,
                new FileToHostBuffer(id, th.devices.back(), th.cfg->file, th.cfg->block_rate, th.cfg->repeat)
            );
        }
    }

    th.start();

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (signal_status == 0) {
        th.print_monitoring();
        usleep(th.cfg->stats.monitoring_period_ms * 1000);
    }
    th.stop();
    return 0;
}
