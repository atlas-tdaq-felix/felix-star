#include "config.hpp"
#include "device.hpp"

ConfigFile::ConfigFile() {}

ConfigFile::~ConfigFile() {}

std::ostream& ConfigFile::format(std::ostream& os) const {
    os << "ConfigFile" << std::endl;
    os << "regmap: " << regmap << std::endl;
    os << "vmem: " << vmem << std::endl;
    os << "file: " << file << std::endl;
    os << "channels: " << channels << std::endl;
    os << "block_size: " << block_size << std::endl;
    os << "toflx_dmaid: " << toflx_dmaid << std::endl;
    os << "firmware: " << firmware << std::endl;
    os << std::endl;
    return os;
}

std::ostream& operator<<(std::ostream& os, const ConfigFile& c) {
    return c.format(os);
}

std::string ConfigFile::options() {
    return
R"(
    File Options:
    -e, --elink=DMAID:LID ...       Enable e-link LID on DMAID e.g. 0:0x08 or 1:0x09.
        --no-of-elinks=N            Enable N additional elinks to the list passed with -e. [default: 0]
                                    Added sequentially wrt the last elink passed on same DMA id.
        --block-size=SIZE           ToHost block size, multiple of 1024. [default: 1024]
        --channels=SIZE             Number of links of emulated PCIe endpoint. [default: 12]
        --firmware=SIZE             Emulated firmware: GBT=0, FULL=1, LPGBT=9. [default: 0]
        --pcie-gen=VALUE            Used to determine FromHost data format. [default: 3]
        --file-is-fifo              Use a FIFO instead of a file. [default: 0]
        --regmap=hex_version        Register map version. [calculated: from compile definition of REGMAP_VERSION]
        --toflx-dma=VALUE           Last DMA id per PCIe endpoint, reserved for ToFlx [default: 5]
        --vmem                      Use virtual memory instead of cmem. [default: 0]
)";
}

void ConfigFile::handle_cmd_line(std::map<std::string, docopt::value> args) {

    file = args["FILE"].asString();

    int last_dmaid = 0;
    for(auto entry : args["--elink"].asStringList() ){
        std::string::size_type n = entry.find(":");
        int dmaid = stoi(entry.substr(0, n));
        uint16_t elink = std::stoul(entry.substr(n+1), nullptr, 16);
        elinks[dmaid].push_back(elink);
        last_dmaid = dmaid;
    }

    unsigned no_of_elinks = args["--no-of-elinks"].asLong();
    if (no_of_elinks > 0){
        uint16_t last_elink = elinks.at(last_dmaid).back();
        for (unsigned int i = last_elink + 1; i <= no_of_elinks; ++i){
            elinks.at(last_dmaid).push_back(i);
        }
    }

    vmem = args["--vmem"].asBool();
    regmap = args["--regmap"] ? args["--regmap"].asLong() : REGMAP_VERSION;
    block_size = args["--block-size"].asLong();
    channels = args["--channels"].asLong();
    toflx_dmaid = args["--toflx-dma"].asLong();
    firmware = args["--firmware"].asLong();
    fifo_as_file = args["--file-is-fifo"].asBool();

    int pcie_gen = args["--pcie-gen"].asLong();
    if (pcie_gen == 0){
        fromHostDataFormat = flx_fromhost_format::FROMHOST_FORMAT_5BIT_LENGTH;
    }
    else if (pcie_gen == 1){
        fromHostDataFormat = flx_fromhost_format::FROMHOST_FORMAT_HDR32_PACKET32;
    }
    else if (pcie_gen == 2){
        fromHostDataFormat = flx_fromhost_format::FROMHOST_FORMAT_HDR32_PACKET64;
    }
    else if (pcie_gen == 5){
        fromHostDataFormat = flx_fromhost_format::FROMHOST_FORMAT_HDR32_PACKET128_8B;
    }
    else if (pcie_gen == 4){
        fromHostDataFormat = flx_fromhost_format::FROMHOST_FORMAT_HDR32_PACKET64_8B;
    } else {
        fromHostDataFormat = flx_fromhost_format::FROMHOST_FORMAT_HDR32_PACKET32_8B;
    }

    //Variables for device emulation
    lock_mask = 0;
    wide_mode = false;
    status_leds = 0;
    max_tlp_bytes = 512;
}
