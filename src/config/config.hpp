#ifndef FELIX_STAR_CONFIG_H_
#define FELIX_STAR_CONFIG_H_

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <iterator>
#include <sstream>
#include <algorithm>

#include <map>
#include "block.hpp"                    //for MAX_BUF_CHUNK_SIZE
#include "felix/felix_fid.h"
#include "felix/felix_ports.h"          //PORT macros
#include "felix/felix_client_util.hpp"  //ip-iface conversion

#include "docopt/docopt.h"


enum class EventLoopType {
    netio_next,
    netio3_native,
    netio3_asio,
};

struct NetworkConfig
{
    std::string ip{"127.0.0.1"};
    std::map<int, int> ports;
    bool daq_unbuffered{false};
    bool daq_tcp{false};
    unsigned netio_pages{256};
    uint32_t netio_pagesize{65536};
    uint32_t netio_watermark{64511};
    unsigned long netio_timeout{0};

    std::string dcs_ip{""};
    std::map<int, int> dcs_ports;
    bool dcs_tcp{false};
    bool dcs_unbuffered{true};
    unsigned dcs_netio_pages{128};
    uint32_t dcs_netio_pagesize{6144};
    uint32_t dcs_netio_watermark{6100};
    unsigned long dcs_netio_timeout{10000};

    std::map<int, int> ttc_ports;
    unsigned ttc_netio_pages{64};
    uint32_t ttc_netio_pagesize{1536};
    uint32_t ttc_netio_watermark{1280};
    unsigned long ttc_netio_timeout{1000};

    std::string bus_dir{""};
    std::string bus_groupname{"FELIX"};
    bool verbose_bus{false};

    EventLoopType evloop_type{EventLoopType::netio_next};
};


struct ResourceConfig
{
    std::vector<uint16_t> device;
    std::vector<uint16_t> cid;
    std::vector<int> dma_ids;
    uint8_t vid{1};
    uint16_t did{0};
    size_t cmem_buffersize{1024*1024*1024};
    bool free_previous_cmem{true};
    bool toflx{false};
    bool use_file{false};
};


struct MonitoringConfig
{
    std::string monitoring_fifo{""};
    int monitoring_period_ms{1000};
};

/**
 * Base configuration data structure used by felix-tohost and felix-toflx.
 */
class Config
{
    public:
        Config();
        virtual ~Config();
        virtual void parse(int argc, char **argv);
        virtual std::ostream& format(std::ostream& os) const;
        friend std::ostream& operator<<(std::ostream& os, const Config& c);
        int get_number_devices();
        uint16_t get_device_cid(unsigned int dev_no);
        int get_unique_dmaid(int dmaid, uint16_t device);
        int udmaid_to_deviceid(int udmaid);
        int udmaid_to_dmaid(int udmaid);

    protected:
        virtual std::string usage() { return "undefined"; } // = 0; FIXME, can only be added if we do not instantiate Config, needs to be reference (statistics, register)
        virtual std::string options();
        virtual std::string bugs();
        virtual void handle_cmd_line(std::map<std::string, docopt::value> args);

    public:
        bool verbose{false};
        std::string appname{""};
        ResourceConfig resource;
        MonitoringConfig stats;
        NetworkConfig network;

        // derived for easy class check
        bool use_file{false};
        bool vmem{false};

    private:
        [[nodiscard]] static std::string evloop_type_to_str(EventLoopType type);
        [[nodiscard]] static EventLoopType evloop_type_from_str(std::string_view type);
};


class ConfigFile : public virtual Config
{
    public:
        ConfigFile();
        virtual ~ConfigFile();
        virtual std::ostream& format(std::ostream& os) const override;
        friend std::ostream& operator<<(std::ostream& os, const ConfigFile& c);

    protected:
        virtual std::string options() override;
        virtual void handle_cmd_line(std::map<std::string, docopt::value> args) override;

    public:
        u_int    firmware{0};
        u_int    lock_mask{0};
        u_int    regmap{0};
        u_int    channels{24};
        u_int    block_size{1024};
        int      fromHostDataFormat{0};
        int      toHostDataFormat{0};
        bool     wide_mode{0};
        int      toflx_dmaid{-1};
        int      max_tlp_bytes{256};
        uint32_t status_leds{0};
        bool     fifo_as_file{false};
        std::string file{""};
        std::map<int, std::vector<uint16_t>> elinks;
        std::map<int, std::vector<uint16_t>> elinks_with_streams;
};


class ConfigToHost : public virtual Config {
    public:
        ConfigToHost();
        virtual std::ostream& format(std::ostream& os) const override;
        friend std::ostream& operator<<(std::ostream& os, const ConfigToHost& c);

    protected:
        virtual std::string usage() override;
        virtual std::string options() override;
        virtual void handle_cmd_line(std::map<std::string, docopt::value> args) override;

    public:
        unsigned int daq_threads{1};
        int dcs_rate_limit{-1};
        int l1id_check{-1};
        uint32_t max_dcs_chunk_size{UINT32_MAX};
        uint32_t max_daq_chunk_size{UINT32_MAX};
        int poll_time{10000};
};


class ConfigToFlx : public virtual Config {
    public:
        ConfigToFlx();
        virtual std::ostream& format(std::ostream& os) const override;
        friend std::ostream& operator<<(std::ostream& os, const ConfigToFlx& c);

    protected:
        virtual std::string usage() override;
        virtual std::string options() override;
        virtual void handle_cmd_line(std::map<std::string, docopt::value> args) override;

    public:
        bool unbuffered{false};
        bool trickle{false};
};


class ConfigFileToHost : public ConfigToHost, public ConfigFile {
    public:
        ConfigFileToHost();
        virtual ~ConfigFileToHost();
        virtual std::ostream& format(std::ostream& os) const override;
        friend std::ostream& operator<<(std::ostream& os, const ConfigFileToHost& c);

    protected:
        virtual std::string usage() override;
        virtual std::string options() override;
        virtual void handle_cmd_line(std::map<std::string, docopt::value> args) override;

    public:
        // alphabetical
        unsigned block_rate{0};
        bool full_mode{false};
        bool repeat{true};
};


class ConfigToFlxToFile : public ConfigToFlx, public ConfigFile {
    public:
        ConfigToFlxToFile();
        virtual ~ConfigToFlxToFile();
        virtual std::ostream& format(std::ostream& os) const override;
        friend std::ostream& operator<<(std::ostream& os, const ConfigToFlxToFile& c);

    protected:
        virtual std::string usage() override;
        virtual std::string options() override;
        virtual void handle_cmd_line(std::map<std::string, docopt::value> args) override;
};

#endif /* FELIX_STAR_CONFIG_H_ */
