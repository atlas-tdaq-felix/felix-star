#include "config.hpp"


ConfigFileToHost::ConfigFileToHost() : ConfigToHost(), ConfigFile() {
    resource.toflx = false;
    use_file = true;
}

ConfigFileToHost::~ConfigFileToHost() {
}

std::ostream& ConfigFileToHost::format(std::ostream& os) const {
    os << "ConfigFileToHost" << std::endl;
    os << "block_rate: " << block_rate << std::endl;
    os << "repeat: " << repeat << std::endl;
    os << std::endl;
    return ConfigToHost::format(ConfigFile::format(os));
}

std::ostream& operator<<(std::ostream& os, const ConfigFileToHost& c) {
    return c.format(os);
}

std::string ConfigFileToHost::usage() {
    return
R"(felix-file2host - FELIX central data acquisition application

    Usage:
      felix-file2host [options --device=<device>... --cid=<cid>... --dma=<id>... --elink=DMAID:LID... --stream=DMAID:LID...] FILE
)";
}

std::string ConfigFileToHost::options() {
    return ConfigToHost::options() + ConfigFile::options() +
R"(
    FileToHost Options:
        --block-rate=RATE           Use to define the reading rate. [default: 0] (max.speed)
        --data-format=FMT           ToHost data format, 0(1) for subchunk trailer(headers). [default: 0]
        --no-repeat                 Do not repeat reading the file
    -u, --stream=ELINK ...          Enable elinks with streams, syntax DMAid:elink e.g. 0:0x08.
)" ;
}

void ConfigFileToHost::handle_cmd_line(std::map<std::string, docopt::value> args) {
    ConfigToHost::handle_cmd_line(args);
    ConfigFile::handle_cmd_line(args);

    block_rate = args["--block-rate"].asLong();
    toHostDataFormat = args["--data-format"].asLong();
    repeat = !args["--no-repeat"].asBool();

    for(auto entry : args["--stream"].asStringList() ){
        std::string::size_type n = entry.find(":");
        int dmaid = stoi(entry.substr(0, n));
        uint16_t elink = std::stoul(entry.substr(n+1), nullptr, 16);
        elinks_with_streams[dmaid].push_back(elink);
    }
}
