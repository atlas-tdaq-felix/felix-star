#include "config.hpp"
#include <cstdint>


ConfigToHost::ConfigToHost() : Config() {
    use_file = false;
    resource.toflx = false;
}

//ConfigToHost::~ConfigToHost() {}

std::ostream& ConfigToHost::format(std::ostream& os) const {
    os << "ConfigToHost" << std::endl;
    os << "cmem_buffersize: " << resource.cmem_buffersize << std::endl;
    os << "netio_watermark: " << network.netio_watermark << std::endl;
    os << "daq_unbuffered: " << network.daq_unbuffered << std::endl;
    os << "dcs_ip: " << network.dcs_ip << std::endl;
    os << "dcs_tcp: " << network.dcs_tcp << std::endl;
    os << "dcs_netio_pages: " << network.dcs_netio_pages << std::endl;
    os << "dcs_netio_pagesize: " << network.dcs_netio_pagesize << std::endl;
    os << "max_dcs_chunk_size: " << max_dcs_chunk_size << std::endl;
    os << "dcs_netio_timeout: " << network.dcs_netio_timeout << std::endl;
    os << "dcs_unbuffered: " << network.dcs_unbuffered << std::endl;
    os << "dcs_netio_watermark: " << network.dcs_netio_watermark << std::endl;
    os << "dmaid: ";
        std::copy(resource.dma_ids.begin(), resource.dma_ids.end(), std::ostream_iterator<int>(os, " "));
        os << std::endl;
    os << "l1id_check: " << l1id_check << std::endl;
    os << "max_daq_chunk_size: " << max_daq_chunk_size << std::endl;
    os << "poll_time (us): "<< poll_time << std::endl;
    os << "ttc_netio_pages: " <<  network.netio_pages << std::endl;
    os << "ttc_netio_pagesize: "  <<  network.ttc_netio_pagesize << std::endl;
    os << "ttc_netio_timeout: "   <<  network.ttc_netio_timeout << std::endl;
    os << "ttc_netio_watermark: " <<  network.ttc_netio_watermark << std::endl;
    os << std::endl;
    return Config::format(os);
}

std::ostream& operator<<(std::ostream& os, const ConfigToHost& c) {
    return c.format(os);
}

std::string ConfigToHost::usage() {
    return
R"(felix-tohost - FELIX central data acquisition application

    Usage:
      felix-tohost [options --device=<device>... --cid=<cid>... --dma=<id>...]
)";
}

std::string ConfigToHost::options() {
    return Config::options() +
R"(
    ToHost Options:
    -D, --dma=<id>                  Use DMA descriptor ID. [default: 0]
    -c, --cmem=SIZE                 CMEM buffer size in MB. [default: 512]
        --daq-unbuffered            DAQ unbuffered mode: zero-copy readout (max O(100) kHz rate per link, useful for >O(kB) messages)
    -T, --netio-timeout=SIZE        NetIO timeout in ms for DAQ traffic. [default: 2]
    -w, --netio-watermark=SIZE      NetIO watermark in Byte for DAQ traffic. [default: 57344]
        --dcs-iface=IFACE           Use this network interface for DCS traffic. [calculated: use libfabric on main iface]
        --dcs-pages=SIZE            DCS Number of NetIO pages. [default: 64]
        --dcs-pagesize=SIZE         DCS NetIO page size in Byte. [default: 1024]
    -s, --dcs-port=PORT             Publish DCS data on port PORT. [calculated: 53500 + 10*device + dma]
        --dcs-size-limit=LIMIT      Truncate DCS messages when size is above LIMIT in bytes [default: 65536]
        --dcs-timeout=SIZE          DCS NetIO timeout in ms. [default: 1]
        --dcs-unbuffered            DCS unbuffered mode.
        --dcs-watermark=SIZE        DCS NetIO watermark in Byte. [default: 972]
    -l, --l1id-check=FORMAT         Check L1ID sequence. Formats: 1=TTC2H only, 2=LATOME, 3=FMEMU, 4=FELIG, 5=NSWVMM, 6=NSWTP.
                                    Slow for unbuffered DAQ. [default: 0]
    -M, --max-chunk-size=LIMIT      Maximum chunk size in Bytes for DAQ traffic. Larger chunks are truncated. [calculated from network page size]
    -p, --port=PORT                 Publish DAQ data on port PORT. [calculated: 53100 + 10*device + dma]
    -P, --poll-period=us            Polling instead of interrupt-driven readout with the given poll period in microseconds [default: 1000]
        --threads=THREADS           Number of threads per DMA to read DAQ links [default: 1]
        --ttc-netio-pages=SIZE      TTC Number of NetIO pages. [default: 64]
        --ttc-netio-pagesize=SIZE   TTC NetIO page size in Byte. [default: 1536]
        --ttc-netio-timeout=SIZE    TTC NetIO timeout in ms. Not necessary as TTC2H buffers are flushed at end-of-block. [default: 0]
        --ttc-netio-watermark=SIZE  TTC NetIO watermark in Byte. [default: 1248]
    -t, --ttc-port=PORT              Publish TTC2H data on port PORT. [calculated: 53300 + 10*device + dma]
)";
}

void ConfigToHost::handle_cmd_line(std::map<std::string, docopt::value> args)
{ 
    Config::handle_cmd_line(args);

    if (not args["--dma"].asStringList().empty()){
        for(const auto& i : args["--dma"].asStringList()){
            resource.dma_ids.push_back(stoi(i));
        }
    } else {
        resource.dma_ids = {0};
    }

    resource.cmem_buffersize = args["--cmem"].asLong() * 1024*1024;

    daq_threads = args["--threads"].asLong();
    
    int base_daq_port = args["--port"] ? args["--port"].asLong() : PORT_TOHOST_OFFSET;
    int base_dcs_port = args["--dcs-port"] ? args["--dcs-port"].asLong() : PORT_DCS_OFFSET;
    int base_ttc_port = args["--ttc-port"] ? args["--ttc-port"].asLong() : PORT_TTC2HOST_OFFSET;

    for(auto dev : resource.device){
        for ( auto dmaid : resource.dma_ids){
            int unique_id = get_unique_dmaid(dmaid, dev);
            network.ports.emplace(unique_id, PORT(base_daq_port, dev, dmaid));
            network.dcs_ports.emplace(unique_id, PORT(base_dcs_port, dev, dmaid));
            network.ttc_ports.emplace(unique_id, PORT(base_ttc_port, dev, dmaid));
        }
    }

    // derive dcs-ip
    if (args["--dcs-iface"]) {
        network.dcs_ip = get_value_from_getifaddrs(args["--dcs-iface"].asString(), true);
        network.dcs_tcp = true;
    } else {
        network.dcs_ip = network.ip;
        network.dcs_tcp = false;
    }

    network.daq_unbuffered = args["--daq-unbuffered"].asBool();
    network.netio_timeout = args["--netio-timeout"].asLong();
    network.netio_watermark = args["--netio-watermark"].asLong();

    network.dcs_unbuffered = args["--dcs-unbuffered"].asBool();
    network.dcs_netio_pages = args["--dcs-pages"].asLong();
    network.dcs_netio_pagesize = args["--dcs-pagesize"].asLong();
    network.dcs_netio_timeout = args["--dcs-timeout"].asLong();
    network.dcs_netio_watermark = args["--dcs-watermark"].asLong();

    network.ttc_netio_pages = args["--ttc-netio-pages"].asLong();
    network.ttc_netio_pagesize = args["--ttc-netio-pagesize"].asLong();
    network.ttc_netio_timeout = args["--ttc-netio-timeout"].asLong();
    network.ttc_netio_watermark = args["--ttc-netio-watermark"].asLong();

    poll_time = args["--poll-period"].asLong();
    l1id_check = args["--l1id-check"].asLong();
    max_dcs_chunk_size = args["--dcs-size-limit"].asLong();
    max_daq_chunk_size = args["--max-chunk-size"] ? args["--max-chunk-size"].asLong() : UINT32_MAX;
}
