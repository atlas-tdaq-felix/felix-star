#include "config.hpp"


ConfigToFlx::ConfigToFlx() : Config()
{
    resource.toflx = true;
}


//ConfigToFlx::~ConfigToFlx() {}


std::ostream& ConfigToFlx::format(std::ostream& os) const
{
    os << "ConfigToFlx" << std::endl;
    os << "cmem_buffersize: " << resource.cmem_buffersize << std::endl;
    os << "unbuffered: " << unbuffered << std::endl;
    os << std::endl;
    return Config::format(os);
}


std::ostream& operator<<(std::ostream& os, const ConfigToFlx& c) {
    return c.format(os);
}


std::string ConfigToFlx::usage()
{
    return
R"(felix-toflx - FELIX central data acquisition application

    Usage:
      felix-toflx [options --device=<device>... --cid=<cid>...]
)";
}


std::string ConfigToFlx::options()
{
    return Config::options() +
R"(
    ToFlx Options:
    -c, --cmem=SIZE                 CMEM buffer size in MB. [default: 20]
    -p, --port=PORT                 Send data to port PORT. [calculated: 53200 + 10*device + dma]
    -u, --unbuffered                Use unbuffered mode
    -t, --trickle                   Use trickle mode
)";
}


void ConfigToFlx::handle_cmd_line(std::map<std::string, docopt::value> args)
{
    Config::handle_cmd_line(args);
    resource.cmem_buffersize = args["--cmem"].asLong() * 1024 * 1024;

    int base_port = args["--port"] ? args["--port"].asLong() : PORT_TOFLX_OFFSET;
    for(auto dev : resource.device){
        network.ports.emplace(dev, PORT(base_port, dev, 0));
    }    

    unbuffered = args["--unbuffered"].asBool();
    trickle = args["--trickle"].asBool();
}
