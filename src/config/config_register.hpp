#ifndef FELIX_REGISTER_CONFIG_H_
#define FELIX_REGISTER_CONFIG_H_

#include <cstdint>
#include <vector>
#include <string>
#include <map>

#include "config.hpp"
#include "docopt/docopt.h"

struct RegisterDeviceConfig
{
    int dev_no{0};
    int did{0};
    int cid{0};
    int port_cmd{-1};
    int port_reply{-1};
    uint64_t fid_cmd{0};
    uint64_t fid_reply{0};
};

/**
 * Configuration data structure used by felix-register.
 */
class ConfigRegister
{
    public:
        std::string  local_ip{"127.0.0.1"};
        std::string  bus_dir{"./bus"};
        std::string  bus_group{"FELIX"};
        bool verbose{false};
        bool verbose_bus{false};
        bool enable_cmd{true};
        bool enable_reply{true};
        bool enable_mon{true};
        int port_mon{-1};
        uint64_t fid_mon{0};
        unsigned int mon_interval{5};
        std::string extra_mon{""};
        std::vector<RegisterDeviceConfig> dconfs;
        EventLoopType evloop_type{EventLoopType::netio_next};

        void parse(int argc, char **argv);
            
    private:
        std::string usage();
        std::string options();
        std::string bugs();
};


#endif /* FELIX_REGISTER_CONFIG_H_ */