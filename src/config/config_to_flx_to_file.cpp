#include "config.hpp"


ConfigToFlxToFile::ConfigToFlxToFile() : ConfigToFlx(), ConfigFile()
{
    resource.use_file = true;
    resource.toflx = true;
}

ConfigToFlxToFile::~ConfigToFlxToFile() { }


std::ostream& ConfigToFlxToFile::format(std::ostream& os) const {
    os << "ConfigToFlxToFile" << std::endl;
    os << std::endl;
    return ConfigToFlx::format(ConfigFile::format(os));
}


std::ostream& operator<<(std::ostream& os, const ConfigToFlxToFile& c) {
    return c.format(os);
}


std::string ConfigToFlxToFile::usage() {
    return
R"(felix-toflx2file - FELIX central data acquisition application

    Usage:
      felix-toflx2file [options --device=<device>... --cid=<cid>... --elink=DMAID:LID...] FILE
)";
}


std::string ConfigToFlxToFile::options() {
    return ConfigToFlx::options() + ConfigFile::options();
}


void ConfigToFlxToFile::handle_cmd_line(std::map<std::string, docopt::value> args) {
    ConfigToFlx::handle_cmd_line(args);
    ConfigFile::handle_cmd_line(args);

}
