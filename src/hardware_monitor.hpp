#ifndef HW_MON_FRONTEND_H_
#define HW_MON_FRONTEND_H_

#include <cstdint>
#include <memory>

#include "hardware_monitor_backend.hpp"
#include "bus.hpp"
#include "publisher.hpp"
#include "network/netio_buffered_publisher.hpp"
#include "network/utility.hpp"
#include "config/config_register.hpp"

using namespace std::chrono_literals;

/**
 * HwMonFrontend published over the network hardware monitoring information
 * for all requested devices using a single publisher.
 * HwMonFrontend shall run on a dedicated thread as the retrieval of info
 * can be blocking due to the regulation of concurrent access to the I2C bus.
 */
template <class DEV>
class HwMonitor 
{
    public:
        using device_pair = std::pair<std::shared_ptr<DEV>, std::shared_ptr<DEV>>;

        /**
         * @brief open the device.
         * @param device_pairs pair of primary and secondary device of a card.
         * @param c configuration data structure.
         */
        HwMonitor(const std::vector<device_pair> & device_pairs, const ConfigRegister & c);

        /**
         * @brief publish monitoring information message over the network
         */
        bool publish_monitor_info();

    private:
        HwMonBackend<DEV> m_backend;
        Bus m_bus;
        uint64_t m_fid;
        std::unique_ptr<Publisher> m_publisher;
};


template <class DEV>
HwMonitor<DEV>::HwMonitor(const std::vector<HwMonitor<DEV>::device_pair> & device_pairs, const ConfigRegister &c)
    : m_backend(device_pairs, c.extra_mon),
      m_bus{c.bus_dir, c.bus_group, "register-mon-"+std::to_string(c.dconfs.at(0).dev_no), c.verbose_bus},
      m_fid(c.fid_mon)
{
    constexpr int net_pages{16};
    constexpr int net_page_size{4096};
    constexpr int net_page_timeout{0};
    auto fid_mon = Elink(c.fid_mon);

    unsigned int mon_interval_us = 1e6*c.mon_interval;
    LOG_INFO("Hardware monitoring poll period %u s.", c.mon_interval);
    m_publisher = network::utility::create_buffered_publisher(c.evloop_type, c.local_ip, c.port_mon, m_bus,
        net_pages, net_page_size, net_page_size, net_page_timeout, UINT32_MAX);
    m_publisher->declare({fid_mon});
    m_publisher->set_periodic_callback(mon_interval_us, [this]{return publish_monitor_info();});
}


template <class DEV>
bool HwMonitor<DEV>::publish_monitor_info()
{
    std::string msg = m_backend.get_monitor_data();   
    auto msg_ptr = reinterpret_cast<uint8_t*>(msg.data());
    iovec net_msg[2];
    uint8_t status = 0;
    net_msg[0].iov_len = 1;
    net_msg[0].iov_base = &status;
    net_msg[1].iov_len = msg.length();
    net_msg[1].iov_base = msg_ptr;
    //The publish function that takes a pointer to the data message
    //assumes that the first byte is the status byte.
    m_publisher->publish(m_fid, net_msg, 2, 0, 0, 0);
    m_publisher->flush(m_fid);
    //This function must return false otherwise the callback is invoked again.
    //TODO: the publisher set_periodic_callback is designed for felix-tohost.
    return false;  
}


#endif /* HW_MON_FRONTEND_H_ */