#ifndef REGISTER_CMD_PARSER_H_
#define REGISTER_CMD_PARSER_H_

#include <cstdint>
#include <string>
#include <vector>

#include "simdjson.h"
#include "felix/felix_client_thread.hpp"

/**
 * Data structure to store a command.
 */
struct Command {
    std::string uuid;
    FelixClientThread::Cmd cmd;
    std::vector<std::string> cmd_args;
};


/**
 * Data structure used by felix-register to parse the command, execute
 * the operation and prepare the reply.
 */
struct ReqData {
    int status_code{FelixClientThread::OK};
    std::string status_message{"Ok"};
    std::string uuid{""};
    FelixClientThread::Cmd cmd{FelixClientThread::Cmd::UNKNOWN};
    std::string resource_name{""};
    std::string value{""};
};

/**
 * RegisterMsgParser de-serialises json-formatted commands (json string to struct)
 * and serialises replies (struct to json string)
 */
class RegisterMsgParser
{
    public:
        /** 
         * @brief parses a char array containing a command in json format
         * @param msg pointer to message C-string
         * @param len length of C-string message
         * @return a ReqData struct containing the command.
         */
        std::vector<ReqData> parse_commands(const char* msg, size_t len);

        /** 
         * @brief parses a string containing a command in json format
         * @param cmd the command string
         * @return a ReqData struct containing the command.
         */
        std::vector<ReqData> parse_commands(const std::string& cmd);

        /** 
         * @brief encodes a reply in json
         * @param fid the reply fid
         * @param cmds vector of ReqData structures containing the command execution data
         * @return an std::string containin the reply in serialised json.
         */
        std::string encode_replies(uint64_t fid, const std::vector<ReqData>& cmds);

        /** 
         * @brief decode felix-register replies
         * @param r the string containing the reply array in json format
         * @return a vector of FelixClientThread::Reply containing the decoded replies.
         */
        std::vector<FelixClientThread::Reply> decode_replies(const std::string & r);

        /** 
         * @brief encodes a vector of commands into a json array
         * @param fid the reply fid
         * @param cmds vector of Command structures cdescribing commands
         * @return an std::string containin the command json array
         */
        std::string encode_commands(const std::vector<Command>& cmds);

    private:
        simdjson::dom::parser parser;      
        void parse_cmd_args(ReqData& msg, simdjson::dom::array& arr);
};

#endif /* REGISTER_CMD_PARSER_H_ */