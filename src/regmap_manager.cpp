#include "regmap_manager.hpp"
#include "log.hpp"
#include "util.hpp"

#include <vector>
#include <algorithm>

RegmapManager::RegmapManager()
{
    #if REGMAP_VERSION < 0x0500
        std::string regmap_file = "regmap4-symbol.yaml";
    #else
        std::string regmap_file = "regmap5-symbol.yaml";
    #endif

    // get felix path, remove bin if needed
    std::filesystem::path executable_path = std::filesystem::canonical("/proc/self/exe").parent_path();
    std::filesystem::path felix_path = executable_path.filename() == "bin" ? executable_path.parent_path() : executable_path;
    std::filesystem::path regmap_path = felix_path / "share" / "felix-star" / regmap_file;
    LOG_INFO("Reading register map %s", regmap_path.u8string().c_str());

    m_regmap = YAML::LoadFile(regmap_path);
}


bool RegmapManager::has_key(std::string name, std::string key, const std::string& value) {
    if (!m_regmap["registers"][name]) return false;
    std::vector<std::string> values = Util::split(m_regmap["registers"][name][key].Scalar(), '|');
    return std::find(values.begin(), values.end(), value) != values.end();
}


bool RegmapManager::has_type(const std::string& name, const std::string& type) {
    return has_key(name, "type", type);
}


bool RegmapManager::has_endpoint(const std::string& name, const std::string& endpoint) {
    return has_key(name, "endpoints", endpoint);
}
