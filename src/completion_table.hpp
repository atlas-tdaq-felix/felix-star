#ifndef FELIX_COMPLETIONTABLE_H_
#define FELIX_COMPLETIONTABLE_H_

#include <map>
#include <cstdint>

/**
 * @brief The completion table is the class that allows to advance the DMA
 * buffer read pointer as completion objects from zero-copy send operations are
 * returned by the network.
 * 
 * @details This class is used by felix-tohost in zero-copy sending mode:
 * in this case the read pointer can be advanced only when the network send
 * operation is complete (and not before, to prevent firmware overwriting the
 * outstanding data.
 * For each chunk to be sent the address of address of the first block it
 * belongs to is pushed to the table. If the address already exists the
 * corresponding counter is increased by one. Each completion object reporting
 * the success of the send operation contains the block address.
 * When the completion object is read the correspoding address counter is
 * decreased by one. When the counter goes to zero the address entry is removed.
 * The read pointer returned by the compeltion table points to the first entry
 * whose counter is not zero. 
 * The implementations uses two std::maps to deal with the wrap-around of
 * DMA buffer addresses.
 */
class CompletionTable
{
    public:
        CompletionTable();

        /**
         * @brief Push a new message address to the completion table.
         * @param addr address in DMA buffer of the outstanding network message.
         */ 
        void push(uint32_t addr);

        /**
         * @brief Update the completion table as a send completion is processed.
         * @param addr address in DMA buffer of the outstanding network message.
         * @return error codes defined within the class
         */ 
        int update(uint32_t addr);

        /**
         * @return the DMA read pointer according to the completion table.
         */ 
        uint32_t get_rd();

        /**
         * @return the counter corresponding to the given address.
         */ 
        uint32_t get_count(uint32_t addr);

        /**
         * @return the DMA read pointer returned in the last call of get_rd()
         */         
        uint32_t get_previous_rd();

        /**
         * @return the number of address
         */   
        size_t get_entries();

        /**
         * @brief dump the content of the table
         */   
        void inspect();

    private:
        typedef uint32_t address_t;
        typedef uint32_t counter_t;

        address_t m_rd;
        address_t m_last_pushed_addr;
        address_t m_last_provided_addr;
        std::map<address_t, counter_t> *m_current_table;
        std::map<address_t, counter_t> *m_wrap_table;
        std::map<address_t, counter_t>  m_table_a;
        std::map<address_t, counter_t>  m_table_b;
        //std::mutex m_mtx;
};

#endif /* FELIX_COMPLETIONTABLE_H_ */