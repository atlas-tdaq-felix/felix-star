#ifndef REGISTER_DEVICE_MANAGER_H_
#define REGISTER_DEVICE_MANAGER_H_

#include <memory>
#include <stdint.h>
#include <string>

#include "log.hpp"
#include "device.hpp"
#include "regmap_manager.hpp"

/**
 * Felix-register interface to a PCIe endpoint.
 * */
template <class DEV>
class RegisterDeviceInterface
{
public:

    /**
     * @brief RegisterDeviceManager class constructor
     * @param regmap the owner of the the yaml register map.
     * @param main the PCIe endpoint for this RegisterDeviceManager.
     * @param primary the primary PCIe endpoint correspoding to "main".
     */
    RegisterDeviceInterface(RegmapManager & regmap, std::shared_ptr<DEV> main, std::shared_ptr<DEV> primary)
        : m_regmap(regmap), m_main(main), m_primary(primary){}

    /**
     * @brief read a register.
     * @param reg_name register name.
     * @return the register value
     */
    uint64_t get_register(const std::string& reg_name);

    /**
     * @brief set a register.
     * @param reg_name register name.
     * @param values the value to be set.
     */
    void set_register(const std::string& reg_name, uint64_t value);

    /**
     * @return whether the register is readable
     */
    bool can_read(const std::string& reg_name){ return m_regmap.can_read(reg_name); };

    /**
     * @return whether the register is writable
     */
    bool can_write(const std::string& reg_name){ return m_regmap.can_write(reg_name); };

private:
    RegmapManager & m_regmap;
    std::shared_ptr<DEV> m_main;
    std::shared_ptr<DEV> m_primary;

    /**
     * @param reg_name the register name.
     * @return the PCIe endpoint to which the register is associated to.
     */
    DEV* select_device(const std::string& reg_name);
};


template <class DEV>
DEV* RegisterDeviceInterface<DEV>::select_device(const std::string& reg_name)
{
    DEV* device = nullptr;
    unsigned dev_no = m_main->get_device_number();

    if (not m_regmap.has_endpoint_1(reg_name) and not m_main->is_primary())
    {
        if (!m_primary.get()){
            LOG_ERR("Cannot access register %s. Primary endpoint for device %u not initiliazed", dev_no, reg_name.c_str());
        }
        else if (!m_primary->is_primary()) {
            unsigned primary_dev_no = m_primary->get_device_number();
            LOG_ERR("Cannot access register %s. Neither devices %u and %u are primary ", reg_name.c_str(), dev_no, primary_dev_no);
        } else {
            device = m_primary.get();
        }
    } else {
        device = m_main.get();
        if (!device){
            LOG_ERR("Cannot access register %s. Device %u not initialized", dev_no, reg_name.c_str());
        }
    }
    return device;
}


template <class DEV>
uint64_t RegisterDeviceInterface<DEV>::get_register(const std::string& reg_name)
{
    DEV* device = select_device(reg_name);
    if (device) {
        return device->get_register(reg_name.c_str());
    }
    return 0;
}


template <class DEV>
void RegisterDeviceInterface<DEV>::set_register(const std::string& reg_name, uint64_t value)
{
    DEV* device = select_device(reg_name);
    if (device) {
        device->set_register(reg_name.c_str(), value);
    }
}

#endif /* REGISTER_DEVICE_MANAGER_H_ */