#include "device.hpp"
#include "config/config_register.hpp"
#include "register.hpp"

int main(int argc, char** argv)
{
    ConfigRegister c;
    c.parse(argc, argv);
    auto engine = Register<FlxDevice>(c);
    return 0;
}
