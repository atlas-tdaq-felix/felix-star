#include "fromhost.hpp"
#include "log.hpp"

static volatile std::sig_atomic_t signal_status;
void signal_handler(int s){signal_status = s;}

int main(int argc, char** argv)
{
    std::unique_ptr<ConfigToFlxToFile> c = std::make_unique<ConfigToFlxToFile>();
    c->parse(argc, argv);

    auto fh = FromHost<ConfigToFlxToFile, FileDevice, FileFromHostBuffer>(std::move(c));
    for (auto & dev_no : fh.cfg->resource.device){
        LOG_INFO("DEVICE %d", dev_no);
        fh.devices.emplace_back(new FileDevice(*fh.cfg, dev_no));
        fh.dma_buffers.emplace(dev_no,
            new FileFromHostBuffer(fh.devices.back(), fh.cfg->file, fh.cfg->fifo_as_file));
    }
    fh.start();

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (signal_status == 0) {
        fh.print_monitoring();
        usleep(fh.cfg->stats.monitoring_period_ms * 1000);
    }
    LOG_INFO("Received signal %d, exiting...", signal_status);
    fh.stop();
    return 0;
}
