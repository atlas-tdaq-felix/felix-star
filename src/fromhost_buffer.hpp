#ifndef FROMHOST_BUFFER_H_
#define FROMHOST_BUFFER_H_

#include <cstddef>
#include <cstdint>
#include <stdio.h>
#include <thread>
#include <mutex>
#include <atomic>
#include <shared_mutex>
#include <condition_variable>
#include "log.hpp"

#include "device.hpp"
#include "encoder.hpp"
#include "cmem_buffer.hpp"
#include "disk_io.hpp"
#include "fromhost_monitor.hpp"

/**
 * Abstract class representing a FromHost DMA buffer.
 * The elinks enabled for the corresponding DMA buffers are read from the
 * device in the constructors.
 * */
class FromHostBuffer
{
    public:

        /**
         * @brief FromHostBuffer contructor.
         * @param d Device shared pointer.
         */
        FromHostBuffer(std::shared_ptr<Device> d) :  
            m_device(d), m_run_flag(true), m_size(0), m_number_of_writers(0),
            m_encoder(), m_mon(), m_mon_prev(), m_trickle_config_size(0)
        {};

        /**
         * @return pointer to device in use.
         */
        std::shared_ptr<Device> get_device(){return m_device;}

        /**
         * @return size of DMA buffer.
         */
        [[nodiscard]] size_t get_size() const{return m_size;}


        /**
         *  @return size of the trickle configuration message.
         */
        size_t get_trickle_config_size(){return m_trickle_config_size;}

        /**
         * @param size : size of trickle configuration message
         */
        void set_trickle_config_size(size_t size){m_trickle_config_size = size;}
        
        /**
         * @param format : FromHost data format supported by firmware.
         */
        void set_encoder_data_format(int format){m_encoder.set_data_format(format);}

        /**
         * @return identifier of DMA buffer.
         */
        [[nodiscard]] int get_dmaid() const{return m_dmaid;}

        /**
         * @param dma_id identifier of DMA buffer.
         * @return null.
         */
        void set_dmaid(int dma_id) 
        {
            if(m_dmaid == -1) m_dmaid = dma_id;
            //else throw std::runtime_error("DMA buffer already initialized with id: " + std::to_string(m_dmaid));
        }


        /**
         * @return stop DMA transfer operations.
         */
        void stop(){m_run_flag = false;}

        /**
         * @param m_dmaid DMA identifier
         * @return vector of enabled e-links.
         */
        std::vector<Elink> get_elinks(){return m_device->read_enabled_elinks(m_dmaid);}

        /**
         * @param m_dmaid DMA identifier
         * @return vector of enabled e-links of a given type (DAQ, DCS...)
         */
        std::vector<Elink> get_elinks_of_type(elink_type_t t){return m_device->get_enabled_elinks_of_type(m_dmaid, t);};

        /**
         * @brief increment the counter of writers concurrently acessing the DMA buffer
         */
        void increment_writer_counter (){++m_number_of_writers;}

        /**
         * @return whether the FromHost DMA buffer is in use by more than a writer.
         */
        [[nodiscard]] bool has_multiple_writers() const{return m_number_of_writers > 1;}

        /**
         * @param size of message to be encoded and written.
         * @return size of the encoded messagesss provided by encoder.
         */
        size_t compute_msg_dma_occupancy(size_t size){return m_encoder.compute_max_msg_occupancy(size);};

        /**
         * @brief encode and write message in the FromHost buffer.
         * @param elink of message to be encoded and written.
         * @param source of message to be encoded and written.
         * @param size of message to be encoded and written.
         */ 
        void encode_and_write(uint64_t elink, const uint8_t *source, size_t size, bool trickle = false);

        /**
         * @return free space in the FromHost buffer in MB.
         */
        uint32_t dma_get_free_MB() {return dma_free_bytes()/1024/1024;}

        /**
         * @return monitoring information with rates.
         */
        FromHostDmaStats get_monitoring_data();

        /**
         * @return free space in the FromHost buffer in bytes.
         */
        virtual size_t   dma_free_bytes()                            = 0;

        /**
         * @param size of the FromHost DMA buffer.
         * @param name of the buffer passed to the CMEM driver.
         * @param vmem allocate the buffer without using the CMEM driver.
         * @param free_previous_cmem claim an unlocked/orphaned CMEM buffer with the same name.
         */
        virtual void allocate_buffer(size_t size,
                            const std::string& name,
                            bool vmem, bool free_previous_cmem=true) = 0;

        std::mutex m_buffer_mutex; //allow FromHostWriters to lock this

    protected:
        virtual void set_oneshot_trickle_buffer(size_t config_size) = 0;
        virtual void     dma_start_continuous()                     = 0;
        virtual void     dma_start_circular_trickle_buffer()        = 0;
        virtual bool     dma_is_full()                              = 0;
        virtual uint64_t dma_get_read_offset()                      = 0;
        virtual uint64_t dma_get_write_ptr()                        = 0;
        virtual uint64_t dma_get_write_offset()                     = 0;
        virtual void     dma_set_write_offset(uint64_t offset)      = 0;
        virtual void     dma_advance_write_ptr(size_t bytes)        = 0;

    protected:
        int m_dmaid = -1; // -1 is just to check if the buffer is not initialized
        std::shared_ptr<Device> m_device;
        std::atomic<bool> m_run_flag{true};
        size_t m_size;
        int m_number_of_writers;
        Encoder m_encoder;
        FromHostDmaStats m_mon;
        FromHostDmaStats m_mon_prev; //to compute rates
        size_t m_trickle_config_size;
        std::unique_ptr<DmaBuffer> m_buffer;

        size_t dma_compute_free_bytes(uint64_t fw_rd_ptr, uint64_t pc_wr_ptr, bool even);
};


inline size_t FromHostBuffer::dma_compute_free_bytes(uint64_t fw_rd_ptr, uint64_t pc_wr_ptr, bool even)
{
    size_t value{0};
    if(pc_wr_ptr < fw_rd_ptr ) {
        value = fw_rd_ptr - pc_wr_ptr;
    }
    else if( pc_wr_ptr > fw_rd_ptr ) {
        value = m_buffer->size - (pc_wr_ptr - fw_rd_ptr);
    }
    else { // pc_wr_ptr == fw_rd_ptr
        if( even ) {
            value = m_buffer->size; // Buffer empty
        } //else value = 0
    }
    return value;
}


/**
 * FromHost DMA buffer interfaced with an FLX card
 * */
class FlxFromHostBuffer : public FromHostBuffer {

    public:
        explicit FlxFromHostBuffer(std::shared_ptr<Device> d);
        ~FlxFromHostBuffer();

        void allocate_buffer(size_t size,
                            const std::string& name,
                            bool vmem, bool free_previous_cmem=true) override;

        void     dma_start_continuous()                     override;
        void     dma_start_circular_trickle_buffer()        override;
        void set_oneshot_trickle_buffer(size_t config_size) override;
        size_t   dma_free_bytes()                           override;
        bool     dma_is_full()                              override;
        uint64_t dma_get_read_offset()                      override;
        uint64_t dma_get_write_offset()                     override;
        uint64_t dma_get_write_ptr()                        override;
        void     dma_set_write_offset(uint64_t addr)        override;
        void     dma_advance_write_ptr(size_t bytes)        override;
};


/**
 * FromHost DMA buffer with FLX card emulation.
 * The card is emulated by a separate thread that can copy the
 * data to a file or a fifo.
 * */
class FileFromHostBuffer : public FromHostBuffer {

    public:
        explicit FileFromHostBuffer(std::shared_ptr<Device> d,
            std::string& filename, bool fifo);
        ~FileFromHostBuffer();

        void allocate_buffer(size_t size,
                            const std::string& name,
                            bool vmem, bool free_previous_cmem=false) override;
                        
        void     dma_start_continuous()                     override;
        void     dma_start_circular_trickle_buffer()        override;
        void set_oneshot_trickle_buffer(size_t config_size) override {m_fw_reading_trickle.store(false);};
        bool     dma_is_full()                              override;
        uint64_t dma_get_read_offset()                      override;
        size_t   dma_free_bytes()                           override;
        uint64_t dma_get_write_offset()                     override;
        uint64_t dma_get_write_ptr()                        override;
        void     dma_set_write_offset(uint64_t addr)        override;
        void     dma_advance_write_ptr(size_t bytes)        override;

    private:
        bool     dma_is_empty();
        size_t   dma_bytes_to_consume();
        void     dma_advance_read_ptr(size_t bytes);
        void     dma_set_read_offset(uint64_t addr);
        void     set_write_ptr_paddr(uint64_t p_addr);
        void     copy_from_dma_buffer_to_file();

    private:
        //regulate concurrent access of read and write pointers
        mutable std::shared_mutex m_driver_mutex;

        //suspend the card emulator thread when the DMA is empty
        std::condition_variable m_reader_cond;
        mutable std::mutex m_wake_reader_mutex;

        DiskIO m_file;
        bool m_rd_odd;
        bool m_wr_odd;
        std::atomic<bool> m_fw_reading_trickle {false};
        std::thread m_reader_thread;
};

#endif /* FROMHOST_BUFFER_H_ */
