#ifndef FELIX_UNBUFFERED_RECEIVER_H_
#define FELIX_UNBUFFERED_RECEIVER_H_

#include <thread>

#include "netio/netio.h"
#include "netio_evloop.hpp"
#include "receiver.hpp"
#include "bus.hpp"
#include "felix/felix_toflx.hpp"

class NetioUnbufferedReceiver : public Receiver {

public:
    explicit NetioUnbufferedReceiver(const std::string &ip, uint32_t port, Bus& bus,
        unsigned int netio_pn, unsigned int netio_ps);

    explicit NetioUnbufferedReceiver(const std::string &ip, uint32_t port, Bus& bus,
        unsigned int netio_pn, unsigned int netio_ps, NetioEventLoop& evloop);

    ~NetioUnbufferedReceiver();

    bool declare(const std::vector<Elink> &elinks) override;

    void set_conn_open_callback(OnConnOpen callback) override {m_on_conn_open = callback;};

    void set_conn_close_callback(OnConnClose callback) override {m_on_conn_close = callback;};

    void set_on_msg_callback(OnMsg callback) override {m_on_msg = callback;};

    int get_number_of_connections() override {return -1;} //Unsupported: need entries of netio_socket_list linked list.

private:
    void eventLoop(uint32_t port);
    void init_listen_socket();

    static void cb_on_connection_established(struct netio_recv_socket* socket) {
        std::string ep = std::to_string(socket->eqfd); //Would like to have an IP here
        NetioUnbufferedReceiver* object = static_cast<NetioUnbufferedReceiver*>(socket->lsocket->usr);
        if (object->m_on_conn_open) {
            object->m_on_conn_open(ep);
        }
    }

    static void cb_on_connection_close(struct netio_recv_socket* socket) {
        std::string ep = std::to_string(socket->eqfd); //Would like to have an IP here
        NetioUnbufferedReceiver* object = static_cast<NetioUnbufferedReceiver*>(socket->lsocket->usr);
        if (object->m_on_conn_close) {
            object->m_on_conn_close(ep);
        }
    }

    static void cb_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len) {
        uint8_t* dataptr = static_cast<uint8_t*>(buf->data);
        std::vector<ToFlxMessage> messages{};
        size_t pos = 0;
        while (pos < len) {
            if (len - pos < sizeof(ToFlxHeader)) {
                messages.push_back({.status = ToFlxMessage::Status::HeaderNotDecoded});
                break;
            }

            ToFlxHeader hdr;
            std::memcpy(&hdr, dataptr + pos, sizeof(ToFlxHeader));
            pos += sizeof(ToFlxHeader);

            if (hdr.length == 0) {
                messages.push_back({.status = ToFlxMessage::Status::InvalidMsgLength});
                break;
            }

            if (len - pos < hdr.length) {
                messages.push_back({.status = ToFlxMessage::Status::MessageNotDecoded});
                break;
            }

            messages.emplace_back(static_cast<uint32_t>(hdr.elink), std::span<uint8_t>(dataptr + pos, hdr.length));
            pos += hdr.length;
        }

        static_cast<NetioUnbufferedReceiver*>(socket->lsocket->usr)->m_on_msg(messages);
        netio_post_recv(socket, buf);
    }

private:
    const std::string m_ip;
    const uint32_t m_port;
    Bus& m_bus;
    netio_unbuffered_socket_attr m_socket_attr;
    std::shared_ptr<netio_context> m_context;
    netio_listen_socket m_socket;

    OnConnOpen m_on_conn_open;
    OnConnClose m_on_conn_close;
    OnMsg m_on_msg;
    std::thread m_event_loop_thread;
};

#endif /* FELIX_UNBUFFERED_RECEIVER_H_ */