#include "network/netio_evloop.hpp"
#include "netio/netio.h"

NetioEventLoop::NetioEventLoop()
    : ctx(std::make_shared<netio_context>())
{
    netio_init(ctx.get());
}


void NetioEventLoop::start_thread(const std::string& thread_name)
{
    m_evloop_thread = std::thread([this, thread_name]{ run(thread_name); });
}


void NetioEventLoop::start()
{
    netio_run(&(ctx->evloop));
}


void NetioEventLoop::stop()
{
    netio_terminate_signal(&(ctx->evloop));
    if ( m_evloop_thread.get_id() != std::thread::id() ) {
        m_evloop_thread.join();
    }
}


void NetioEventLoop::run(const std::string& thread_name)
{
    pthread_setname_np(pthread_self(), thread_name.c_str());
    netio_run(&(ctx->evloop));
}