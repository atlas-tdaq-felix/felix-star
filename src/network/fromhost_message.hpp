#ifndef FROMHOST_MESSAGE_HPP
#define FROMHOST_MESSAGE_HPP

#include <cstdint>
#include <span>

/**
 * @brief Represents a message to be sent to the FLX device.
 */
struct ToFlxMessage {

    /**
     * @brief Enum representing the status of a message in ToFLX direction
     */
    enum class Status {
        MessageOk,           ///< Message is OK.
        HeaderNotDecoded,    ///< Header not decoded, not enough data.
        MessageNotDecoded,   ///< Message not decoded, not enough data.
        InvalidMsgLength     ///< Invalid message length.
    };

    uint32_t elink{};                   ///< The elink identifier.
    std::span<const uint8_t> payload{}; ///< The payload and size of the message.
    Status status{ Status::MessageOk }; ///< The status of the message.

    /**
     * @brief Converts the status enum to a string representation.
     * 
     * @param status The status to convert.
     * @return const char* The string representation of the status.
     */
    static const char* statusToString(Status status) {
        switch (status) {
            case Status::MessageOk: return "MessageOk";
            case Status::HeaderNotDecoded: return "Header not decoded, not enough data";
            case Status::MessageNotDecoded: return "Message not decoded, not enough data";
            case Status::InvalidMsgLength: return "Invalid message length";
            default: return "Unknown status in ToFlxMessage";
        }
    }

};

#endif // FROMHOST_MESSAGE_HPP