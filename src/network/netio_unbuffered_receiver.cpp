#include "network/netio_unbuffered_receiver.hpp"
#include "log.hpp"

NetioUnbufferedReceiver::NetioUnbufferedReceiver(const std::string &ip, uint32_t port, Bus& bus,
    unsigned int netio_pn, unsigned int netio_ps)
    : m_ip{ip},
      m_port(port),
      m_bus(bus),
      m_socket_attr{netio_pn, netio_ps},
      m_context(new netio_context())
{
    netio_init(m_context.get());
    init_listen_socket();
    m_event_loop_thread = std::thread([this, port]{ eventLoop(port); });
}


NetioUnbufferedReceiver::NetioUnbufferedReceiver(const std::string &ip, uint32_t port, Bus& bus,
    unsigned int netio_pn, unsigned int netio_ps, NetioEventLoop& evloop)
    : m_ip{ip},
      m_port(port),
      m_bus(bus),
      m_socket_attr{netio_pn, netio_ps},
      m_context(evloop.ctx)
{
    init_listen_socket();
}


void NetioUnbufferedReceiver::init_listen_socket()
{
    memset(&m_socket, 0, sizeof m_socket);
    netio_init_listen_socket(&m_socket, m_context.get(), &m_socket_attr);
    m_socket.usr = this;
    m_socket.cb_connection_established = cb_on_connection_established;
    m_socket.cb_connection_closed = cb_on_connection_close;
    m_socket.cb_msg_received = cb_msg_received;
    netio_listen(&m_socket, netio_hostname(m_ip.c_str()), m_port);
    LOG_DBG("Initialised netio-next listen_socket. Port %u PROVIDER: %s", m_port, m_socket.fi->fabric_attr->prov_name);
}


NetioUnbufferedReceiver::~NetioUnbufferedReceiver()
{
    netio_terminate_signal(&(m_context->evloop));
    if ( m_event_loop_thread.get_id() != std::thread::id() ) {
        m_event_loop_thread.join();
    }
}


bool NetioUnbufferedReceiver::declare(const std::vector<Elink> &elinks)
{
    bool pubsub = false;
    bool unbuffered = true;
    uint32_t n_pages = m_socket_attr.num_buffers;
    uint32_t sz_page = m_socket_attr.buffer_size;
    return m_bus.publish(elinks, m_ip, m_port, n_pages, sz_page, pubsub, unbuffered);
}


void NetioUnbufferedReceiver::eventLoop(uint32_t port)
{
    std::ostringstream out;
    out << "rec[" << port << "]";
    std::string s = out.str();
    pthread_setname_np(pthread_self(), s.c_str());

    netio_run(&(m_context->evloop));
}