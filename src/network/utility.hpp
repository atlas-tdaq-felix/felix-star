#ifndef FELIX_NETWORK_UTILITY_HPP
#define FELIX_NETWORK_UTILITY_HPP

#include <memory>

#include "bus.hpp"
#include "config.hpp"
#include "netio_buffered_publisher.hpp"
#include "netio_buffered_receiver.hpp"
#include "netio_unbuffered_receiver.hpp"
#include "netio_zerocopy_publisher.hpp"
#include "publisher.hpp"
#include "receiver.hpp"

namespace network::utility {
[[nodiscard]] inline std::unique_ptr<Publisher>
create_buffered_publisher(EventLoopType evloop_type, const std::string &ip,
                          uint32_t port, Bus &bus, unsigned int netio_pn,
                          unsigned int netio_ps, unsigned int netio_wm,
                          unsigned int netio_to, std::uint32_t max_chunk_size) {
  switch (evloop_type) {
  case EventLoopType::netio_next:
    return std::make_unique<NetioBufferedPublisher>(
        ip, port, bus, netio_pn, netio_ps, netio_wm, netio_to, max_chunk_size);
  case EventLoopType::netio3_native:
  case EventLoopType::netio3_asio:
    throw std::runtime_error("Netio3 not implemented");
  default:
    throw std::logic_error("Invalid EventLoopType");
  }
}

[[nodiscard]] inline std::unique_ptr<Publisher>
create_zero_copy_publisher(EventLoopType evloop_type, const std::string &ip,
                           uint32_t port, Bus &bus, unsigned int netio_pn,
                           unsigned int netio_ps, std::uint32_t max_chunk_size,
                           uint64_t dma_buffer_vaddr, size_t dma_size) {

  switch (evloop_type) {
  case EventLoopType::netio_next:
    return std::make_unique<NetioZerocopyPublisher>(ip, port, bus, netio_pn,
                                                    netio_ps, max_chunk_size,
                                                    dma_buffer_vaddr, dma_size);
  case EventLoopType::netio3_native:
  case EventLoopType::netio3_asio:
    throw std::runtime_error("Netio3 not implemented");
  default:
    throw std::logic_error("Invalid EventLoopType");
  }
}

[[nodiscard]] inline std::unique_ptr<Receiver>
create_buffered_receiver(EventLoopType evloop_type, const std::string &ip,
                         uint32_t port, Bus &bus, unsigned int netio_pn,
                         unsigned int netio_ps) {

  switch (evloop_type) {
  case EventLoopType::netio_next:
    return std::make_unique<NetioBufferedReceiver>(ip, port, bus, netio_pn,
                                                   netio_ps);
  case EventLoopType::netio3_native:
  case EventLoopType::netio3_asio:
    throw std::runtime_error("Netio3 not implemented");
  default:
    throw std::logic_error("Invalid EventLoopType");
  }
}

[[nodiscard]] inline std::unique_ptr<Receiver>
create_unbuffered_receiver(EventLoopType evloop_type, const std::string &ip,
                           uint32_t port, Bus &bus, unsigned int netio_pn,
                           unsigned int netio_ps) {

  switch (evloop_type) {
  case EventLoopType::netio_next:
    return std::make_unique<NetioUnbufferedReceiver>(ip, port, bus, netio_pn,
                                                     netio_ps);
  case EventLoopType::netio3_native:
  case EventLoopType::netio3_asio:
    throw std::runtime_error("Netio3 not implemented");
  default:
    throw std::logic_error("Invalid EventLoopType");
  }
}
} // namespace network::utility

#endif