#ifndef FELIX_NETIOZEROCOPYPUBLISHER_H_
#define FELIX_NETIOZEROCOPYPUBLISHER_H_

#include <array>
#include <string>
#include <thread>

#include "netio/netio.h"
#include "netio_evloop.hpp"
#include "bus.hpp"
#include "publisher.hpp"
#include "completion_table.hpp"


class NetioZerocopyPublisher : public Publisher
{
public:
    explicit NetioZerocopyPublisher(const std::string &ip, uint32_t port, Bus& bus,
        unsigned int netio_pn, unsigned int netio_ps, unsigned int max_msg_size,
        uint64_t dma_buffer_vaddr, size_t dma_size);

    explicit NetioZerocopyPublisher(const std::string &ip, uint32_t port, Bus& bus,
        unsigned int netio_pn, unsigned int netio_ps, unsigned int max_msg_size,
        uint64_t dma_buffer_vaddr, size_t dma_size, NetioEventLoop& evloop);

    ~NetioZerocopyPublisher();

    NetioZerocopyPublisher(const NetioZerocopyPublisher &) = delete;
    NetioZerocopyPublisher &operator=(const NetioZerocopyPublisher &) = delete;

    bool declare(const std::vector<Elink> &elinks) override;

    Result publish(felix_id_t fid, iovec *iov, uint32_t iovlen, size_t bytes, uint32_t block_addr, std::uint8_t status) override;

    Result publish(felix_id_t fid, uint8_t* data, size_t len) override;

    Result flush(felix_id_t fid) override;

    void set_periodic_callback(uint32_t period_us, Callback callback) override;

    void set_asynch_callback(Callback callback) override;

    void fire_asynch_callback() override;

    uint32_t get_progress_ptr() override;

    uint32_t get_resource_counter() override;

    uint32_t get_subscription_number() override;

    uint64_t get_resource_available_calls() override {return 0;};

private:
    void eventLoop(uint32_t port);
    void init_publish_socket(unsigned int max_msg_size);

    void on_msg_published(uint64_t);

    static void cb_on_msg_published(struct netio_unbuffered_publish_socket* socket, uint64_t key) {
        static_cast<NetioZerocopyPublisher*>(socket->usr)->on_msg_published(key);
    }


private:
    struct StreamCache
    {
        StreamCache()
        {
            netio_subscription_cache_init(&m_cache);
        }
        int m_again = 0;
        uint64_t key = 0;
        netio_subscription_cache m_cache;
    };

    struct LinkCache
    {
        std::array<StreamCache, 0x100> m_streams;
    };

    const std::string m_ip;
    const uint32_t m_port;
    Bus& m_bus;
    netio_buffer m_dma_buffer;
    std::unique_ptr<CompletionTable> m_ct;
    netio_unbuffered_socket_attr m_socket_attr;
    std::shared_ptr<netio_context> m_context;
    netio_unbuffered_publish_socket m_socket;
    netio_signal m_signal;  //interrupt-driven 
    netio_timer m_timer;    //polling readout
    Callback m_read_callback;
    std::array<LinkCache, 0x1000> m_cache;
    std::thread m_event_loop_thread;
};

#endif /* FELIX_NETIOZEROCOPYPUBLISHER_H_ */
