#include <bits/types/struct_iovec.h>
#include <cassert>
#include <sstream>

#include "network/netio_zerocopy_publisher.hpp"
#include "tohost_monitor.hpp"


NetioZerocopyPublisher::NetioZerocopyPublisher(
    const std::string &ip, uint32_t port, Bus& bus,
    unsigned int netio_pn, unsigned int netio_ps, unsigned int max_msg_size,
    uint64_t dma_buffer_vaddr, size_t dma_size)
    : m_ip{ip},
      m_port(port),
      m_bus(bus),
      m_dma_buffer{reinterpret_cast<void*>(dma_buffer_vaddr), dma_size, nullptr, 0},
      m_ct(new CompletionTable()),
      m_socket_attr{netio_pn, netio_ps},
      m_context(new netio_context())
{
    netio_init(m_context.get());
    init_publish_socket(max_msg_size);
    m_event_loop_thread = std::thread([this, port]{ eventLoop(port); });
}


NetioZerocopyPublisher::NetioZerocopyPublisher(
    const std::string &ip, uint32_t port, Bus& bus,
    unsigned int netio_pn, unsigned int netio_ps, unsigned int max_msg_size,
    uint64_t dma_buffer_vaddr, size_t dma_size, NetioEventLoop& evloop)
    : m_ip{ip},
      m_port(port),
      m_bus(bus),
      m_dma_buffer{reinterpret_cast<void*>(dma_buffer_vaddr), dma_size, nullptr, 0},
      m_ct(new CompletionTable()),
      m_socket_attr{netio_pn, netio_ps},
      m_context(evloop.ctx)
{
    init_publish_socket(max_msg_size);
}


void NetioZerocopyPublisher::init_publish_socket(unsigned int max_msg_size)
{
    unsigned int max_msg_by_net = m_socket_attr.buffer_size - sizeof(felix_id_t) - sizeof(uint8_t);
    m_max_msg_size = max_msg_size < max_msg_by_net ? max_msg_size : max_msg_by_net;
    m_max_iov_len = NETIO_MAX_IOV_LEN -1;
    memset(&m_socket, 0, sizeof m_socket);
    netio_unbuffered_publish_socket_init(&m_socket, m_context.get(), m_ip.c_str(), m_port, &m_dma_buffer);
    LOG_DBG("Initialised netio-next unbuffered_publish_socket. Port %u PROVIDER: %s", m_port ,m_socket.lsocket.fi->fabric_attr->prov_name);
    m_socket.usr = this;
    m_socket.cb_msg_published = cb_on_msg_published;

    netio_timer_init(&(m_context->evloop), &m_timer);
    netio_signal_init(&(m_context->evloop), &m_signal);
}


NetioZerocopyPublisher::~NetioZerocopyPublisher()
{
    netio_timer_stop(&m_timer);
    netio_terminate_signal(&(m_context->evloop));
    if ( m_event_loop_thread.get_id() != std::thread::id() ) {
        m_event_loop_thread.join();
    }
}

void NetioZerocopyPublisher::eventLoop(uint32_t port)
{
    std::ostringstream out;
    out << "zerocopypub[" << port << "]";
    std::string s = out.str();
    pthread_setname_np(pthread_self(), s.c_str());
    netio_run(&(m_context->evloop));
}


bool NetioZerocopyPublisher::declare(const std::vector<Elink> &elinks)
{
    return m_bus.publish(elinks, m_ip, m_port, m_socket_attr.num_buffers, m_socket_attr.buffer_size, true, true); //TODO: pass pages
}


Publisher::Result NetioZerocopyPublisher::publish(felix_id_t fid, iovec *iov,
     uint32_t iovlen, size_t bytes, uint32_t block_addr, std::uint8_t status)
{
    if(iovlen == 0){return Publisher::Result::OK;}

    // Truncate too big messages.
    // Thresholds are the max number of IOV entries (sender) and the page size (receiver).
    // + 1 for status byte
    if ( iovlen > m_max_iov_len or bytes > m_max_msg_size ){
        status = truncate_msg_if_too_large(iov, iovlen, status);
        *static_cast<uint8_t*>(iov[0].iov_base) = status;
    }

    //remove status byte
    if(iov[0].iov_len > 0){
        uint8_t* iov_ptr = static_cast<uint8_t*>(iov[0].iov_base);
        iov[0].iov_base = static_cast<void*>(iov_ptr + 1);
        --iov[0].iov_len;
    }

    StreamCache &cache = m_cache[(fid >> 16) & 0xfff].m_streams[fid & 0xff];
    unsigned netio_flags = 0;
    
    if (cache.m_again == 0) {
        cache.key = block_addr ;
        m_ct->push(block_addr);
    }
    else if (cache.m_again == 1) { //AGAIN
        cache.key = block_addr;
    }
    else { //PARTIAL
        //don't touch key
        netio_flags |= NETIO_REENTRY;
    }

    auto r = netio_unbuffered_publishv_usr(&m_socket, fid, iov, iovlen,
        &cache.key, netio_flags, &cache.m_cache, status, 1);

    if(r == NETIO_STATUS_OK || r == NETIO_STATUS_OK_NOSUB) {
        cache.m_again = 0;
        return Publisher::Result::OK;
    }
    else if(r == NETIO_STATUS_AGAIN) {
        // No data were sent, we need to redo the whole call
        cache.m_again = 1;
        return Publisher::Result::AGAIN;
    }
    else if(r == NETIO_STATUS_PARTIAL) {
        // Some data were sent, we need to redo the call but set NETIO_REENTRY
        cache.m_again = 2;
        return Publisher::Result::PARTIAL;
    }
    else if (r == NETIO_ERROR_MAX_IOV_EXCEEDED) {
        // Message too large, discarded.
        cache.m_again = 0;
        LOG_WARN("Message too large, IOV count %lu. Discarded.", iovlen);
        m_ct->update(block_addr);
        return Publisher::Result::ERROR_TOO_BIG;
    }
    else {
        //NETIO_STATUS_ERROR message discarded.
        cache.m_again = 0;
        LOG_WARN("Netio error, message discarded");
        m_ct->update(block_addr);
        return Publisher::Result::ERROR;
    }
}


Publisher::Result NetioZerocopyPublisher::publish(felix_id_t fid, uint8_t* data, size_t len)
{
    iovec iov;
    iov.iov_base = static_cast<void*>(data);
    iov.iov_len = len;
    // TODO: status byte not handled correctly
    return publish(fid, &iov, 1, len, 0, 0);
}


Publisher::Result NetioZerocopyPublisher::flush(felix_id_t fid)
{
    //Nothing to flush in zero-copy mode, harmless
    return Publisher::Result::OK;
}


void NetioZerocopyPublisher::set_periodic_callback(
    uint32_t period_us, Callback callback)
{
    m_read_callback = callback;
    m_signal.data = this;
    m_signal.cb = [](void *ptr)
    {
        NetioZerocopyPublisher* self = static_cast<NetioZerocopyPublisher*>(ptr);
        if (self->m_read_callback())
        {
            netio_signal_fire(&self->m_signal);
        }
    };

    m_timer.data = this;
    m_timer.cb = [](void *ptr)
    {
        NetioZerocopyPublisher* self = static_cast<NetioZerocopyPublisher*>(ptr);
        if (self->m_read_callback())
        {
            netio_signal_fire(&self->m_signal);
        }
    };
    netio_timer_start_us(&m_timer, period_us);
}


void NetioZerocopyPublisher::set_asynch_callback(Callback callback)
{
    m_read_callback = callback;
    m_signal.data = this;
    m_signal.cb = [](void *ptr)
    {
        NetioZerocopyPublisher* self = static_cast<NetioZerocopyPublisher*>(ptr);
        if (self->m_read_callback())
        {
            netio_signal_fire(&self->m_signal);
        }
    };
}


void NetioZerocopyPublisher::fire_asynch_callback()
{
    netio_signal_fire(&m_signal);
}


void NetioZerocopyPublisher::on_msg_published(uint64_t key)
{
    uint32_t offset = key & 0xffffffff;
    m_ct->update(offset);
}


uint32_t NetioZerocopyPublisher::get_resource_counter()
{
    return netio_pubsocket_get_available_co(&m_socket);
}


uint32_t NetioZerocopyPublisher::get_subscription_number()
{
    return m_socket.subscription_table.num_subscriptions;
}


uint32_t NetioZerocopyPublisher::get_progress_ptr()
{
    return m_ct->get_rd();
}
