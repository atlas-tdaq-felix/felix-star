#include <cassert>
#include <sstream>

#include "network/netio_buffered_publisher.hpp"


NetioBufferedPublisher::NetioBufferedPublisher(
    const std::string &ip, uint32_t port,
    Bus& bus, unsigned int netio_pn, unsigned int netio_ps,
    unsigned int netio_wm, unsigned int netio_to, unsigned int max_msg_size)
    : m_ip{ip},
      m_port(port),
      m_bus(bus),
      m_socket_attr{netio_pn, netio_ps, netio_wm, netio_to},
      m_context(new netio_context())
{
    netio_init(m_context.get());
    init_publish_socket(max_msg_size);
    m_event_loop_thread = std::thread([this, port]{ eventLoop(port); });
}


NetioBufferedPublisher::NetioBufferedPublisher(
    const std::string &ip, uint32_t port,
    Bus& bus, unsigned int netio_pn, unsigned int netio_ps, unsigned int netio_wm,
    unsigned int netio_to, unsigned int max_msg_size, NetioEventLoop& evloop)
    : m_ip{ip},
      m_port(port),
      m_bus(bus),
      m_socket_attr{netio_pn, netio_ps, netio_wm, netio_to},
      m_context(evloop.ctx)
{
    init_publish_socket(max_msg_size);
}


void NetioBufferedPublisher::init_publish_socket(unsigned int max_msg_size)
{
    unsigned int max_msg_by_net = m_socket_attr.pagesize - sizeof(felix_id_t) - sizeof(uint32_t); //netio buffer structure: fid | size | msg
    m_max_msg_size = max_msg_size < max_msg_by_net ? max_msg_size : max_msg_by_net;
    m_max_iov_len = 0; //No limit, all copied in one IOV in netio-next

    memset(&m_socket, 0, sizeof m_socket);
    netio_publish_socket_init(&m_socket, m_context.get(), m_ip.c_str(), m_port, &m_socket_attr);
    LOG_DBG("Initialised netio-next buffered_publish_socket. Port %u PROVIDER: %s", m_port, m_socket.lsocket.fi->fabric_attr->prov_name);
    netio_timer_init(&(m_context->evloop), &m_timer);
    netio_signal_init(&(m_context->evloop), &m_signal);

    m_socket.usr = this;
    m_socket.cb_buffer_available = cb_on_buffer_available;
}


NetioBufferedPublisher::~NetioBufferedPublisher()
{
    netio_timer_stop(&m_timer);
    netio_terminate_signal(&(m_context->evloop));
    if ( m_event_loop_thread.get_id() != std::thread::id() ) {
        m_event_loop_thread.join();
    }
}


void NetioBufferedPublisher::eventLoop(uint32_t port)
{
    std::ostringstream out;
    out << "pub[" << port << "]";
    std::string s = out.str();
    pthread_setname_np(pthread_self(), s.c_str());

    netio_run(&(m_context->evloop));
}


bool NetioBufferedPublisher::declare(const std::vector<Elink> &elinks)
{
    return m_bus.publish(elinks, m_ip, m_port, m_socket_attr.num_pages, m_socket_attr.pagesize);
}


Publisher::Result NetioBufferedPublisher::publish(
    felix_id_t fid, iovec *iov, uint32_t iovlen, size_t bytes, uint32_t block_addr, std::uint8_t status)
{
    StreamCache &cache = m_cache[(fid >> 16) & 0xfff].m_streams[fid & 0xff];

    // Truncate too big messages. 
    // The threshold is the netio page size, used also by the receiver.
    // + 1 for status byte
    if (bytes >= m_max_msg_size){
        status = truncate_msg_if_too_large(iov, iovlen, status);
        *static_cast<uint8_t*>(iov[0].iov_base) = status;
    }

    auto r = netio_buffered_publishv(&m_socket, fid, iov, iovlen,
                                     cache.m_again, &cache.m_cache);
    cache.m_again = (r == NETIO_STATUS_AGAIN);
    return (Publisher::Result)r;
}


Publisher::Result NetioBufferedPublisher::flush(felix_id_t fid)
{
    StreamCache &cache = m_cache[(fid >> 16) & 0xfff].m_streams[fid & 0xff];
    netio_buffered_publish_flush(&m_socket, fid, &cache.m_cache);
    return Publisher::Result::OK;
}


Publisher::Result NetioBufferedPublisher::publish(felix_id_t fid, uint8_t* data, size_t len)
{
    StreamCache &cache = m_cache[(fid >> 16) & 0xfff].m_streams[fid & 0xff];
    auto r = netio_buffered_publish(&m_socket, fid, static_cast<void*>(data), len, cache.m_again, &cache.m_cache);
    cache.m_again = (r == NETIO_STATUS_AGAIN);
    return (Publisher::Result)r;
}


void NetioBufferedPublisher::set_periodic_callback(
    uint32_t period_us, Callback callback)
{
    m_read_callback = callback;
    m_signal.data = this;
    m_signal.cb = [](void *ptr)
    {
        NetioBufferedPublisher* self = static_cast<NetioBufferedPublisher*>(ptr);
        if (self->m_read_callback())
        {
            netio_signal_fire(&self->m_signal);
        }
    };

    m_timer.data = this;
    m_timer.cb = [](void *ptr)
    {
        NetioBufferedPublisher* self = static_cast<NetioBufferedPublisher*>(ptr);
        if (self->m_read_callback())
        {
            netio_signal_fire(&self->m_signal);
        }
    };
    netio_timer_start_us(&m_timer, period_us);
}


void NetioBufferedPublisher::on_buffer_available()
{
    ++m_buf_available_calls;
    netio_signal_fire(&m_signal);
}


void NetioBufferedPublisher::set_asynch_callback(Callback callback)
{
    m_read_callback = callback;
    m_signal.data = this;
    m_signal.cb = [](void *ptr)
    {
        NetioBufferedPublisher* self = static_cast<NetioBufferedPublisher*>(ptr);
        if (self->m_read_callback())
        {
            netio_signal_fire(&self->m_signal);
        }
    };
}


void NetioBufferedPublisher::fire_asynch_callback()
{
    netio_signal_fire(&m_signal);
}

uint32_t NetioBufferedPublisher::get_subscription_number()
{
    return m_socket.subscription_table.num_subscriptions;
}


uint32_t NetioBufferedPublisher::get_resource_counter()
{
    return netio_pubsocket_get_minimum_pages(&m_socket);
}
