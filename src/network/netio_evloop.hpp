#ifndef FELIX_NETIO_EVLOOP_H_
#define FELIX_NETIO_EVLOOP_H_

#include "netio/netio.h"
#include <thread>
#include <memory>

class NetioEventLoop
{
    public:
        NetioEventLoop();
        void start_thread(const std::string& thread_name);
        void start();
        void stop();

        std::shared_ptr<netio_context> ctx;

    private:
        std::thread m_evloop_thread;
        void run(const std::string& thread_name);
};


#endif /* FELIX_NETIO_EVLOOP_H_ */