#ifndef FELIX_MONITOR_H_
#define FELIX_MONITOR_H_

#include <time.h>
#include <sys/timerfd.h>
#include <climits>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdint>
#include <string>

#include <nlohmann/json.hpp>

/**
 * Generic class to write json strings containing monitoring information
 * in UNIX fifos.
 */
class Monitor
{
    public:

        /**
         * @brief Monitor constructor.
         * @param fifoname file name of the UNIX fifo.
         */
        explicit Monitor(std::string& fifoname);

        ~Monitor();
        Monitor(Monitor&&) = delete;
        Monitor& operator=(Monitor&&) = delete;

        /**
         * @return serialised json std::string.
         */
        std::string get_serialized_message();

        /**
         * @brief write serialised message in fifo.
         */
        void write_message();

    protected:
        nlohmann::json m_message;

    private:
        int m_fifo_fd;
        bool is_fifo(const char *filename);
};

#endif /* FELIX_MONITOR_H_ */
