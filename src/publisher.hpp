#ifndef FELIX_DATAPUBLISHER_H_
#define FELIX_DATAPUBLISHER_H_

#include <sys/uio.h>

#include <cstdint>
#include <functional>
#include <vector>

#include "elink.hpp"

/**
 * Interface class to any publisher implemented by the network backend.
 */
class Publisher
{
    protected:
        size_t m_max_msg_size{};
        uint32_t m_max_iov_len{};
        /**
         * @brief Truncate the message if it is too large and returns status
         *
         * Changes iov to fit into message
         *
         * @param iov pointer to array of I/O vector
         * @param size number of entries in I/O vector array
         * @param status status byte
         * @return updated status byte
         */
        [[nodiscard]] std::uint8_t truncate_msg_if_too_large(iovec *iov, uint32_t &size, std::uint8_t status);

    public:
        using Callback = std::function<bool()>;

        enum Result
        {
            OK = 0,
            ERROR = 1,
            AGAIN = 2,
            PARTIAL = 3,
            ERROR_TOO_BIG = 4,
            NO_SUBSCRIBERS = 5,
            DECODING_ERROR = 7
        };

        virtual ~Publisher() = default;

        /** 
         * @brief advertise served e-links
         * @param elinks to advertise.
         * @return whether the operation was succesfull.
         */
        virtual bool declare(const std::vector<Elink> &elinks) = 0;

        /** 
         * @brief publish chunk.
         * @param fid e-link universal identifier.
         * @param iov pointer to array of I/O vector.
         * @param iovlen number of entries in I/O vector array.
         * @param bytes total message size in bytes.
         * @param block_addr address of block where the chunk starts (for zero-copy mode).
         * @param status felix-star status byte.
         * @return publisher-specific return code.
         */
        virtual Result publish(felix_id_t fid, iovec *iov, uint32_t iovlen, size_t bytes, uint32_t block_addr, std::uint8_t status) = 0;

        /** 
         * @brief publish chunk/message.
         * @param fid e-link universal identifier.
         * @param data pointer to the start of chunk/message to publish.
         * @param len size of message/chunk to publish.
         * @return publisher-specific return code.
         */
        virtual Result publish(felix_id_t fid, uint8_t *data, size_t len) = 0;

        /** 
         * @brief send right away (flush) the current buffer.
         * @param fid e-link universal identifier. All buffers containing data from
         * this e-link are flushed.
         * @return publisher-specific return code.
         */
        virtual Result flush(felix_id_t fid) = 0;

        /** 
         * @brief configure periodic callback e.g. the function used for polling input data.
         */
        virtual void set_periodic_callback(uint32_t period_us, Callback callback) = 0;

        /** 
         * @brief configure a callback with an asynchronous signal associated.
         */
        virtual void set_asynch_callback(Callback callback) = 0;

        /** 
         * @brief fire the signal associated to the asynchronous callback.
         */
        virtual void fire_asynch_callback() = 0;

        /** 
         * @brief zero-copy specific function to obtain the DMA read pointer
         * that can be set given the status of outstanding network transfers.
         * @return address in the DMA buffer.
         */
        virtual uint32_t get_progress_ptr() = 0;

        // monitoring
        /**    
         * @return the number of available network buffers or similar resources.
         */
        virtual uint32_t get_resource_counter() = 0;

        /**    
         * @return the number of subscriptions.
         */
        virtual uint32_t get_subscription_number() = 0;

        /**    
         * @return the number of calls occurring when network resources become
         * availale after exhaustion.
         */
        virtual uint64_t get_resource_available_calls() = 0;
};

#endif /* FELIX_DATAPUBLISHER_H_ */
