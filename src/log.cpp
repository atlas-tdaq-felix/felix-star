#include <ctime>
#include <unistd.h>
#include <stdarg.h>


#include "log.hpp"


static struct {
  int verbose;
} log_config;


void log_init(int verbose)
{
  log_config.verbose = verbose;
}


void _log_dump(const char* level, const char* filename, unsigned line, const char* functionname, int tid, const char *format, ...)
{
  va_list args;
  va_start(args, format);

  char timestamp[72];
  time_t ltime;
  ltime=time(NULL);
  struct tm *tm;
  tm=localtime(&ltime);

  sprintf(timestamp,"%04d-%02d-%02d %02d:%02d:%02d", tm->tm_year+1900, tm->tm_mon +1,
      tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
  fprintf(stderr, "%s [%s] ", timestamp, level);

  char msg[1024];
  vsnprintf(msg, 1024, format, args);
  fputs(msg, stderr);
  fputs("\n", stderr);
  fflush(stderr);
  va_end(args);
}


void _log(const char* level, const char* filename, unsigned line, const char* functionname, int tid, const char *format, ...)
{
  if (!log_config.verbose && !strcmp("DEBUG", level)) return;

  va_list args;
  va_start(args, format);

  char timestamp[72];
  time_t ltime;
  ltime=time(NULL);
  struct tm *tm;
  tm=localtime(&ltime);

  sprintf(timestamp,"%04d-%02d-%02d %02d:%02d:%02d", tm->tm_year+1900, tm->tm_mon +1,
      tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);

  printf("%s [%s] ", timestamp, level);
  char msg[1024];
  vsnprintf(msg, 1024, format, args);

  puts(msg);
  log_flush();
  va_end(args);
}

void log_flush()
{
  fflush(stdout);
}
