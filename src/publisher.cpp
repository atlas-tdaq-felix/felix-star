#include "publisher.hpp"
#include "tohost_monitor.hpp"
#include "log.hpp"

std::uint8_t Publisher::truncate_msg_if_too_large(iovec *iov, uint32_t& size, std::uint8_t status)
{
    //IOV length
    if (m_max_iov_len != 0 and size > m_max_iov_len) {
        size = m_max_iov_len;
        LOG_DBG("Truncating to IOV %u", m_max_iov_len);
        status |= SW_TRUNC;
    }

    //Max message size
    size_t byte_counter = 1;  // status byte
    for (uint32_t i = 0; i < size; ++i){
        byte_counter += iov[i].iov_len;
        if (byte_counter > m_max_msg_size){
            size_t retain_iov_len = iov[i].iov_len - (byte_counter - m_max_msg_size);
            if (retain_iov_len == 0){
                LOG_DBG("Truncating IOV: length was %u now is %u", size, i);
                size = i;
            } else {
                LOG_DBG("Truncating IOV and last entry: length was %u now is %u, last entry was %u bytes, now %u", size, i+1, iov[i].iov_len, retain_iov_len);
                size = i+1;
                iov[i].iov_len = retain_iov_len;                
            }
            status |= SW_TRUNC;
        }
    }
    return status;
}
