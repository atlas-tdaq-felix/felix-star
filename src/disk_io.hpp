#ifndef FELIX_DISKIO_H_
#define FELIX_DISKIO_H_

#include <string>
#include <string_view>

std::string errno_msg(int errn);

/**
 * Class used to read from or write into files or Unix fifos.
 * Used in tests when hardware is emulated in software.
 */
class DiskIO
{
    public:
        static constexpr std::string_view FREAD = "r";
        static constexpr std::string_view FWRITE = "w+";

        /**
         * @brief DiskIO constructor.
         * @param filename file or fifo to open.
         * @param block_size felix block size.
         * @param op read or write.
         * @param is_fifo true if fifo, file otherwise.
         */
        DiskIO(const std::string& filename, size_t block_size, std::string_view op, bool is_fifo);

        DiskIO(const DiskIO &) = delete;
        DiskIO &operator=(const DiskIO &) = delete;

        ~DiskIO();

        /**
         * @brief read blocks from file/fifo.
         * @param to_read read from stream to this destination.
         * @param max_blocks to read.
         * @return number of blocks read.
         */
        size_t block_read(void *to_read, size_t max_blocks);

        /**
         * @brief write blocks into file/fifo.
         * @param to_write write this data into the output stream.
         * @param max_blocks to write.
         * @return number of blocks written.
         */
        size_t block_write(const void *to_write, size_t max_blocks);

        /**
         * @brief read bytes from file/fifo.
         * @param to_read read from file/fifo to this destination.
         * @param bytes maximum number of bytes to read.
         * @return number of bytes read.
         */
        size_t byte_read(void* to_read, size_t bytes);

        /**
         * @brief write bytes to file/fifo.
         * @param to_write write this data into the fifo/file.
         * @param bytes maximum number of bytes to write.
         * @return number of bytes written.
         */
        size_t byte_write(void* to_write, size_t bytes);

        /**
         * @return if the end-of-file has been reached.
         */
        bool is_eof();

        /**
         * @return go back to the start of file.
         */
        int reset_position();

    private:
        std::string m_filename{};
        size_t m_block_size{0};
        int m_fd{-1};
        FILE* m_fstream{nullptr};

        void open_file(std::string_view op);
        void open_fifo(std::string_view op);
};


#endif /* FELIX_DISKIO_H_ */
