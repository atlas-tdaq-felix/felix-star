#ifndef FELIX_BLOCK_H_
#define FELIX_BLOCK_H_


#include <cstdint>
#include <sys/uio.h>

#include "felix/felix_client_status.h"

#define BLOCK_HEADER_SIZE  (4)
#define MAX_IOV_SIZE (28)       //max max_sge of Connect-X5 is 30
#define MAX_CHUNK_SIZE  (26624) //27 sub-chunks max to fit in 27 IOV entries
//#define MAX_BUF_CHUNK_SIZE  (12288)

#define CHUNK_FW_TRUNC FELIX_STATUS_FW_TRUNC
#define CHUNK_SW_TRUNC FELIX_STATUS_SW_TRUNC
#define CHUNK_FW_MALF  FELIX_STATUS_FW_MALF
#define CHUNK_FW_CRC   FELIX_STATUS_FW_CRC
#define CHUNK_SW_MALF  FELIX_STATUS_SW_MALF


struct Block {
    uint32_t elink :11;
    uint32_t sequence_number :5;
    uint16_t marker;
    uint8_t data[1020];
} __attribute__((packed));

/*
template <std::size_t block_size>
struct Block {
    uint32_t elink :11;
    uint32_t sequence_number :5;
    uint16_t marker;
    uint8_t data[block_size-4];
} __attribute__((packed));
*/

#if REGMAP_VERSION < 0x0500
union subchunk_trailer_t
{
    struct
    { // 16-bit trailer
        uint32_t length   : 10;
        uint32_t crcerr   :  1;
        uint32_t err      :  1;
        uint32_t trunc    :  1;
        uint32_t type     :  3;
    } __attribute__((packed)) data;
    uint16_t value;
};

#else
union subchunk_trailer_t
{
    struct
    { // 32-bit trailer
        uint32_t length   : 16;
        uint32_t unused   :  9;
        uint32_t busymask :  1;
        uint32_t crcerr   :  1;
        uint32_t err      :  1;
        uint32_t trunc    :  1;
        uint32_t type     :  3;
    } __attribute__((packed)) data;
    uint32_t value;
};
#endif

struct subchunk_t {
    uint32_t pos;
    subchunk_trailer_t trailer;
};


union xl1id_t
{
    struct {
        uint32_t ec : 24;
        uint8_t ecrc;
    } fields;
    uint32_t xl1id;
};


union bcid_t
{
    struct {
        uint8_t fmt : 8;
        uint8_t length: 8;
        uint16_t bcid : 12;
        uint8_t reserved : 4;
    } fields;
    uint32_t bc_data;
} ;

#endif /* FELIX_BLOCK_H_ */
