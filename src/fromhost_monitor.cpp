#include <format>
#include "fromhost_monitor.hpp"

FromHostDmaStats FromHostDmaStats::get_increment(FromHostDmaStats & previous)
{
    struct timespec now;
    FromHostDmaStats output(*this);
    clock_gettime(CLOCK_MONOTONIC_RAW , &now);
    float seconds = now.tv_sec - previous.ts.tv_sec + 1e-9*(now.tv_nsec - previous.ts.tv_nsec);
    float msg_increment = static_cast<float>(output.msg_counter - previous.msg_counter);
    float bytes_increment = static_cast<float>(output.bytes_counter - previous.bytes_counter);
    //rates
    output.msg_rate_Hz = msg_increment/seconds;
    output.msg_rate_Mbps = ((8*bytes_increment) / seconds) / 1024. / 1024.;
    previous = output;
    previous.ts = now;
    return output;
}


void FromHostElinkStats::on_processed_msg(uint32_t size)
{
    ++processed_msg;
    if ( size > largest_msg_size){
        largest_msg_size = size;
    }
    processed_bytes += size;
}


FromHostElinkStats FromHostElinkStats::get_increment(FromHostElinkStats & previous)
{
    assert(this->fid == previous.fid);
    struct timespec now;
    FromHostElinkStats output(previous.fid);

    //Counters
    output.processed_msg    = this->processed_msg;
    output.processed_bytes  = this->processed_bytes;
    output.dropped_msg      = this->dropped_msg;
    output.largest_msg_size = this->largest_msg_size;

    //Rates
    clock_gettime(CLOCK_MONOTONIC_RAW , &now);
    float seconds = now.tv_sec - previous.ts.tv_sec + 1e-9*(now.tv_nsec - previous.ts.tv_nsec);
    float msg_increment = static_cast<float>(output.processed_msg - previous.processed_msg);
    float bytes_increment = static_cast<float>(output.processed_bytes - previous.processed_bytes);    
    output.rate_msg_Hz      = msg_increment / seconds;
    output.rate_msg_Mbps    = ((8*bytes_increment) / seconds) / 1024. / 1024.;

    //update old version
    previous.processed_msg      = this->processed_msg;
    previous.processed_bytes    = this->processed_bytes;
    previous.ts                 = now;

    return output;
}


void FromHostMonitor::append_device_stats(const std::string& ts, const std::string& hostname, const FromHostDeviceStats & ds)
{
};


void FromHostMonitor::append_dma_stats(const std::string& ts, const std::string& hostname, int device, const FromHostDmaStats & ds)
{
    nlohmann::json j;
    j["dma_free_MB"] = ds.dma_free_MB;
    j["msg_rate_Hz"] = ds.msg_rate_Hz;
    j["msg_rate_Mbps"] = ds.msg_rate_Mbps;

    m_message = nlohmann::json();
    m_message["ts"] = ts;
    m_message["host"][hostname]
             ["app"]["fromhost"]
             ["device"][std::to_string(device)]
             ["dma"][std::to_string(ds.dmaid)] = j;

    write_message();
}

void FromHostMonitor::append_writer_stats(const std::string& ts, const std::string& hostname, int device, int dma_id, const FromHostWriterStats & ws)
{
    nlohmann::json j;
    j["type"] = ws.type;
    j["number_of_connections"] = ws.number_of_connections;

    m_message = nlohmann::json();
    m_message["ts"] = ts;
    m_message["host"][hostname]
             ["app"]["fromhost"]
             ["device"][std::to_string(device)]
             ["dma"][std::to_string(dma_id)]
             ["thread"][std::to_string(ws.writer_id)] = j;

    write_message();
}

void FromHostMonitor::append_elink_stats(const std::string& ts, const std::string& hostname, int device, int dma_id, int reader_id, const FromHostElinkStats & es)
{
    nlohmann::json j;
    j["msgs"]          = es.processed_msg;
    j["bytes"]         = es.processed_bytes;
    j["max_msg_size"]  = es.largest_msg_size;
    j["dropped_msgs"]  = es.dropped_msg;
    j["rate_msg_Hz"]   = es.rate_msg_Hz;
    j["rate_msg_Mbps"] = es.rate_msg_Mbps;

    m_message = nlohmann::json();
    m_message["ts"] = ts;
    m_message["host"][hostname]
             ["app"]["fromhost"]
             ["device"][std::to_string(device)]
             ["dma"][std::to_string(dma_id)]
             ["thread"][std::to_string(reader_id)]
             ["fid"][std::format("{:#0x}", es.fid)] = j;

    write_message();
}
