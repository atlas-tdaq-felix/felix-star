#ifndef FELIX_TOHOST_MONITOR_H_
#define FELIX_TOHOST_MONITOR_H_

#include "monitor.hpp"

enum ChunkStatus {
    FW_TRUNC = 1, SW_TRUNC = 2, FW_MALF = 4, FW_CRC = 8, SW_MALF = 16
};

/**
 * Monitoring info for one Device.
 */
struct ToHostDeviceStats
{
    int device_id;

    explicit ToHostDeviceStats(int d)
        : device_id(d) {};
};


/**
 * Monitoring info for one DMA buffer.
 */
struct ToHostDmaStats
{
    int dmaid;
    uint32_t dma_free_MB;
    uint64_t irq_count;

    explicit ToHostDmaStats(int dm, int mb, uint64_t irq)
        : dmaid(dm), dma_free_MB(mb), irq_count(irq) {};
};


/**
 * Monitoring info for each reader associated to a DMA buffer.
 */
struct ToHostReaderStats
{
    int reader_id;
    int type;
    uint32_t number_of_subscriptions;
    uint32_t number_of_network_resources;
    uint64_t number_of_network_resource_calls;

    explicit ToHostReaderStats(int ri, int t, uint32_t s, uint32_t r, uint64_t c) :
        reader_id(ri),
        type(t),
        number_of_subscriptions(s),
        number_of_network_resources(r),
        number_of_network_resource_calls(c) {};
};


/**
 * Monitoring info for each decoder (one per e-link) part of a reader.
 */
struct ToHostElinkStats
{
    uint64_t fid                 = 0;
    uint64_t processed_blocks    = 0;
    uint64_t processed_chunks    = 0;
    uint64_t processed_bytes     = 0;
    float rate_chunks_kHz        = 0;
    uint64_t dropped_blocks      = 0;
    uint32_t largest_chunk_size  = 0;
    uint32_t average_chunk_bytes = 0;
    uint64_t sw_trunc_chunks     = 0;
    uint64_t fw_trunc_chunks     = 0;
    uint64_t sw_error_chunks     = 0;
    uint64_t fw_error_chunks     = 0;
    uint64_t fw_crc_chunks       = 0;
    uint64_t oosequence_l0id     = 0;
    struct timespec ts;

    void increment_processed_blocks(){++processed_blocks;}
    void increment_dropped_blocks(){++dropped_blocks;}
    void increment_oosequence_l0id(){++oosequence_l0id;}
    void update_processed_chunk(uint8_t e, uint32_t s);
    ToHostElinkStats get_increment(ToHostElinkStats & stats);
    ToHostElinkStats() = default;

    explicit ToHostElinkStats(uint64_t f) : fid(f), processed_blocks(0),  processed_chunks(0),
        processed_bytes(0), rate_chunks_kHz(0),
        dropped_blocks(0),largest_chunk_size(0), sw_trunc_chunks(0), fw_trunc_chunks(0),
        sw_error_chunks(0), fw_error_chunks(0), fw_crc_chunks(0), oosequence_l0id(0)
        {
            clock_gettime(CLOCK_MONOTONIC_RAW , &ts);
        };
};


/**
 * Monitoring information for the felix-tohost application.
 * ToHostMonitor gathers the hierarchical monitoring data (buffers > readers > decoders)
 * and returns a json string.
 */
class ToHostMonitor : public Monitor
{
    public:
        explicit ToHostMonitor(std::string& fifoname) : Monitor(fifoname) {
            m_message = nlohmann::json::array();
        }
        //These functions are called in a nested loop with hierarchy
        //devices > dmas > readers > elinks.
        /**
         * @brief append to the JSON message device-level information.
         * @param ts the timestamp
         * @param hostname the hostname
         * @param s data structure containing device statistics
         */
        void append_device_stats(const std::string& ts, const std::string& hostname, const ToHostDeviceStats & s);

        /**
         * @brief append to the JSON message buffer-level information.
         * @param ts the timestamp
         * @param hostname the hostname
         * @param deviceid the device identifier.
         * @param s data structure containing DMA buffer statistics
         */
        void append_dma_stats(const std::string& ts, const std::string& hostname, int deviceid, const ToHostDmaStats & s);

        /**
         * @brief append to the JSON message reader-level information.
         * @param ts the timestamp
         * @param hostname the hostname
         * @param deviceid the device identifier.
         * @param dma_id the dma identifier
         * @param s data structure containing reader statistics
         */
        void append_reader_stats(const std::string& ts, const std::string& hostname, int deviceid, int dma_id, const ToHostReaderStats & s);

        /**
         * @brief append to the JSON message decoder-level information.
         * @param ts the timestamp
         * @param hostname the hostname
         * @param deviceid the device identifier.
         * @param dma_id the dma identifier
         * @param reader_id the reader id
         * @param s data structure containing e-link statistics
         */
        void append_elink_stats(const std::string& ts, const std::string& hostname, int device, int dma_id, int reader_id, const ToHostElinkStats & s);
};

#endif /* FELIX_TOHOST_MONITOR_H_ */
