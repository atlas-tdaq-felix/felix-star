#ifndef REGMAP_MANAGER_H_
#define REGMAP_MANAGER_H_

#include <filesystem>
#include <string>

// NOTE: Ignore the deprecated-declarations warning of gcc13, see FLX-2310
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "yaml-cpp/yaml.h"
#pragma GCC diagnostic pop


/**
 * RegmapManager parses the YAML file that describes the register map,
 * therefore it knows whether a register exists, it is readable, writable
 * and whether it is attached to the secondary PCIe endpoint.
 * RegmapManager does not interact with the FELIX card (real or emulated).
 */
class RegmapManager
{
    public:
        RegmapManager();

        /**
         * @param name the register name.
         * @return whether the register is readable.
         */
        bool can_read(const std::string& name) {
            return has_type(name, "REGMAP_REG_READ");
        }

        /**
         * @param name the register name.
         * @return whether the register is writable.
         */
        bool can_write(const std::string& name) {
            return has_type(name, "REGMAP_REG_WRITE");
        }

        /**
         * @param name the register name.
         * @return whether the register is used also by the secondary PCIe endpoint.
         */
        bool has_endpoint_1(const std::string& name) {
            return has_endpoint(name, "REGMAP_ENDPOINT_1");
        }

    private:
        std::filesystem::path m_regmap_file;
        YAML::Node m_regmap;

        bool has_key(std::string name, std::string key, const std::string& value);
        bool has_type(const std::string& name, const std::string& type);
        bool has_endpoint(const std::string& name, const std::string& endpoint);
};



#endif /* REGMAP_MANAGER_H_ */