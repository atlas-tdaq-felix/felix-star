#include "fromhost.hpp"
#include "log.hpp"

static volatile std::sig_atomic_t signal_status;
void signal_handler(int s){signal_status = s;}

int main(int argc, char** argv)
{
    std::unique_ptr<ConfigToFlx> c = std::make_unique<ConfigToFlx>();
    c->parse(argc, argv);

    auto fh = FromHost<ConfigToFlx, FlxDevice, FlxFromHostBuffer>(std::move(c));

    for (auto & dev_no : fh.cfg->resource.device){
        fh.devices.emplace_back(new FlxDevice(*fh.cfg, dev_no)); //TODO: fhis wants a ToHost config. Needs refactoring or inheritance check
        fh.dma_buffers.emplace(dev_no, new FlxFromHostBuffer(fh.devices.back()));
    }

    fh.start();

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (signal_status == 0) {
        fh.print_monitoring();
        usleep(fh.cfg->stats.monitoring_period_ms * 1000);
    }
    LOG_INFO("Received signal %d, exiting...", signal_status);
    return 0;
}
