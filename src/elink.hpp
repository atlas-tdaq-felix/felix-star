 #ifndef FELIX_ELINK_H_
#define FELIX_ELINK_H_

#include <cstdint>

typedef uint16_t local_elink_t;
typedef uint64_t felix_id_t;


enum elink_type_t : uint8_t {
    NONE_ELINK_TYPE,
    DAQ,
    TTC,
    DCS,
    IC
};


enum encoding_t : uint8_t {
    ENC_DIRECT,
    ENC_8b10b,
    ENC_HDLC,
    ENC_TTC
#if REGMAP_VERSION >= 0x0500
    ,
    ENC_8b10b_STRIPS,
    ITk_ItkPixel,
    Endeavour,
    ENC_INTERLAKEN
#endif
};


struct Elink {
    felix_id_t fid;
    local_elink_t lid;
    bool has_streams;
    elink_type_t type;
    encoding_t enc;

    Elink(felix_id_t f, local_elink_t l, bool s, elink_type_t t, encoding_t e) :
        fid(f), lid(l), has_streams(s), type(t), enc(e) {};

    explicit Elink(felix_id_t f) :
        fid(f), lid(0), has_streams(false), type(NONE_ELINK_TYPE), enc(ENC_DIRECT) {};
};

#endif /* FELIX_ELINK_H_ */

