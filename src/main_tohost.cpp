#include "tohost.hpp"
#include "log.hpp"

static volatile std::sig_atomic_t signal_status;
void signal_handler(int s){signal_status = s;}

int main(int argc, char** argv)
{
    std::unique_ptr<ConfigToHost> c = std::make_unique<ConfigToHost>();
    c->parse(argc, argv);

    auto th = ToHost<ConfigToHost, FlxDevice, FlxToHostBuffer>(std::move(c));

    for (auto & dev_no : th.cfg->resource.device){
        th.devices.emplace_back(new FlxDevice(*th.cfg, dev_no));
        for(int id : th.cfg->resource.dma_ids){
            int unique_id = th.cfg->get_unique_dmaid(id, dev_no);
            th.dma_buffers.emplace(unique_id, new FlxToHostBuffer(id, th.devices.back()));
        }
    }

    th.start();

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (signal_status == 0) {
        th.print_monitoring();
        usleep(th.cfg->stats.monitoring_period_ms * 1000);
    }
    LOG_INFO("Received signal %d, exiting...", signal_status);
    th.stop();
    return 0;
}
