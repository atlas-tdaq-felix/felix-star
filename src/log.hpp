#pragma once

#include <cstring>
#include <cstdio>
#include <pthread.h>

void log_init(int verbose);
void log_flush();

void _log(const char* level, const char* filename, unsigned line, const char* functionname, int tid, const char *format, ...);
void _log_dump(const char* level, const char* filename, unsigned line, const char* functionname, int tid, const char *format, ...);

#define LOG_DBG(...)  _log("DEBUG",    __FILE__, __LINE__,  __func__, (int) pthread_self(), __VA_ARGS__)
#define LOG_INFO(...) _log("INFO",     __FILE__, __LINE__,  __func__, (int) pthread_self(), __VA_ARGS__)
#define LOG_WARN(...) _log("WARNING",  __FILE__, __LINE__,  __func__, (int) pthread_self(), __VA_ARGS__)
#define LOG_ERR(...)  _log("ERROR",  __FILE__, __LINE__, __func__, (int) pthread_self(), __VA_ARGS__)
#define LOG_DUMP(...) _log_dump("ERROR",  __FILE__, __LINE__, __func__, (int) pthread_self(), __VA_ARGS__)

#ifdef ENABLE_TRACE
# define LOG_TRACE(...) _log("TRACE", __FILE__, __LINE__, __func__, (int) pthread_self(), __VA_ARGS__)
#else
# define LOG_TRACE(...)
#endif
