#ifndef TOHOST_BUFFER_H_
#define TOHOST_BUFFER_H_

#include <cstddef>
#include <cstdint>
#include <thread>
#include <span>
#include <mutex>
#include <shared_mutex>
#include <cstdio>
#include <condition_variable>

#include "device.hpp"
#include "cmem_buffer.hpp"
#include "block.hpp"


#if REGMAP_VERSION < 0x500
    #define IRQ_WRAP_AROUND_FROM_HOST 0
    #define IRQ_WRAP_AROUND_TO_HOST 1
    #define IRQ_DATA_AVAILABLE 2
    #define IRQ_FIFO_FULL_FROM_HOST 3
    #define IRQ_BUSY_CHANGE_TO_HOST 6
    #define IRQ_FIFO_FULL_TO_HOST 7
#else
    #define IRQ_DATA_AVAILABLE 0
    #define IRQ_XOFF_TO_HOST 5
    #define IRQ_BUSY_CHANGE_TO_HOST 6
    #define IRQ_FIFO_FULL_TO_HOST 7
#endif

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))


/**
 * Abstract class representing a ToHost DMA buffer.
 * The elinks enabled for the corresponding DMA buffers are read from the
 * device in the constructors.
 */
class ToHostBuffer
{
    public:
        ToHostBuffer(int dmaid, std::shared_ptr<Device> d);

        //Interrupts
        /**
         * \defgroup Interrupt handling
         * @{
         * Functions to enable/disable for MSI-X interrupts.
         */
        /**
         * @brief enable on_data interrupt for this DMA buffer.
         */
        void irq_data_enable() {m_device->irq_enable(m_irq_on_data);}

        /**
         * @brief disable on_data interrupt for this DMA buffer.
         */
        void irq_data_disable(){m_device->irq_disable(m_irq_on_data);}

        /**
         * @brief enable busy interrupt for this DMA buffer.
         */
        void irq_busy_enable() {m_device->irq_enable(IRQ_BUSY_CHANGE_TO_HOST);}

        /**
         * @brief disable busy interrupt for this DMA buffer.
         */
        void irq_busy_disable(){m_device->irq_disable(IRQ_BUSY_CHANGE_TO_HOST);}
        /**
         * @}
         */

        /**
         * @return a shared pointer to the device serving this DMA buffer.
         */ 
        std::shared_ptr<Device> get_device(){return m_device;}

        /**
         * @return identifying number of this DMA buffer.
         */ 
        int get_dmaid() const {return m_dmaid;}

        /**
         * @return read e-links enabled for this DMA buffer.
         */ 
        std::vector<Elink> get_elinks(){return m_device->read_enabled_elinks(m_dmaid);}

        /**
         * @return read e-links enabled for this DMA buffer of a given type.
         * @param t the e-link type, determined from the encoding.
         */
        std::vector<Elink> get_elinks_of_type(elink_type_t t){return m_device->get_enabled_elinks_of_type(m_dmaid, t);};

        /**
         * @return fraction of e-links enabled for this DMA buffer of a given type.
         * @param t the e-link type, determined from the encoding.
         */
        std::vector<std::vector<Elink>> split_elinks_of_type(elink_type_t t, size_t n);

        //DMA buffer operations
        /**
         * \defgroup DMA operations
         * @{
         * Functions to manage DMA read/write operations.
         */
        /**
         * @brief fraction of e-links enabled for this DMA buffer of a given type.
         * @param name of the buffers passed to the CMEM driver.
         * @param vmem if true, do not use CMEM but malloc a buffer in memory.
         * @param free_previous_cmem if true, re-claim a buffer with the same name if present and not locked.
         */
        virtual void allocate_buffer(size_t size,
                            const std::string& name,
                            bool vmem, bool free_previous_cmem=true) = 0;

        /**
         * @brief inform the FELIX card of the allocated buffer, to be used as a circular DMA buffer.
         */
        virtual void     dma_start_continuous()                  = 0; //start_circular_dma

        /**
         * @brief wait for an on-data MSI-X interrupt.
         */
        virtual void     dma_wait_for_data_irq()                 = 0; //wait_data_available

        /**
         * @return number of bytes written by firmware, to be read by software.
         */
        virtual size_t   dma_bytes_available()                   = 0; //bytes_available

        /**
         * @return number of bytes written by firmware up to the end of the buffer,
         * to be read by software. 
         */
        virtual size_t   dma_bytes_available_nowrap()            = 0; //bytes_available_to_read

        /**
         * @return true if the DMA buffer is full.
         */
        virtual bool     dma_is_full()                           = 0;

        /**
         * @return address in virtual memory of write pointer (moved by firmware).
         */
        virtual uint64_t dma_get_write_ptr()                     = 0;

        /**
         * @return address in virtual memory of read pointer (moved by software).
         */
        virtual uint64_t dma_get_read_ptr()                      = 0;

        /**
         * @brief set a new read pointer.
         * @details software uses the virtual addresses, firmware uses the physical ones.
         * @param v_addr virtual address of new read pointer.
         */
        virtual void     dma_set_read_ptr_vaddr(uint64_t v_addr) = 0;

        /**
         * @brief set a new read pointer.
         * @details software uses the virtual addresses, firmware uses the physical ones.
         * @param p_addr physical address of new read pointer.
         */
        virtual void     dma_set_read_ptr_paddr(uint64_t p_addr) = 0;

        /**
         * @return read pointer offset with respect to the start of buffer.
         */
        virtual uint64_t dma_get_read_offset()                   = 0;

        /**
         * @return write pointer offset with respect to the start of buffer.
         */
        virtual uint64_t dma_get_write_offset()                  = 0;

        /**
         * @brief advance read pointer.
         * @param bytes number of bytes processed by software.
         */
        virtual void     dma_advance_read_ptr(size_t bytes)      = 0;

        /**
         * @return virtual address of start of buffer.
         */
        uint64_t         dma_get_vaddr(){return m_buffer->vaddr;}

        /**
         * @return buffer size.
         */
        size_t           dma_get_size(){return m_buffer->size;}
        /**
         * @}
         */

        /**
         * \defgroup Multi-reader functions
         * @{
         * Functions to manage readers' opearation
         */
        /**
         * @brief register a new reader to this DMA buffer.
         * @param zc_flag true if the reader uses zero-copy.
         * @return reader identifier.
         */
        uint32_t         reader_register(bool zc_flag);

        /**
         * @param reader_id reader identifier.
         * @return whether there are data in the DMA buffer for this reader to read.
         */
        bool             reader_is_data_available(uint32_t reader_id);

        /**
         * @param reader_id reader identifier.
         * @return bytes available for reading for the given reader.
         */
        size_t           reader_get_available_bytes(uint32_t reader_id);

        /**
         * @param reader_id reader identifier.
         * @return blocks to be read by the given reader.
         */
        std::span<Block> reader_get_available_blocks(uint32_t reader_id);

        /**
         * @brief advance the DMA buffer read pointer according to the readers' progress
         * @details single synchronisation point of the different ToHostReaders.
         * @param reader_id reader identifier.
         * @param read_bytes bytes read by the reader.
         * @param reader_id bytes sent by the reader (sent != read only in zero-copy mode).
         */
        void             reader_advance_read_ptr(uint32_t reader_id,  uint32_t read_bytes, uint32_t sent_bytes);
        /**
         * @}
         */

        /**
         * \defgroup Support for zero-copy module
         * @{
         * Functions to enable zero-copy flags.
         */
        /**
         * @return whether a zero-copy reader is present among those registered to this buffer.
         */
        bool             has_zero_copy_reader() const {return m_has_zero_copy_reader;}

        /**
         * @brief set flag for the presence of a zero-copy reader.
         */
        void             set_zero_copy_reader(){m_has_zero_copy_reader = true;}
        /**
         * @}
         */

        /**
         * @brief stop reading operations.
         */
        void             stop(){m_stop_flag = true;}

        /**
         * @return whether read operations have been stopped.
         */
        bool             is_stopped() const {return m_stop_flag;}

        /**
         * \defgroup Monitoring information
         * @{
         * Functions to manage readers' opearation
         */
        /**
         * @return free space in the buffer in MB.
         */
        uint32_t dma_get_free_MB();

        /**
         * @brief increase the on-data interrupt counter.
         */
        void dma_increase_irq_counter(){++m_irq_counter;}

        /**
         * @return the number of on-data interrupts.
         */
        uint64_t dma_get_irq_counter() const {return m_irq_counter;}
        /**
         * @}
         */

    protected:
        int m_dmaid;
        std::shared_ptr<Device> m_device;
        unsigned int m_block_size;
        int m_irq_on_data; //IRQ_DATA_AVAILABLE + m_dmaid;
        uint64_t m_irq_counter;
        size_t m_size;
        bool m_stop_flag; //stop flag for readers
        bool m_has_zero_copy_reader;
        size_t m_min_bytes_sent;

        std::mutex m_mutex;
        std::unique_ptr<DmaBuffer> m_buffer;

        //support for multiple ToHostBufferReader
        std::vector<uint64_t> m_bytes_read; //read from DMA buffer
        std::vector<uint64_t> m_bytes_sent; //different from read only for zc mode

        size_t dma_compute_bytes_to_read(uint64_t rd_ptr, uint64_t wr_ptr, bool even);
};


inline size_t ToHostBuffer::dma_compute_bytes_to_read(uint64_t rd_ptr, uint64_t wr_ptr, bool even)
{
    if( wr_ptr > rd_ptr ) {
        return wr_ptr - rd_ptr;
    }
    else if( wr_ptr < rd_ptr ) {
        return m_buffer->size + wr_ptr -rd_ptr;
    }
    else { // dma_ptr == buf->pc_ptr
        if( even ) {
            return 0;         // Buffer empty
        }
        else {
            return m_buffer->size; // Buffer full
        }
    }
}


/**
 * Reader of a Felix card ToHost DMA buffer. 
 * */
class FlxToHostBuffer : public ToHostBuffer {

    public:
        FlxToHostBuffer(int m_dmaid, std::shared_ptr<Device> d);
        ~FlxToHostBuffer();


        void allocate_buffer(size_t size,
                            const std::string& name,
                            bool vmem, bool free_previous_cmem=false) override;

        void     dma_start_continuous()                  override;
        void     dma_wait_for_data_irq()                 override;
        size_t   dma_bytes_available()                   override;
        //uint32_t dma_get_free_MB()                       override;
        size_t   dma_bytes_available_nowrap()            override;
        bool     dma_is_full()                           override;
        uint64_t dma_get_write_ptr()                     override;
        uint64_t dma_get_read_ptr()                      override;
        void     dma_set_read_ptr_vaddr(uint64_t v_addr) override;
        void     dma_set_read_ptr_paddr(uint64_t p_addr) override;
        uint64_t dma_get_read_offset()                   override;
        uint64_t dma_get_write_offset()                  override;
        void     dma_advance_read_ptr(size_t bytes)      override;
};


/**
 * Reader for the ToHost DMA buffer with hardware emulated in software.
 * A dedicated thread fills writes in the DMA buffer in stead of the FELIX card. 
 * */
class FileToHostBuffer : public ToHostBuffer {

    public:
        FileToHostBuffer(int m_dmaid, std::shared_ptr<Device> d,
            std::string& filename, unsigned int block_rate, bool repeat);
        ~FileToHostBuffer();
        void allocate_buffer(size_t size,
                            const std::string& name,
                            bool vmem, bool free_previous_cmem=true) override;

        void     dma_start_continuous()                  override;
        void     dma_wait_for_data_irq()                 override;
        size_t   dma_bytes_available()                   override;
        //uint32_t dma_get_free_MB()                       override;
        size_t   dma_bytes_available_nowrap()            override;
        bool     dma_is_full()                           override;
        uint64_t dma_get_write_ptr()                     override;
        uint64_t dma_get_read_ptr()                      override;
        void     dma_set_read_ptr_vaddr(uint64_t v_addr) override;
        void     dma_set_read_ptr_paddr(uint64_t p_addr) override;
        uint64_t dma_get_read_offset()                   override;
        uint64_t dma_get_write_offset()                  override;
        void     dma_advance_read_ptr(size_t bytes)      override;

    private:
        void    set_read_ptr_paddr(uint64_t p_addr);
        void    write_in_dma_buffer();
        size_t  limit_block_rate(size_t max_blocks);
        size_t  blocks_to_write(uint64_t rd_ptr, uint64_t wr_ptr, bool even);
        void    throttle_writer(const std::chrono::nanoseconds &elapsed, size_t count);
        void    writer_updates_fw_ptr(size_t written_blocks);
        bool    reset_file();

    private:
        //regulate the concurrent access to read and write pointers
        mutable std::shared_mutex m_driver_mutex;

        //emulate MSI-X on_data interrupts
        std::condition_variable m_irq_cond;
        mutable std::mutex m_irq_mutex;

        //suspend the card emulator thread when the DMA is full
        std::condition_variable m_writer_cond;
        mutable std::mutex m_stop_writer_mutex;

        unsigned int m_block_rate;
        bool m_repeat;     
        bool m_rd_odd;
        bool m_wr_odd;
        FILE*  m_fp;
        std::thread m_writer_thread;
};

#endif /* TOHOST_BUFFER_H_ */