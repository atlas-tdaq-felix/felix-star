#include "bus.hpp"


Bus::Bus(const std::string& bus_dir, const std::string& bus_group, int dmaid, bool verbose)
    : m_felix_bus_path{"dma-" + std::to_string(dmaid)}, m_bus(bus_dir, bus_group, verbose)
{ }


Bus::Bus(const std::string& bus_dir, const std::string& bus_group, const std::string& filename, bool verbose)
    : m_felix_bus_path{filename}, m_bus(bus_dir, bus_group, verbose)
{ }


bool Bus::publish(const std::vector<Elink> &elinks,
                    const std::string &ip, uint32_t port,
                    uint32_t num_pages, uint32_t page_size,
                    bool pubsub, bool unbuffered)
{
    for(auto& e : elinks){
        felixbus::FelixBusInfo info;
        info.fid = e.fid;
        info.pubsub = pubsub;
        info.stream = e.has_streams;
        info.unbuffered = unbuffered;
        info.ip = ip;
        info.port = port;
        info.netio_pages = num_pages;
        info.netio_pagesize = page_size;
        info.raw_tcp = false;
        try {
            m_bus.publish(e.fid, m_felix_bus_path, info);
        }
        catch (std::exception& ex){
            LOG_ERR("%s", ex.what());
            return false;
        }
    }
    return true;
}