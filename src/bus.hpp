#ifndef FELIX_BUS_H_
#define FELIX_BUS_H_

#include <string>
#include <vector>
#include "elink.hpp"
#include "log.hpp"
#include "felixbus/felixbus.hpp"

class FelixBus;

/**
 * Inteface class for felix-bus (medium for e-link advertisement)
 */
class Bus
{
    public:
        /**
         * Felix-bus constructor. The bus filename is generated automatically
         * using the DMA buffer identifier.
         *
         * @param bus_dir bus directory.
         * @param bus_group bus group name.
         * @param dmaid DMA buffer number.
         * @param verbose option for the felix-bus.
         */ 
        explicit Bus(const std::string& bus_dir, const std::string& bus_group, int dmaid, bool verbose);

        /**
         * Felix-bus constructor with ad-hoc filename.
         *
         * @param bus_dir bus directory.
         * @param bus_group bus group name.
         * @param filename bus file name, to set a custom file name.
         * @param verbose option for the felix-bus.
         */ 
        explicit Bus(const std::string& bus_dir, const std::string& bus_group, const std::string& filename, bool verbose);

        Bus(const Bus &) = delete;
        Bus &operator=(const Bus &) = delete;

        /**
         * Publish on the current bus a list of e-links
         *
         * @param elinks vector of elinks.
         * @param ip local ip for the advertising application.
         * @param num_pages number of network buffers the client shall allocate.
         * @param page_size size of network buffers the client shall allocate.
         * @param pubsub publish/subscribe or send/receive?
         * @param unbuffered let the client know if unbuffer communication is used.
         */       
        bool publish(const std::vector<Elink> &elinks,
                     const std::string &ip, uint32_t port,
                     uint32_t num_pages = 0, uint32_t page_size = 0,
                     bool pubsub = true, bool unbuffered = false);
    private:
        std::string m_felix_bus_path;
        felixbus::FelixBus m_bus;
};

#endif /* FELIX_BUS_H_ */
