#ifndef FELIX_DECODER_H_
#define FELIX_DECODER_H_

#include <string>
#include <memory>
#include "device.hpp"
#include "elink.hpp"
#include "tohost_monitor.hpp"
#include "block.hpp"
#include "publisher.hpp"
#include "l0id_decoder.hpp"


/**
 * Data structure used to store a chunk as a vector of subchunks.
 * Each subchunk is described by an iovec.
 * */
struct chunk_buffer
{
    std::uint8_t m_status_byte{};
    size_t chunk_size{};
    std::vector<iovec> iov;

    chunk_buffer() : m_status_byte(0), chunk_size(0){
        iov.emplace_back(&m_status_byte, 1);
    };

    void push_back(iovec&& x) {
        iov.push_back(x);
        chunk_size += x.iov_len;
    }
    
    void clear() noexcept {
        iov.resize(1);
        chunk_size = 0;
        m_status_byte = 0;
    }

    bool empty() const noexcept {
        // Status byte
        return iov.size() == 1;
    }

    void remove_last_entry() {
        chunk_size -= iov.back().iov_len;
        iov.resize(iov.size()-1);
    }

    void update_status_byte(uint8_t status) {
        m_status_byte |= status;
    }

    uint8_t get_status_byte() const {
        return m_status_byte;
    }

    size_t byte_size() {
        return chunk_size;
    }

    size_t iov_len() {
        return iov.size();
    }

    iovec* iov_addr() {
        return iov.data();
    }

};


/**
 * Block decoder associated to a one e-link.
 * The Decoder translates blocks into chunks and published them over the
 * network. Multiple Decoders share one Publisher.
 * */
class Decoder
{
    public:
        Decoder(const Decoder&) = delete;
        Decoder& operator=(const Decoder&) = delete;

        Decoder(Decoder&&) = default;
        Decoder& operator=(Decoder&&) = default;

        /**
         * @brief Decoder class constructor
         * @param elink whose blocks are to be processed by this decoder.
         * @param l0id_decoder_fmt data format for L0ID sequence check (debug feature for detector integration).
         * @param block_size size of the blocks read from the DMA buffer.
         * @param buf_vaddr virtual address of beginning of DMA buffer (used for computation of key needed in zero-copy mode).
         * 
         * @return Publisher return code
         */
        Decoder(const Elink &elink, Publisher &publisher, flx_tohost_format fmt,
            int l0id_decoder_fmt, unsigned int block_size, uint64_t buf_vaddr);

        /**
         * @return address of the last processed block. Used in zero-copy mode.
         */
        uint32_t get_last_block(){return m_last_block;}

        /**
         * @return fid of the e-link associated to this decoder
         */
        uint64_t get_fid(){return m_elink.fid;}

        /**
         * @brief set the block size used by firmware
         */
        void set_block_size(unsigned int block_size){m_block_size = block_size;}

        /**
         * @brief decode the block. This function will redirect to decode_subchunk_headers or decode_subchunk_trailers
         * @param block address of the block to decode
         * @return Publisher return code
         */
        Publisher::Result decode(Block & block);

        /**
         * @return copy of the current monitoring data
         */
        ToHostElinkStats get_decoder_stats(){return m_stats;}

        /**
         * @brief wrapper of ToHostElinkStats::get_increment
         * @return a new instance of ToHostElinkStats containing the difference between the current values
         * and the previous ones. It also updates the the "previous" value to the current one.
         */
        ToHostElinkStats get_decoder_stats_increment(ToHostElinkStats & previous);

    private:
        enum SubchunkType {
            NIL = 0, FIRST, LAST, WHOLE, MIDDLE, TIMEOUT, OOB
        };

        /**
         * @details subchunks are accumulated. If the chunk is completed it is posted to the Publisher.
         */
        Publisher::Result post_subchunk(uint8_t *data, uint32_t length, SubchunkType type, uint8_t err);

        /**
         * @details Check header integrity, sequence number.
         */
        [[nodiscard]] Publisher::Result check_block_integrity(Block & block);


        /**
         * @brief decode the block chunk header format
         * @param block address of the block to decode
         * @return Publisher return code
         */
        Publisher::Result decode_subchunk_headers(Block & block);

        /**
         * @brief decode the block with chunk trailer format
         * @param block address of the block to decode
         * @return Publisher return code
         */
        Publisher::Result decode_subchunk_trailers(Block & block);

        void on_successful_send();

    private:
        ToHostElinkStats m_stats;
        Elink m_elink;
        Publisher &m_publisher;
        uint64_t m_buffer_vaddr;
        unsigned int m_block_size;

        uint8_t m_seqnr_err = 0;
        uint8_t m_last_seqnr = 0;
        uint32_t m_last_block = 0;
        uint16_t m_chunk_position = 0;
        std::function< Publisher::Result(Block & block)> m_decode;


        std::unique_ptr<L0Decoder> m_l0id_checker;
        std::vector<std::pair<uint16_t, uint32_t>> m_subchunks;
        chunk_buffer m_scratch;
};

#endif /* FELIX_DECODER_H_ */
