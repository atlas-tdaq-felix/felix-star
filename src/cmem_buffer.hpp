#ifndef FELIX_DMA_H_
#define FELIX_DMA_H_

#include <string>

/**
 * Base class for a FELIX DMA buffer.
 * The buffer is defined by a physical address (paddr - used by the FELIX card),
 * a size (size) and end-address (pend).
 * The physical address is mapped to a virtual address (vaddr) used by software
 * in user space.
 * Software and firmware maintain each a read/write address depending on the
 * data transfer direction. The address managed by software is pc_ptr.
 * The firmware pointer is read from the card register or is emulated in software
 * by emu_fw_ptr.
 * */
class DmaBuffer {
    public:
        uint64_t paddr;
        uint64_t vaddr;
        size_t size;
        uint64_t pend;

        uint64_t pc_ptr;
        uint64_t emu_fw_ptr;

        /**
         * @brief Constructor for a DMA buffer.
         * @details All address are initialised to zero.
         * @param size buffer size.
         */ 
        explicit DmaBuffer(size_t c) : paddr(0), vaddr(0), size(c), pend(0), pc_ptr(0), emu_fw_ptr(0) {};
        virtual ~DmaBuffer() {};

};

/**
 * DMA buffer allocated by the CMEM driver.
 * */
class CmemBuffer : public DmaBuffer {
    public:
        int handle;
        bool free_previous_cmem;

        /**
         * @brief Allocate a CMEM buffer
         * @param size requested buffer size.
         * @param cmem_name buffer name passed to the CMEM driver.
         * @param free_previous_cmem let CMEM free an existing buffer with same name if not locked.
         */ 
        explicit CmemBuffer(size_t size, const std::string& cmem_name, bool free_previous_cmem = false);
        ~CmemBuffer();

        CmemBuffer(const CmemBuffer&) = delete;
        CmemBuffer& operator=(CmemBuffer&&) = delete;

        /**
         * @brief Check if a buffer with the same name is already allocated.
         * @param cmem_name buffer name passed to the CMEM driver.
         */ 
        static bool cmem_buffer_exists(const std::string& cmem_name);
};

/**
 * Buffer allocated in memory to emulate a CMEM DMA buffer.
 * */
class VmemBuffer : public DmaBuffer {
    public:
        /**
         * @brief Allocate a buffer in memory
         * @param size requested buffer size.
         */ 
        explicit VmemBuffer(size_t size);
        ~VmemBuffer();

        VmemBuffer(const VmemBuffer&) = delete;
        VmemBuffer& operator=(VmemBuffer&&) = delete;

    private:
        void* ptr;
};

#endif /* FELIX_DMA_H_ */
