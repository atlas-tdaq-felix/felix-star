#ifndef DEVICE_H_
#define DEVICE_H_

#include <cstdint>
#include <sys/types.h>
#include <string>

#include "flxcard/FlxCard.h"
#include "cmem_buffer.hpp"
#include "config/config.hpp"
#include "elink.hpp"

#define MAX_BROADCAST_CHANNELS (32)
#define MAX_BROADCAST_ELINKS (1 + 32 + 64)

#define TOFLX_EOM_MASK_RM4 0x0001
#define TOFLX_EOM_SHIFT_RM4 0
#define TOFLX_LENGTH_MASK_RM4 0x001E
#define TOFLX_LENGTH_SHIFT_RM4 1
#define TOFLX_ELINK_MASK_RM4 0xFFE0
#define TOFLX_ELINK_SHIFT_RM4 5
#define TOFLX_LENGTH_MASK 0x001F
#define TOFLX_ELINK_SHIFT 5
#define TOFLX_LENGTH_MASK_HDR32 0x003F
#define TOFLX_ELINK_SHIFT_HDR32 6
#define TOFLX_LENGTH_MASK_HDR32_8B 0x000000FF
#define TOFLX_ELINK_SHIFT_HDR32_8B 8


enum flx_fromhost_format {
    FROMHOST_FORMAT_REGMAP4,
    FROMHOST_FORMAT_5BIT_LENGTH,
    FROMHOST_FORMAT_HDR32_PACKET32,
    FROMHOST_FORMAT_HDR32_PACKET64,
    // Header with 8-bit fields (see FLX-2294):
    FROMHOST_FORMAT_HDR32_PACKET32_8B,
    FROMHOST_FORMAT_HDR32_PACKET64_8B,
    FROMHOST_FORMAT_HDR32_PACKET128_8B,
    UN_INITIALIZED
};

enum flx_tohost_format {
    TOHOST_SUBCHUNK_TRAILER,
    TOHOST_SUBCHUNK_HEADER
};

/**
 * Abstract base class that represents a Felix device.
 * The two implementations are FlxDevice and FileDevice.
 * FlxDevice manages one PCIe endpoint of a physical FELIX card. 
 * FileDevice emulates the hardware.
 */
class Device
{
    protected:
        unsigned                m_device_number;
        uint8_t                 m_vid;
        uint16_t                m_did, m_cid;
        u_int                   m_firmware_type;
        flx_fromhost_format     m_fromHostDataFormat    {UN_INITIALIZED};
        flx_tohost_format       m_toHostDataFormat      {TOHOST_SUBCHUNK_TRAILER};

        /**
         * @return value of BROADCAST_ENABLE_GEN register
         * @param channel the link to be checked 
         */
        virtual uint64_t get_broadcast_enable_gen(u_int channel) = 0;

        /**
         * Check if the e-link has streams enabled
         * @param channel (lp)GBT link 
         * @param egroup (lp)GBT e-group 
         * @param epath (lp)GBT e-path
         */
        virtual bool elink_has_stream_id(u_int channel, u_int egroup, u_int epath) = 0;

    public:

        /**
         * Constructor of abstract class to be used by implementation.
         * The constructor allocates the object, does not interact with hardware.
         *
         * @param dev_no Device number, as use dby the flx driver.
         * @param vid Version of felix resource identifier (fid)
         * @param did Detector ID of felix resource identifier
         * @param cid Connect ID of felix resource identifier, maps to the PCIe endpoint 
         */
        Device(unsigned dev_no, uint8_t vid, uint16_t did, uint16_t cid);

        /**
         * Opens the device, maps and reads registers, sets resource locks.
         * @param lock_mask to lock resources at driver level
         */
        virtual int open_device(u_int lock_mask) = 0;

        /**
         * Releases the device.
         */
        virtual void close_device()              = 0;

        /**
         * @return whether if this PCIe endpoint is the primary,
         * where all registers are available.
         */
        virtual bool is_primary() = 0;

        /**
         * @return the card type: 709, 710, 711, 712, 182, 155.
         */
        virtual unsigned int get_card_model() = 0;

        /**
         * @return number of PCIe endpoints for the card
         */
        virtual unsigned int get_card_endpoints() = 0;

        /**
         * @return the firmware register map version, read from the
         * register REG_MAP_VERSION
         */
        virtual u_int get_regmap_version()       = 0;

        /**
         * @return the firmware type as reported in the register FIRMWARE_MODE
         */
        virtual u_int get_firmware_type(){return m_firmware_type;}

        /**
         * @return the number of links for the PCie endpoint as reported in
         * the register NUM_OF_CHANNELS
         */
        virtual u_int get_number_of_channels()   = 0;

        /**
         * @return whether GBT wide mode is used
         */        
        virtual bool  get_wide_mode()            = 0;

        /**
         * @return the ToHost block size as read from the BLOCKSIZE register
         */     
        virtual u_int get_block_size()           = 0;

        /**
         * @return the ToHost trailer/header size. Derived from CHUNK_TRAILER_32B.
         */  
        virtual u_int get_trailer_size()         = 0;

        /**
         * @return the FromHost data format, as in FROMHOST_DATA_FORMAT.
         */          
        virtual flx_fromhost_format get_fromhost_data_format() = 0;

        /**
         * @return the ToHost data format, as in TOHOST_DATA_FORMAT.
         */          
        virtual flx_tohost_format get_tohost_data_format() = 0;

        /**
         * @return the identifier of the FromHoist DMA buffer, to be used by
         * felix-toflx.
         */            
        virtual int   get_fromhost_dmaid()       = 0;

        /**
         * @return the identifier of the Trickle configuration DMA buffer.
         */
        virtual int   get_trickle_dmaid()        = 0;

        /**
         * @return the maximum size supported for a TLP block
         */            
        virtual int   dma_max_tlp_bytes()        = 0;

        /**
         * \defgroup Registers
         * @{
         * Functions to interact with the device registers
         */
        /**
         * Set a new value for the given register
         * @param key register name
         * @param value register value
         */
        virtual void set_register(const char *key, uint64_t value) = 0;

        /**
         * Get the current value of the fiven register
         * @param key register name
         */
        virtual uint64_t get_register(const char *key)             = 0;

        /**
         * @return if the given register exists
         * @param key register name
         */
        virtual bool check_register(const char *key)               = 0;
        /**
         * @}
         */

        /**
         * \defgroup Interrupts
         * @{
         * Functions to configure or wait for MSI-X interrupts
         */
        /**
         * Cancel wait requests for the given MSI-X interrupt
         * @param i Interrupt number 
         */
        virtual void cancel_irq(int i)  = 0;

        /**
         * Wait for the given interrupt. This function blocks.
         * @param i Interrupt number 
         */
        virtual void irq_wait(int i)    = 0;

        /**
         * Arm the given interrupt.
         * @param i interrupt number 
         */
        virtual void irq_enable(int i)  = 0;
    
        /**
         * Disarm the given interrupt.
         * @param i interrupt number 
         */
        virtual void irq_disable(int i) = 0;
        /**
         * @}
         */

        /**
         * \defgroup E-link configuration
         * @{
         * Functions related to e-link configuration including broadcast
         */
        /**
         * Return the list of enabled e-links for the given DMA id.
         * @param dmaid dma buffer identifier
         */
        virtual std::vector<Elink> read_enabled_elinks(int dmaid) = 0;

        /**
         * Return the list of e-links enabled of a given type.
         * @param dmaid dma buffer identifier
         * @param t e-link type e.g. DAQ, DCS, TTC2H...
         */
        std::vector<Elink> get_enabled_elinks_of_type(int dmaid, elink_type_t t);

        /**
         * @return the device number.
         */
        unsigned get_device_number(){return m_device_number;}

        /**
         * @return the lock mask for DMA resources
         * @param dmaids list of DMA buffers in use
         */
        unsigned int make_dma_lock_mask(const std::vector<int>& dmaids);

        /**
         * @return the number of broadcast e-links
         * @param broadcast_elinks list of DMA buffers in use
         * @param num_of_channels list of DMA buffers in use
         */
        u_int get_broadcast_elinks(uint64_t broadcast_elinks[], uint32_t num_of_channels);

        /**
         * @return (?)
         * @param elink list of DMA buffers in use
         */
        static int64_t broadcast_for(uint64_t elink);

      /**
         * @return the level of broadcast: link, channel, e-liink
         * @param elink broadcast e-link
         */
        static int broadcast_type(uint64_t elink);

        /**
         * @return the level of broadcast: link, channel, e-liink
         * @param elink broadcast e-link
         */
        static const std::string broadcast_type_str(uint64_t elink);

        /**
         * @brief read broadcast_elinks and broadcast_enable_registers
         * @param broadcast_elinks array of broadcast enabled e-links per link
         * @param broadcast_enable_registers register values for each link
         * @param num_of_channels number of device links
         * @return the number of broadcast links
         */
        static u_int broadcast_elinks(uint64_t broadcast_elinks[], const uint64_t broadcast_enable_registers[], uint32_t num_of_channels);

        /**
         * Return the e-link type given the encoding used
         * @param encoding e-link encoding
         */        
        static const std::string get_elink_type_str(elink_type_t encoding);

        /**
         * Compare the software register map version with hardware one
         */  
        bool has_wrong_rm();

        /**
         * Firmware does not implement ToHost (lp)GBT e-links
         */  
        bool is_full_mode();

        /**
         * Firmware implements ToHost (lp)GBT e-links
         */ 
        bool is_lpgbt_mode();

        /**
         * Firmware implements ToHost (lp)GBT e-links
         */ 
        uint64_t get_elink(u_int channel, u_int egroup, u_int epath);

        /**
         * Firmware implements ToHost (lp)GBT e-links
         * @param elink elink number
         */ 
        uint32_t get_channel(uint32_t elink);

        /**
         * Firmware implements ToHost (lp)GBT e-links
         * @param is_to_flx true if the DMA is a FromHost one
         * @param elink elink number
         */ 
        uint32_t get_egroup(uint32_t elink);

        /**
         * Firmware implements ToHost (lp)GBT e-links
         */ 
        uint32_t get_epath(uint32_t elink);

        /**
         * Firmware implements ToHost (lp)GBT e-links
         * @param is_to_flx true if the DMA is a FromHost one
         */ 
        u_int get_egroups_per_channel(u_int is_to_flx);

        /**
         * Firmware implements ToHost (lp)GBT e-links
         * @param is_to_flx true if the DMA is a FromHost one
         */ 
        u_int get_epaths_per_egroup(u_int is_to_flx);

        /**
         * Firmware implements ToHost (lp)GBT e-links
         */ 
        u_int get_ctrl_elinks_per_channel();
        /**
         * @}
         */

        /**
         * \defgroup DMA operations
         * @{
         * Functions related to DMA read/write operations 
         */
        /**
         * @return true is the DMA buffer is enabled
         * @param dmaid true if the DMA is a FromHost one
         */ 
        virtual bool dma_enabled(int dmaid)                                     = 0;

        /**
         * @brief set up ToHost (circular) DMA buffer
         * @param dmaid DMA buffer number
         */ 
        virtual void dma_to_host(DmaBuffer* buf, int dmaid)                     = 0;

        /**
         * @brief set up FromHost (circular) DMA buffer
         * @param dmaid DMA buffer number
         */ 
        virtual void dma_from_host(DmaBuffer* buf, int dmaid)                   = 0;


        /**
         * @brief Set up FromHost (circular) DMA buffer for trickle mode.
         * @param buf DMA buffer pointer.
         * @param dmaid DMA buffer identifier.
         * @param config_size Size of the trickle configuration message.
         */
        virtual void dma_from_host_trickle(DmaBuffer* buf, int dmaid, size_t config_size)   = 0;

        /**
         * @brief set up FromHost (non-circular) DMA buffer
         * @param dmaid DMA buffer number
         */ 
        virtual void dma_set_oneshot(DmaBuffer* buf, int dmaid, size_t config_size)         = 0;

        /**
         * @return pointer managed by firmware (wr for ToHost, rd for FromHost)
         * @param dmaid DMA buffer number
         */ 
        virtual uint64_t dma_get_fw_ptr(int dmaid)                              = 0;

        /**
         * @return pointer managed by software (rd for ToHost, wr for FromHost)
         * @param dmaid DMA buffer number
         */         
        virtual uint64_t dma_get_sw_ptr(int dmaid)                              = 0;

        /**
         * @brief set new value for pointer managed by software (rd for ToHost, wr for FromHost)
         * @param dmaid DMA buffer number
         * @param p_absolute birtual memory address
         */  
        virtual void     dma_set_sw_ptr(int dmaid, uint64_t p_absolute)         = 0;

        /**
         * @return whether sw and fw pointers are have wrapped around the
         * circular buffer the the same number of times.
         * @param dmaid DMA buffer number
         */  
        virtual bool     dma_cmp_even_bits(int dmaid)                           = 0;

        /**
         * @brief stop DMA buffer data transfer operations
         * @param dmaid DMA buffer number
         */  
        virtual void     dma_stop(unsigned int dmaid)                           = 0;
        /**
         * @}
         */

        /**
         * @return hardware monitoring data
         */
        virtual monitoring_data_t hw_get_monitoring_data( unsigned int mon_mask = 0xFFF ) = 0;
};


/**
 * FlxDevice represents a PCIe endpoint of a physical FELIX card.
 * */
class FlxDevice : public Device
{
    private:
        FlxCard m_dev;
        encoding_t elink_get_encoding(u_int channel, u_int egroup, u_int epath, bool is_to_flx);
        bool elink_is_in_dma(local_elink_t elink, int dmaid);
        bool elink_is_enabled(u_int channel, u_int egroup, u_int epath, bool is_to_flx);

        local_elink_t get_ec_elink(bool is_to_flx, u_int channel);
        local_elink_t get_ic_elink(bool is_to_flx, u_int channel);
        local_elink_t get_aux_elink(bool is_to_flx, u_int channel);

        bool elink_is_ic_enabled(bool is_to_flx, u_int channel);
        bool elink_is_ec_enabled(bool is_to_flx, u_int channel);
        bool elink_is_aux_enabled(bool is_to_flx, u_int channel);

        uint64_t get_broadcast_enable_gen(u_int channel)                   override;
        bool elink_has_stream_id(u_int channel, u_int egroup, u_int epath) override;        

    public:
        FlxDevice(Config& cfg, unsigned device);
        explicit FlxDevice(unsigned dev_no);

        int  open_device(u_int lock_mask) override;
        void close_device()               override;
        bool is_primary()                 override;

        unsigned int get_card_model()     override;
        unsigned int get_card_endpoints() override;
        u_int get_regmap_version()        override;
        u_int get_number_of_channels()    override;
        bool  get_wide_mode()             override;
        u_int get_block_size()            override;
        u_int get_trailer_size()          override;
        flx_fromhost_format get_fromhost_data_format()  override;
        flx_tohost_format   get_tohost_data_format()    override;
        int get_fromhost_dmaid()          override;
        int get_trickle_dmaid()           override;
        int dma_max_tlp_bytes()           override;

        //Registers
        void     set_register(const char *key, uint64_t value) override;
        uint64_t get_register(const char *key)                 override;
        bool     check_register(const char *key)               override;

        //Interrupts
        void cancel_irq(int i)  override;
        void irq_wait(int i)    override;
        void irq_enable(int i)  override;
        void irq_disable(int i) override;

        //Elinks
        std::vector<Elink> read_enabled_elinks(int dmaid) override;

        //DMA operations
        bool dma_enabled(int dmaid)                                         override;
        void dma_to_host(DmaBuffer* buf, int dmaid)                         override; //start_circular_dma
        void dma_from_host(DmaBuffer* buf, int dmaid)                       override;
        void dma_from_host_trickle(DmaBuffer* buf, int dmaid, size_t config_size)  override;
        void dma_set_oneshot(DmaBuffer* buf, int dmaid, size_t config_size)        override;
        uint64_t dma_get_fw_ptr(int dmaid)                                  override; //dma_get_current_address
        uint64_t dma_get_sw_ptr(int dmaid)                                  override;
        void     dma_set_sw_ptr(int dmaid, uint64_t p_phys)                 override; //dma_set_ptr
        bool     dma_cmp_even_bits(int dmaid)                               override;
        void     dma_stop(unsigned int dmaid)                               override;

         //HW monitor
        monitoring_data_t hw_get_monitoring_data( unsigned int mon_mask = 0xFFF ) override;
};


/**
 * FileDevice represents a PCIe endpoint of a FELIX card emulated in software.
 * */
class FileDevice : public Device  
{
    private:
        u_int   m_locks;
        u_int   m_regmap;
        u_int   m_channels;
        u_int   m_block_size;
        u_int   m_trailer_size;  //determined in open_device
        bool    m_wide_mode;
        int     m_max_tlp_bytes;
        int     m_toflx_dmaid;
        int     m_trickle_dmaid;
        uint32_t m_status_leds;
        int     m_busy;
        std::map<int, std::vector<Elink>> m_enabled_elinks;

        uint64_t get_broadcast_enable_gen(u_int channel)                   override;
        bool elink_has_stream_id(u_int channel, u_int egroup, u_int epath) override;  

    public:
        FileDevice(ConfigFile& cfg, unsigned device);
        explicit FileDevice(unsigned dev_no);
        int open_device(u_int lock_mask)        override;
        void close_device() override {return;}
        bool is_primary() override {return not (m_device_number % 2);}
        unsigned int get_card_endpoints() override {return 2;}
        unsigned int get_card_model() override {return 712;}
        u_int get_regmap_version()       override {return m_regmap;}
        u_int get_number_of_channels()   override {return m_channels;}
        bool get_wide_mode()             override {return m_wide_mode;}
        u_int get_block_size()           override {return m_block_size;}
        u_int get_trailer_size()         override {return m_trailer_size;}
        flx_fromhost_format get_fromhost_data_format() override {return m_fromHostDataFormat;}
        flx_tohost_format   get_tohost_data_format()   override {return m_toHostDataFormat;}
        int get_fromhost_dmaid()         override {return m_toflx_dmaid;}
        int get_trickle_dmaid()          override {return m_trickle_dmaid;}
        int dma_max_tlp_bytes()          override {return m_max_tlp_bytes;}

        //Used for felix-register tests
        void     set_register(const char *key, uint64_t value) override;
        uint64_t get_register(const char *key)                 override;
        bool     check_register(const char *key) override  {return true;}

        //Interrupts
        void cancel_irq(int i)  override {return;}
        void irq_wait(int i)    override {return;}
        void irq_enable(int i)  override {return;}
        void irq_disable(int i) override {return;}

        //Elinks
        std::vector<Elink> read_enabled_elinks(int dmaid) override;
        void add_enabled_elink(Elink e, int dmaid);

        //DMA operations
        bool dma_enabled(int dmaid)                     override;
        void dma_to_host(DmaBuffer* buf, int dmaid)     override; //start_circular_dma
        // Remaining functions not implemented, 
        // buffer to be managed by DmaBufferReader/Writer
        void dma_from_host(DmaBuffer* buf, int dmaid)                       override {return;};
        void dma_from_host_trickle(DmaBuffer* buf, int dmaid, size_t config_size)  override {return;};
        void dma_set_oneshot(DmaBuffer* buf, int dmaid, size_t config_size)        override {return;};
        uint64_t dma_get_fw_ptr(int dmaid)                                  override {return 0;} //dma_get_current_address
        uint64_t dma_get_sw_ptr(int dmaid)                                  override {return 0;}
        void     dma_set_sw_ptr(int dmaid, uint64_t p_phys)                 override {return;}//dma_set_ptr
        bool     dma_cmp_even_bits(int dmaid)                               override {return false;}
        void     dma_stop(unsigned int dmaid)                               override {return;}

        //HW monitor
        monitoring_data_t hw_get_monitoring_data( unsigned int mon_mask = 0xFFF ) override;

};

#endif /* DEVICE_H_ */
