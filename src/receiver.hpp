#ifndef FELIX_RECEIVER_H_
#define FELIX_RECEIVER_H_

#include <sys/uio.h>
#include <cstdint>
#include <functional>
#include <vector>
#include <string>

#include "elink.hpp"
#include "network/fromhost_message.hpp"

/**
 * Interface class to any receiver implemented by the network backend.
 */
class Receiver {

public:
    using OnMsg = std::function<void(const std::vector<ToFlxMessage>&)>;
    using OnConnOpen = std::function<void (const std::string &)>;
    using OnConnClose = std::function<void (const std::string &)>;

    virtual ~Receiver() = default;

    /** 
     * @brief advertise e-links services by the receiver.
     * @param elinks to advertise.
     * @return whether the operation was succesfull.
     */
    virtual bool declare(const std::vector<Elink> &elinks) = 0;

    /** 
     * @brief application callback for new connections.
     * @param callback the std::function<void (std::string &)> to invoked when a
     * new connection is established.
     */
    virtual void set_conn_open_callback(OnConnOpen callback) = 0;

    /** 
     * @brief application callback for closed connections.
     * @param callback the std::function<void (std::string &)> to be invoked
     * when a new connection is closed.
     */
    virtual void set_conn_close_callback(OnConnClose callback) = 0;

    /** 
     * @brief application callback for received messages.
     * @param callback the std::function<void(uint8_t*, size_t)> to invoke when
     * a message is received.
     */
    virtual void set_on_msg_callback(OnMsg callback) = 0;

    /** 
     * @brief Monitor the number of open connetions.
     * @return  the number of open connections.
     */
    virtual int get_number_of_connections() = 0;

};

#endif /* FELIX_RECEIVER_H_ */
