#ifndef L0ID_DECODER_H_
#define L0ID_DECODER_H_

#include "block.hpp"
#include <functional>
#include <vector>
#include <sys/uio.h>

#include "felix/felix_fid.h"

inline bool is_ttc2h_elink(uint64_t fid)
{
    uint32_t elink = get_elink(fid);
#if REGMAP_VERSION < 0x0500
    if (elink == 315 || elink == 827 || elink == 1537 || elink == 0x600){
#else
    if (elink == 0x600){
#endif
        return true;
    } else {
        return false;
    }
}

enum L0ID_FMT {
    TTC2H = 1, LATOME = 2, FMEMU = 3, FELIG = 4, NSW_VMM = 5, NSW_TP = 6
};

/**
 * Check of L0ID sequentiality
 *
 * The L0Decoder class is capable of retrieving the L0ID (and BCID for TTC2H
 * link) and verify it increases sequentially across chunks.
 *
 */
class L0Decoder
{
    public:
        L0Decoder(int format, uint64_t fid);

        /**
         * @brief Check L0ID sequence of the passed chunk
         * @param data The iov vector used by Decoder to store a chunk.
         * @return true in when out-of-sequence L0ID is detected.
         */
        std::function<bool(const uint8_t* data, size_t len)> check_sequence_error;

        /**
         * @brief Assemble the necessary chunk bytes and calls check_sequence_error
         * @param data the iovec vector of subchunks
         * @return whether the sequence check is passed or not.
         */
        bool check_tohost_chunk(std::vector<iovec> const &data);

        /**
         * @return Last L0ID processed.
         */
        xl1id_t get_last(){return m_last;}

        /**
         * @return Mask of L0ID event counter.
         */
        uint32_t get_ec_mask(){return m_ec_mask;}

    private:
        uint64_t m_fid;
        xl1id_t m_last;
        uint32_t m_required_bytes;
        uint32_t m_ec_mask;

        /** @brief Printout of error message  */
        bool compare(xl1id_t current, xl1id_t expected);

        bool check_sequence_ttc2h(  const uint8_t* data, size_t len);
        bool check_sequence_latome( const uint8_t* data, size_t len);
        bool check_sequence_fmemu(  const uint8_t* data, size_t len);
        bool check_sequence_felig(  const uint8_t* data, size_t len);
        bool check_sequence_nsw_vmm(const uint8_t* data, size_t len);
        bool check_sequence_nsw_tp( const uint8_t* data, size_t len);
};

#endif /* L0ID_DECODER_H_ */
