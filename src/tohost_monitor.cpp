#include <cassert>
#include <cstdint>
#include <format>

#include "tohost_monitor.hpp"

void ToHostElinkStats::update_processed_chunk(uint8_t status, uint32_t size) {
    ++processed_chunks;
    if ( size > largest_chunk_size){
        largest_chunk_size = size;
    }
    processed_bytes += size;
    if (status & SW_TRUNC){++sw_trunc_chunks;}
    if (status & FW_TRUNC){++fw_trunc_chunks;}
    if (status & SW_MALF) {++sw_error_chunks;}
    if (status & FW_MALF) {++fw_error_chunks;}
    if (status & FW_CRC)  {++fw_crc_chunks;}
}


ToHostElinkStats ToHostElinkStats::get_increment(ToHostElinkStats & previous)
{
    assert(this->fid == previous.fid);
    struct timespec now;
    ToHostElinkStats output(previous.fid);

    //Counters
    output.dropped_blocks     = this->dropped_blocks;
    output.processed_blocks   = this->processed_blocks;
    output.processed_chunks   = this->processed_chunks;
    output.processed_bytes    = this->processed_bytes;
    output.largest_chunk_size = this->largest_chunk_size;
    output.sw_trunc_chunks    = this->sw_trunc_chunks;
    output.fw_trunc_chunks    = this->fw_trunc_chunks;
    output.sw_error_chunks    = this->sw_error_chunks;
    output.fw_error_chunks    = this->fw_error_chunks;
    output.fw_crc_chunks      = this->fw_crc_chunks;
    output.oosequence_l0id    = this->oosequence_l0id;

    //Rates
    clock_gettime(CLOCK_MONOTONIC_RAW , &now);
    float seconds = now.tv_sec - previous.ts.tv_sec + 1e-9*(now.tv_nsec - previous.ts.tv_nsec);
    float chunk_increment = static_cast<float>(output.processed_chunks - previous.processed_chunks);
    float bytes_increment = static_cast<float>(output.processed_bytes - previous.processed_bytes);
    output.rate_chunks_kHz    = (chunk_increment / seconds) / 1000.;
    output.average_chunk_bytes = chunk_increment > 0 ? static_cast<uint32_t>(bytes_increment / chunk_increment) : 0;

    //Update old version of quantities for which a rate is printed
    previous.processed_chunks   = this->processed_chunks  ;
    previous.processed_bytes    = this->processed_bytes  ;
    previous.ts                 = now;

    return output;
}


void ToHostMonitor::append_device_stats(const std::string& ts, const std::string& hostname, const ToHostDeviceStats & s)
{
};


void ToHostMonitor::append_dma_stats(const std::string& ts, const std::string& hostname, int device, const ToHostDmaStats & ds)
{
    nlohmann::json j;
    j["dma_free_MB"] = ds.dma_free_MB;
    j["irqs"] = ds.irq_count;

    m_message = nlohmann::json();
    m_message["ts"] = ts;
    m_message["host"][hostname]
             ["app"]["tohost"]
             ["device"][std::to_string(device)]
             ["dma"][std::to_string(ds.dmaid)] = j;

    write_message();
}


void ToHostMonitor::append_reader_stats(const std::string& ts, const std::string& hostname, int device, int dma_id, const ToHostReaderStats & rs)
{
    nlohmann::json j;
    j["type"] = rs.type;
    j["subscriptions"] = rs.number_of_subscriptions;
    j["net_resources"] = rs.number_of_network_resources;
    j["net_calls"] = rs.number_of_network_resource_calls;

    m_message = nlohmann::json();
    m_message["ts"] = ts;
    m_message["host"][hostname]
             ["app"]["tohost"]
             ["device"][std::to_string(device)]
             ["dma"][std::to_string(dma_id)]
             ["thread"][std::to_string(rs.reader_id)] = j;

    write_message();
}


void ToHostMonitor::append_elink_stats(const std::string& ts, const std::string& hostname, int device, int dma_id, int reader_id, const ToHostElinkStats & es)
{
    nlohmann::json j;
    j["blocks"]          = es.processed_blocks   ;
    j["dropped_blocks"]  = es.dropped_blocks     ;
    j["chunks"]          = es.processed_chunks   ;
    j["chunk_kHz"]       = es.rate_chunks_kHz    ;
    j["avg_chunk_size"]  = es.average_chunk_bytes;
    j["max_chunk_size"]  = es.largest_chunk_size ;
    j["sw_trunc_chunks"] = es.sw_trunc_chunks    ;
    j["fw_trunc_chunks"] = es.fw_trunc_chunks    ;
    j["sw_error_chunks"] = es.sw_error_chunks    ;
    j["fw_error_chunks"] = es.fw_error_chunks    ;
    j["fw_crc_chunks"]   = es.fw_crc_chunks      ;
    j["oosequence_l0id"] = es.oosequence_l0id    ;

    m_message = nlohmann::json();
    m_message["ts"] = ts;
    m_message["host"][hostname]
             ["app"]["tohost"]
             ["device"][std::to_string(device)]
             ["dma"][std::to_string(dma_id)]
             ["thread"][std::to_string(reader_id)]
             ["fid"][std::format("{:#0x}", es.fid)] = j;

    write_message();
}
