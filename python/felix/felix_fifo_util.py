"""Felix Fifo Utils."""
import json  # noqa: E402
import os  # noqa: E402

from datetime import datetime  # noqa: E402


class FelixFifoUtil(object):
    """Felix Fifo Utils."""

    def __init__(self, fifo):
        """Init."""
        self.source = fifo
        rd_pipe = os.open(fifo, os.O_RDONLY | os.O_NONBLOCK)
        self.pipe = os.open(fifo, os.O_WRONLY | os.O_NONBLOCK)
        os.close(rd_pipe)

    def error_write(self, source, timestamp, level, msg):
        """Publish an error."""
        data = {}
        data['source'] = source
        data['timestamp'] = timestamp
        data['level'] = level
        data['msg'] = msg
        buffer = json.dumps(data)
        os.write(self.pipe, buffer)

    def log(self, level, msg):
        """
        Log to error FIFO.

        "2012-04-23T18:25:43.511Z"
        CRITICAL, ERROR, WARNING, INFO, DEBUG, TRACE
        """
        timestamp = datetime.today().strftime('%Y-%m-%dT%H:%M:%S.000Z')
        self.error_write(self.source, timestamp, level, msg)

    def WARN(self, msg):
        """WARNING message."""
        self.log("WARNING", msg)

    def INFO(self, msg):
        """INFO message."""
        self.log("INFO", msg)

    def DBG(self, msg):
        """DEBUG message."""
        self.log("DEBUG", msg)

    def ERR(self, msg):
        """ERROR message."""
        self.log("ERROR", msg)
