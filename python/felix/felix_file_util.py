#!/usr/bin/env python3
import subprocess  # noqa: E402


def is_open_for_write(directory):
    """Return list of open files for write or update."""
    files = []
    try:
        out = subprocess.check_output(['lsof', '-Fant', '+d', directory])
    except Exception as e:
        out = str(e.output)
    if not out.strip():
        return []
    access = None
    file_type = None
    lines = out.strip().split('\n')
    for line in lines:
        if line.startswith('a'):
            access = line[1:]
        elif line.startswith('t'):
            file_type = line[1:]
        elif line.startswith('n'):
            if file_type == 'REG' and (access == 'u' or access == 'w'):
                files.append(line[1:])
            access = None
            file_type = None
        else:
            pass
    return files


# if __name__ == "__main__":
#     """test."""
#     print is_open_for_write('/var/run/felix')
